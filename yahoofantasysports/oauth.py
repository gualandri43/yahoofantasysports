"""
yahoo specialization of OAuth base class
"""

import webbrowser
import json
import os
import requests_oauthlib

import yahoofantasysports.log as yahoo_log


class YahooOAuthSession(requests_oauthlib.OAuth2Session):
    '''
    specialized requests_oauthlib.OAuth2Session.
    '''

    authorizeTokenUrl = 'https://api.login.yahoo.com/oauth2/request_auth'
    accessTokenUrl = 'https://api.login.yahoo.com/oauth2/get_token'

    def __init__(self, clientId=None, clientSecret=None, token=None, openBrowser=False, authorizeOnInit=True):
        '''
        clientId and clientSecret are required to be set, either by kwarg or environment variables.
        values given in constructor take precedence over environment variables.
            YAHOO_API_CLIENT_ID
            YAHOO_API_CLIENT_SECRET

        token               a previously authorized OAuth session token dictionary,
                            as returned by requests_oauthlib.
        openBrowser         open browser automatically to authorization url during
                            authorization step.
        authorizeOnInit     if true, check if session is authorized at the start and
                            run authorization flow if needed
        '''

        # use the given Yahoo API client ID and secret, or look for value in environment vars
        #
        if clientId:
            self.clientId = clientId
        else:
            self.clientId = os.environ.get('YAHOO_API_CLIENT_ID')

        if clientSecret:
            self.clientSecret = clientSecret
        else:
            self.clientSecret = os.environ.get('YAHOO_API_CLIENT_SECRET')

        if self.clientId is None or self.clientSecret is None:
            raise ValueError('A client ID and secret must be given or defined via environment variables.')


        # build session object passing correct values to parent class
        #
        super().__init__(
            client_id=self.clientId,
            token=token,
            auto_refresh_url=self.accessTokenUrl,
            auto_refresh_kwargs={
                'client_id': self.clientId,
                'client_secret': self.clientSecret
            },
            redirect_uri='oob',
            token_updater=lambda x: None
        )

        # logger
        #
        self.logger = yahoo_log.set_up_logger('YahooOAuthSession')

        # if session hasn't been autorized, go through OAuth approval flow
        #
        if not self.authorized and authorizeOnInit:
            self.run_authorization_flow(openBrowser=openBrowser)

    def run_authorization_flow(self, openBrowser=False):
        '''
        execute authorization flow procedure.

        openBrowser     open browser automatically to authorization url during authorization step.

        returns the new access token
        '''

        self.logger.info('Running authorization flow.')

        authorizationUrl, state = self.authorization_url(self.authorizeTokenUrl)

        if openBrowser:
            # open web browser automatically
            self.logger.info('Authorizing connection with Yahoo. Please enter the verifier code on the opened browser.')
            webbrowser.open(authorizationUrl)

        else:
            # command line only
            self.logger.info(
                'Authorizing connection with Yahoo. '
                'Visit the following url and authenticate to get verification code.'
            )
            self.logger.info(authorizationUrl)

        verifierCode = input("Enter verifier code: ")

        return self.fetch_token(
            self.accessTokenUrl,
            code=verifierCode,
            client_secret=self.clientSecret
        )

    def request(self, method, url, data=None, headers=None, withhold_token=False, **kwargs):
        '''
        override parent class request method to automatically add client_id and client_secret for
        refresh purposes.
        '''

        self.logger.debug('Requesting {0} within the session.'.format(url))

        return super().request(
            method,
            url,
            data=data,
            headers=headers,
            withhold_token=withhold_token,
            client_id=self.clientId,
            client_secret=self.clientSecret,
            **kwargs
        )

    def refresh_token(self, refresh_token=None, body="", auth=None, timeout=None,
                            headers=None, verify=True, proxies=None, **kwargs):
        '''
            override parent class request method to automatically add refresh url and
            extra needed info
        '''

        if headers is None:
            headers = dict()

        # add information required by yahoo api in headers
        #
        headers.update({
            'client_id': self.clientId,
            'client_secret': self.clientSecret
        })

        self.logger.debug('Refreshing token.')

        return super().refresh_token(
            self.accessTokenUrl,
            headers=headers
            #refresh_token=refresh_token,
            #body=body,
            #auth=auth,
            #timeout=timeout,
            #verify=verify,
            #proxies=proxies,
            #**kwargs
        )

    def token_to_json(self):
        '''
        save token state to json string.
        '''

        outdict = self.token
        return json.dumps(outdict)

    @classmethod
    def create_from_token(cls, state, clientId=None, clientSecret=None):
        '''
        Recreate a session from saved token data.

        state       json string of serialized token
        '''

        loaded = json.loads(state)
        return cls(
            clientId=clientId,
            clientSecret=clientSecret,
            token=loaded
        )

    @classmethod
    def create_from_refresh_token(cls, refreshToken, clientId=None, clientSecret=None):
        '''
        Recreate a session from only a refresh token.
        '''

        #
        # Create a new session, but disable authorization on
        #   creation. Then refresh the token to update everything
        #   as needed.
        #

        newSession = cls(
            clientId=clientId,
            clientSecret=clientSecret,
            authorizeOnInit=False
        )

        newSession.refresh_token(refresh_token=refreshToken)

        return newSession


if __name__ == "__main__":
    print('This is an import-only module.')
