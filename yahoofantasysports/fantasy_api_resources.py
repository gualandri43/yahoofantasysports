'''
Resources for loading yahoo fantasy api responses into objects.
'''

import json
import collections
import logging
import datetime as dt
import lxml.etree as etree

import yahoofantasysports.oauth as yahoo_auth
import yahoofantasysports.fantasy_api as yahoo_api
import yahoofantasysports.log as yahoo_log

class YahooResource:
    '''
    This is a base class to build all of the specific Yahoo API Resource objects.
    '''

    # Note:
    # lxml requires that the namespace be specified when using XPATH. The Yahoo API responses
    # all contain a default namespace of http://fantasysports.yahooapis.com/fantasy/v2/base.rng.
    # This can be given a shortcut name by passing namespaces={} parameter to the xpath() method
    # call. A standard argument is defined below in apiXMLnamespaces.
    #

    def __init__(self, apiElement):

        self.rootXML = apiElement
        self.apiXMLnamespaces = {
            'yns':'http://fantasysports.yahooapis.com/fantasy/v2/base.rng'
        }

    def _text_from_xpath(self, elem, xpath):

        try:
            # xpath with return a list always, even if only one result.
            #   empty list is also possible
            searchElems = elem.xpath(xpath, namespaces=self.apiXMLnamespaces)

            if len(searchElems) == 0:
                return None

            if len(searchElems) == 1:
                return searchElems[0].text

            if len(searchElems) > 1:
                return [x.text for x in searchElems]

        except:
            return None

    def _convert_from_string(self, inp):
        '''
            detect the type of string value and convert if possible
            handles
                int
                float
                dates (YYYY-MM-DD format)
                all else returned as string
        '''

        # null test
        if inp is None:
            return None

        # integer test
        try:
            return int(inp.replace(",",""))
        except:

            # float test
            try:
                return float(inp.replace(",",""))
            except:

                # date tests
                try:
                    return dt.datetime.strptime(inp, "%Y-%m-%d")
                except:

                    #return unaltered input
                    return inp

    def _parse_from_xpath_mapping(self, elemTree, xpathMapping):

        for member, xpath in xpathMapping.items():

            if not hasattr(self, member):
                raise ValueError("Member not valid: " + str(member))

            # xpathText can be none, a single text value, or a list of text
            xpathText = self._text_from_xpath(elemTree, xpath)

            if isinstance(xpathText, list):
                for entry in xpathText:
                    setattr(self, member, self._convert_from_string(entry))
            else:
                setattr(self, member, self._convert_from_string(xpathText))

    def _check_root_tag_name(self, expectedRootTag=None):

        if expectedRootTag:
            if self.rootXML.tag != expectedRootTag:
                raise ValueError("Root XML element tag must be {0}.".format(expectedRootTag))

    @staticmethod
    def load_xml_from_api_response(apiResponse):
        '''

        this can be used to produce a root element directly from a Yahoo
        API response.

        apiResponse     The response object from the requests library from the
                        API request.

        returns a lxml Element with the loaded API response data.
        '''

        #
        # lxml is picky about encodings. when the requests library
        #   reports the response.text attribute, it converts it to python
        #   unicode from the original bytes. The response.content attribute
        #   gives the encoded text as bytes. passing the response.content attribute
        #   to etree.fromstring() allows it to parse as unicode bytes and avoids
        #   weird stripping out of different parts of the API text response.
        #
        #

        rootXML = etree.fromstring(apiResponse.content)
        return rootXML

    def to_dict(self, keysToInclude=None, includeNested=True, exportXmlData=False):
        '''
        get resource as a dict

        exportXmlData       export the rootXML and apiXMLnamespaces elements
                            (only useful for debug or full object serialization)
        includeNested       if True, will recurse into nested YahooResource objects.
        keysToInclude       a list or dict defining the keys to include
                            in the returned dict.
                            If none, all keys are exported.
                            If keysToInclude is a list, it applies to the first level.
                            If keysToInclude is a dict, it applies to the nested level
                            that matches the dict keys. The keys of the dict match the
                            attribute name of the YahooResource. In the following example,
                            attrib3 is a nested YahooResource and only the subattribs
                            will be included in the nested dict. The value of the
                            non-nested attribs is not used.
                            {
                                'attrib1': None,
                                'attrib2': None,
                                'attrib3': {
                                    'subattrib1': None,
                                    'subattrib2': None
                                }
                            }
        '''

        dumpDict = {}

        # apply filter if given
        if keysToInclude and isinstance(keysToInclude, list):
            # convert list into dict keys
            keysToKeep = {k:None for k in keysToInclude}

        elif keysToInclude and isinstance(keysToInclude, dict):
            keysToKeep = keysToInclude.copy()

        else:
            # get all
            keysToKeep = {k:None for k in self.__dict__.keys()}

        # remove the XML element metadata that isn't very useful beyond debugging
        #
        if not exportXmlData:
            keysToKeep.pop('rootXML', None)
            keysToKeep.pop('apiXMLnamespaces', None)

        #loop through members
        for _member, _val in self.__dict__.items():

            if _member in keysToKeep:

                if isinstance(_val, YahooResource):
                    if includeNested:
                        dumpDict[_member] = _val.to_dict(keysToInclude=keysToKeep[_member])

                elif isinstance(_val, list) or isinstance(_val, YahooResourceCollection):
                    # YahooResourceCollection implements list-like interface through collections.UserList
                    #
                    for _entry in _val:

                        if _member not in dumpDict:
                            dumpDict[_member] = list()

                        if isinstance(_entry, YahooResource):
                            if includeNested:
                                dumpDict[_member].append(_entry.to_dict(keysToInclude=keysToKeep[_member]))
                        else:
                            dumpDict[_member].append(_entry)

                elif isinstance(_val, etree._Element):
                    dumpDict[_member] = etree.tostring(_val).decode()

                else:
                    dumpDict[_member] = _val

        return dumpDict

    def to_json(self):
        '''
        get resource as json string.
        '''
        return json.dumps(self.to_dict())

class YahooResourceCollection(collections.UserList):
    '''
    List-like collection to hold a set of YahooResource objects.
    '''

    def __init__(self, initlist=None):
        super().__init__(initlist=initlist)

    def __add__(self, other):

        if not isinstance(other, YahooResource):
            raise ValueError('Collection items must be derived from YahooResource.')

        super().__add__(other)

    def __setitem__(self, i, item):

        if not isinstance(item, YahooResource):
            raise ValueError('Collection items must be derived from YahooResource.')

        super().__setitem__(i, item)

    def __radd__(self, other):

        if not isinstance(other, YahooResource):
            raise ValueError('Collection items must be derived from YahooResource.')

        super().__radd__(other)

    def __iadd__(self, other):

        if not isinstance(other, YahooResource):
            raise ValueError('Collection items must be derived from YahooResource.')

        super().__iadd__(other)

    def to_dict(self, keysToInclude=None):
        '''
        returns a list of dict records from the collection items.
        '''

        return [x.to_dict(keysToInclude=keysToInclude) for x in self.data]

class LeagueResource(YahooResource):
    '''
    <league>
        <league_key>390.l.240720</league_key>
        <league_id>240720</league_id>
        <name>Metamora Fantasy Football</name>
        <url>https://football.fantasysports.yahoo.com/archive/nfl/2019/240720</url>
        <logo_url>https://yahoofantasysports-res....</logo_url>
        <password/>
        <draft_status>postdraft</draft_status>
        <num_teams>10</num_teams>
        <edit_key>16</edit_key>
        <weekly_deadline/>
        <league_update_timestamp>1577869680</league_update_timestamp>
        <scoring_type>head</scoring_type>
        <league_type>private</league_type>
        <renew>380_115846</renew>
        <renewed/>
        <iris_group_chat_id>FSRMWDV5KLTBDH6IPJVAY6NFNY</iris_group_chat_id>
        <short_invitation_url>https://football.fantasysports.yahoo.com/...</short_invitation_url>
        <allow_add_to_dl_extra_pos>0</allow_add_to_dl_extra_pos>
        <is_pro_league>0</is_pro_league>
        <is_cash_league>0</is_cash_league>
        <current_week>16</current_week>
        <start_week>1</start_week>
        <start_date>2019-09-05</start_date>
        <end_week>16</end_week>
        <end_date>2019-12-23</end_date>
        <is_finished>1</is_finished>
        <game_code>nfl</game_code>
        <season>2019</season>

        <settings>
            ...
        </settings>

        --------------------
        optional: draft results
        --------------------
        <draft_results>
            <draft_result>
                <pick>1</pick>
                <round>1</round>
                <team_key>390.l.240720.t.1</team_key>
                <player_key>390.p.30121</player_key>
            </draft_result>
            ...
        </draft_results>


        --------------------
        optional: players
        --------------------
        <players count="25">
            <player>
                ...
            </player>
            ...
        </players>

        --------------------
        optional: standings
        --------------------
        <standings count="25">
            <teams count="14">
                <team>
                    ...
                </team>
                ...
            </teams>
        </standings>

        --------------------
        optional: scoreboard
        --------------------
        <scoreboard>
        </scoreboard>

        --------------------
        optional: teams
        --------------------
        <teams>
        </teams>
    </league>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.leagueKey = None
        self.leagueId = None
        self.name = None
        self.url = None
        self.logoUrl = None
        self.password = None
        self.draftStatus = None
        self.numTeams = None
        self.editKey = None
        self.weeklyDeadline = None
        self.leagueUpdateTimestamp = None
        self.scoringType = None
        self.leagueType = None
        self.renew = None
        self.renewed = None
        self.irisGroupChatId = None
        self.shortInvitationUrl = None
        self.allowAddToDlExtraPos = None
        self.isProLeague = None
        self.isCashLeague = None
        self.currentWeek = None
        self.startWeek = None
        self.startDate = None
        self.endWeek = None
        self.endDate = None
        self.isFinished = None
        self.gameCode = None
        self.season = None

        #optional fields
        self.draftResults = None
        self.players = None
        self.standings = None
        self.scoreboard = None
        self.teams = None
        self.settings = None

        #run parser
        self._parse()

    def __repr__(self):
        return "LeagueResource (id: {0})".format(self.leagueId)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'leagueKey': 'yns:league_key',
            'leagueId': 'yns:league_id',
            'name': 'yns:name',
            'url': 'yns:url',
            'logoUrl': 'yns:logo_url',
            'password': 'yns:password',
            'draftStatus': 'yns:draft_status',
            'numTeams': 'yns:num_teams',
            'editKey': 'yns:edit_key',
            'weeklyDeadline': 'yns:weekly_deadline',
            'leagueUpdateTimestamp': 'yns:league_update_timestamp',
            'scoringType': 'yns:scoring_type',
            'leagueType': 'yns:league_type',
            'renew': 'yns:renew',
            'renewed': 'yns:renewed',
            'irisGroupChatId': 'yns:iris_group_chat_id',
            'shortInvitationUrl': 'yns:short_invitation_url',
            'allowAddToDlExtraPos': 'yns:allow_add_to_dl_extra_pos',
            'isProLeague': 'yns:is_pro_league',
            'isCashLeague': 'yns:is_cash_league',
            'currentWeek': 'yns:current_week',
            'startWeek': 'yns:start_week',
            'startDate': 'yns:start_date',
            'endWeek': 'yns:end_week',
            'endDate': 'yns:end_date',
            'isFinished': 'yns:is_finished',
            'gameCode': 'yns:game_code',
            'season': 'yns:season'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        # extra loading

        # draft results present
        for _pick in self.rootXML.xpath('yns:draft_results/yns:draft_result', namespaces=self.apiXMLnamespaces):
            if self.draftResults is None:
                self.draftResults = YahooResourceCollection()

            self.draftResults.append(DraftResultResource(_pick))

        # players present
        for _player in self.rootXML.xpath('yns:players/yns:player', namespaces=self.apiXMLnamespaces):
            if self.players is None:
                self.players = YahooResourceCollection()

            self.players.append(PlayerResource(_player))

        # standings present
        for _standings in self.rootXML.xpath('yns:standings', namespaces=self.apiXMLnamespaces):
            self.standings = LeagueStandingsResource(_standings)

        # scoreboard present
        for _board in self.rootXML.xpath('yns:scoreboard', namespaces=self.apiXMLnamespaces):
            if self.scoreboard is None:
                self.scoreboard = YahooResourceCollection()

            self.scoreboard.append(ScoreboardResource(_board))

        # teams present
        for _team in self.rootXML.xpath('yns:teams/yns:team', namespaces=self.apiXMLnamespaces):
            if self.teams is None:
                self.teams = YahooResourceCollection()

            self.teams.append(TeamResource(_team))

        # settings present
        for _setting in self.rootXML.xpath('yns:settings', namespaces=self.apiXMLnamespaces):
            self.settings = LeagueSettingsResource(_setting)

class DraftResultResource(YahooResource):
    '''
    Data about a draft pick.

    <draft_result>
        <pick>1</pick>
        <round>1</round>
        <team_key>390.l.240720.t.1</team_key>
        <player_key>390.p.30121</player_key>
    </draft_result>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.overallPick = None
        self.roundPick = None
        self.teamKey = None
        self.playerKey = None
        self.playerId = None
        self.teamId = None

        self._parse()

    def __repr__(self):
        return "DraftResultResource (pick: {0})".format(self.overallPick)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'overallPick': 'yns:pick',
            'roundPick': 'yns:round',
            'teamKey': 'yns:team_key',
            'playerKey': 'yns:player_key'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        #extra manipulations
        if self.playerKey:
            self.playerId = int(self.playerKey.split('.')[-1])

        if self.teamKey:
            self.teamId = int(self.teamKey.split('.')[-1])

class PlayerResource(YahooResource):
    '''
        Data about a player.

        <player>
            <player_key>380.p.3727</player_key>
            <player_id>3727</player_id>
            <name>
                <full>Adam Vinatieri</full>
                <first>Adam</first>
                <last>Vinatieri</last>
                <ascii_first>Adam</ascii_first>
                <ascii_last>Vinatieri</ascii_last>
            </name>
            <status>NA</status>
            <status_full>Inactive: Coach's Decision or Not on Roster</status_full>
            <editorial_player_key>nfl.p.3727</editorial_player_key>
            <editorial_team_key>nfl.t.11</editorial_team_key>
            <editorial_team_full_name>Indianapolis Colts</editorial_team_full_name>
            <editorial_team_abbr>Ind</editorial_team_abbr>
            <bye_weeks>
                <week>9</week>
            </bye_weeks>
            <uniform_number>4</uniform_number>
            <display_position>K</display_position>
            <headshot>
                <url>https://s.yimg.com/iu/api/res/1.2/...</url>
                <size>small</size>
            </headshot>
            <image_url>https://s.yimg.com/iu/api/res/...</image_url>
            <is_undroppable>0</is_undroppable>
            <position_type>K</position_type>
            <primary_position>K</primary_position>
            <eligible_positions>
                <position>K</position>
            </eligible_positions>
            <has_player_notes>1</has_player_notes>
            <selected_position>
                <coverage_type>date</coverage_type>
                <date>2011-07-22</date>
                <position>C</position>
            </selected_position>
            <starting_status>
                <coverage_type>date</coverage_type>
                <date>2011-07-22</date>
                <is_starting>1</is_starting>
            </starting_status>

            <player_stats>
                <coverage_type>season</coverage_type>
                <season>2009</season>
                <stats>
                    <stat>
                        <stat_id>4</stat_id>
                        <value>4388</value>
                    </stat>
                    <stat>
                        <stat_id>5</stat_id>
                        <value>34</value>
                    </stat>
                    ...
                </stats>
            </player_stats>
            <player_points>
                <coverage_type>season</coverage_type>
                <season>2009</season>
                <total>310.17</total>
            </player_points>

            <transaction_data>
                <type>pending_trade</type>
                <source_type>team</source_type>
                <source_team_key>257.l.193.t.2</source_team_key>
                <destination_type>team</destination_type>
                <destination_team_key>257.l.193.t.1</destination_team_key>
            </transaction_data>

            <transaction_data>
                <type>add</type>
                <source_type>waivers</source_type>
                <destination_type>team</destination_type>
                <destination_team_key>257.l.193.t.2</destination_team_key>
            </transaction_data>

            <transaction_data>
                <type>drop</type>
                <source_type>team</source_type>
                <source_team_key>257.l.193.t.1</source_team_key>
                <destination_type>waivers</destination_type>
            </transaction_data>
        </player>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.playerKey = None
        self.playerId = None
        self.fullName = None
        self.firstName = None
        self.lastName = None
        self.asciiFirstName = None
        self.asciiLastName = None
        self.status = None
        self.statusFull = None
        self.editorialPlayerKey = None
        self.editorialTeamKey = None
        self.editorialTeamFullName = None
        self.editorialTeamAbbr = None
        self.byeWeeks = None
        self.uniformNumber = None
        self.displayPosition = None
        self.headshotUrl = None
        self.headshotSize = None
        self.imageUrl = None
        self.isUndroppable = None
        self.positionType = None
        self.primaryPosition = None

        self.eligiblePositions = None

        self.playerStats = None
        self.playerPointCoverage = None
        self.playerPointSeason = None
        self.playerPointWeek = None
        self.playerPointDate = None
        self.playerPointTotal = None

        self.selectedPositionCoverage = None
        self.selectedPositionDate = None
        self.selectedPositionWeek = None
        self.selectedPosition = None

        self.startingStatusCoverage = None
        self.startingStatusDate = None
        self.startingStatusWeek = None
        self.startingStatusIsStarting = None

        self.transactionType = None
        self.transactionSourceType = None
        self.transactionSourceTeamKey = None
        self.transactionDestinationType = None
        self.transactionDestinationTeamKey = None

        # convenience calculated Id fields
        self.transactionSourceTeamId = None
        self.transactionDestinationTeamId = None

        self._parse()

    def __repr__(self):
        return "PlayerResource (id: {0})".format(self.playerId)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'playerKey': 'yns:player_key',
            'playerId': 'yns:player_id',
            'fullName': 'yns:name/yns:full',
            'firstName': 'yns:name/yns:first',
            'lastName': 'yns:name/yns:last',
            'asciiFirstName': 'yns:name/yns:ascii_first',
            'asciiLastName': 'yns:name/yns:ascii_last',
            'status': 'yns:status',
            'statusFull': 'yns:status_full',
            'editorialPlayerKey': 'yns:editorial_player_key',
            'editorialTeamKey': 'yns:editorial_team_key',
            'editorialTeamFullName': 'yns:editorial_team_full_name',
            'editorialTeamAbbr': 'yns:editorial_team_abbr',
            'uniformNumber': 'yns:uniform_number',
            'displayPosition': 'yns:display_position',
            'headshotUrl': 'yns:headshot/yns:url',
            'headshotSize': 'yns:headshot/yns:size',
            'imageUrl': 'yns:image_url',
            'isUndroppable': 'yns:is_undroppable',
            'positionType': 'yns:position_type',
            'primaryPosition': 'yns:position_type',
            'selectedPositionCoverage': 'yns:selected_position/yns:converage_type',
            'selectedPositionDate': 'yns:selected_position/yns:date',
            'selectedPositionWeek': 'yns:selected_position/yns:week',
            'selectedPosition': 'yns:selected_position/yns:position',
            'startingStatusCoverage': 'yns:starting_status/yns:converage_type',
            'startingStatusDate': 'yns:starting_status/yns:date',
            'startingStatusWeek': 'yns:starting_status/yns:week',
            'startingStatusIsStarting': 'yns:starting_status/yns:is_starting',
            'transactionType': 'yns:transaction_data/yns:type',
            'transactionSourceType': 'yns:transaction_data/yns:source_type',
            'transactionSourceTeamKey': 'yns:transaction_data/yns:source_team_key',
            'transactionDestinationType': 'yns:transaction_data/yns:destination_type',
            'transactionDestinationTeamKey': 'yns:transaction_data/yns:destination_team_key',
            'playerPointCoverage': 'yns:player_points/yns:coverage_type',
            'playerPointSeason': 'yns:player_points/yns:season',
            'playerPointWeek': 'yns:player_points/yns:week',
            'playerPointDate': 'yns:player_points/yns:date',
            'playerPointTotal': 'yns:player_points/yns:total',
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        # positions
        for _pos in self.rootXML.xpath('yns:eligible_positions/yns:position', namespaces=self.apiXMLnamespaces):
            if self.eligiblePositions is None:
                self.eligiblePositions = list()

            self.eligiblePositions.append(self._convert_from_string(_pos.text))

        # bye weeks
        for _bye in self.rootXML.xpath('yns:bye_weeks/yns:week', namespaces=self.apiXMLnamespaces):
            if self.byeWeeks is None:
                self.byeWeeks = list()

            self.byeWeeks.append(self._convert_from_string(_bye.text))

        # player stats
        for _stat in self.rootXML.xpath('yns:player_stats/yns:stats/yns:stat', namespaces=self.apiXMLnamespaces):
            if self.playerStats is None:
                self.playerStats = YahooResourceCollection()

            self.playerStats.append(StatResource(_stat))


        #parse out Ids from team keys
        self.transactionSourceTeamId = yahoo_api.YahooEndpoint.get_team_id_from_key(self.transactionSourceTeamKey)
        self.transactionDestinationTeamId = yahoo_api.YahooEndpoint.get_team_id_from_key(
                                                self.transactionDestinationTeamKey
                                            )

class TransactionResource(YahooResource):
    '''
        Data about a transaction between two teams.

        <transaction>
            <transaction_key>257.l.193.pt.1</transaction_key>
            <type>pending_trade</type>
            <status>proposed</status>
            <trader_team_key>257.l.193.t.2</trader_team_key>
            <tradee_team_key>257.l.193.t.1</tradee_team_key>
            <trade_proposed_time>1310694832</trade_proposed_time>
            <trade_note>This is a great trade, fo' shizzle.</trade_note>
            <players count="2">
                <player>
                    <player_key>257.p.8261</player_key>
                    <player_id>8261</player_id>
                    <name>
                    <full>Adrian Peterson</full>
                    <first>Adrian</first>
                    <last>Peterson</last>
                    <ascii_first>Adrian</ascii_first>
                    <ascii_last>Peterson</ascii_last>
                    </name>
                    <transaction_data>
                        <type>pending_trade</type>
                        <source_type>team</source_type>
                        <source_team_key>257.l.193.t.2</source_team_key>
                        <destination_type>team</destination_type>
                        <destination_team_key>257.l.193.t.1</destination_team_key>
                    </transaction_data>
                </player>
                <player>
                    <player_key>257.p.9527</player_key>
                    <player_id>9527</player_id>
                    <name>
                    <full>Arian Foster</full>
                    <first>Arian</first>
                    <last>Foster</last>
                    <ascii_first>Arian</ascii_first>
                    <ascii_last>Foster</ascii_last>
                    </name>
                    <transaction_data>
                        <type>pending_trade</type>
                        <source_type>team</source_type>
                        <source_team_key>257.l.193.t.1</source_team_key>
                        <destination_type>team</destination_type>
                        <destination_team_key>257.l.193.t.2</destination_team_key>
                    </transaction_data>
                </player>
            </players>

            <waiver_player_key>257.p.6390</waiver_player_key>
            <waiver_team_key>257.l.193.t.2</waiver_team_key>
            <waiver_date>2011-07-17</waiver_date>
            <waiver_priority>1</waiver_priority>
            <players count="2">
                <player>
                    <player_key>257.p.6390</player_key>
                    <player_id>6390</player_id>
                    <name>
                    <full>Anquan Boldin</full>
                    <first>Anquan</first>
                    <last>Boldin</last>
                    <ascii_first>Anquan</ascii_first>
                    <ascii_last>Boldin</ascii_last>
                    </name>
                    <transaction_data>
                    <type>add</type>
                    <source_type>waivers</source_type>
                    <destination_type>team</destination_type>
                    <destination_team_key>257.l.193.t.2</destination_team_key>
                    </transaction_data>
                </player>
                <player>
                    <player_key>257.p.8266</player_key>
                    <player_id>8266</player_id>
                    <name>
                    <full>Marshawn Lynch</full>
                    <first>Marshawn</first>
                    <last>Lynch</last>
                    <ascii_first>Marshawn</ascii_first>
                    <ascii_last>Lynch</ascii_last>
                    </name>
                    <transaction_data>
                    <type>drop</type>
                    <source_type>team</source_type>
                    <source_team_key>257.l.193.t.2</source_team_key>
                    <destination_type>waivers</destination_type>
                    </transaction_data>
                </player>
            </players>

        <type>add/drop</type>
        <status>successful</status>
        <timestamp>1310694660</timestamp>
            <players count="2">
                <player>
                    <player_key>257.p.7847</player_key>
                    <player_id>7847</player_id>
                    <name>
                    <full>Owen Daniels</full>
                    <first>Owen</first>
                    <last>Daniels</last>
                    <ascii_first>Owen</ascii_first>
                    <ascii_last>Daniels</ascii_last>
                    </name>
                    <transaction_data>
                    <type>add</type>
                    <source_type>freeagents</source_type>
                    <destination_type>team</destination_type>
                    <destination_team_key>257.l.193.t.1</destination_team_key>
                    </transaction_data>
                </player>
                <player>
                    <player_key>257.p.6390</player_key>
                    <player_id>6390</player_id>
                    <name>
                    <full>Anquan Boldin</full>
                    <first>Anquan</first>
                    <last>Boldin</last>
                    <ascii_first>Anquan</ascii_first>
                    <ascii_last>Boldin</ascii_last>
                    </name>
                    <transaction_data>
                    <type>drop</type>
                    <source_type>team</source_type>
                    <source_team_key>257.l.193.t.1</source_team_key>
                    <destination_type>waivers</destination_type>
                    </transaction_data>
                </player>
            </players>
        </transaction>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.transactionKey = None
        self.type = None
        self.status = None
        self.timestamp = None
        self.traderTeamKey = None
        self.tradeeTeamKey = None
        self.tradeProposedTime = None
        self.tradeNote = None
        self.waiverPlayerKey = None
        self.waiverTeamKey = None
        self.waiverDate = None
        self.waiverPriority = None

        # convenience calculated Id fields
        self.traderTeamId = None
        self.tradeeTeamId = None
        self.waiverTeamId = None

        self.players = None

        self._parse()

    def __repr__(self):
        return "TransactionResource (key: {0})".format(self.transactionKey)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'transactionKey': 'yns:transaction_key',
            'type': 'yns:type',
            'status': 'yns:status',
            'timestamp': 'yns:timestamp',
            'traderTeamKey': 'yns:trader_team_key',
            'tradeeTeamKey': 'yns:tradee_team_key',
            'tradeProposedTime': 'yns:trade_proposed_time',
            'tradeNote': 'yns:trade_note',
            'waiverPlayerKey': 'yns:waiver_player_key',
            'waiverTeamKey': 'yns:waiver_team_key',
            'waiverDate': 'yns:waiver_date',
            'waiverPriority': 'yns:waiver_priority',
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        for _player in self.rootXML.xpath('yns:players/yns:player', namespaces=self.apiXMLnamespaces):
            if self.players is None:
                self.players = YahooResourceCollection()

            self.players.append(PlayerResource(_player))

        #parse out Ids from team keys
        self.traderTeamId = yahoo_api.YahooEndpoint.get_team_id_from_key(self.traderTeamKey)
        self.tradeeTeamId = yahoo_api.YahooEndpoint.get_team_id_from_key(self.tradeeTeamKey)
        self.waiverTeamId = yahoo_api.YahooEndpoint.get_team_id_from_key(self.waiverTeamKey)

class LeagueStandingsResource(YahooResource):
    '''
        data on the standings of the teams within a league.

        <standings>
            <teams count="14">
                <team>
                ...
                </team>
                ...
            </teams>
        </standings>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.teams = None

        self._parse()

    def __repr__(self):
        return "LeagueStandingsResource"

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        for _team in self.rootXML.xpath('yns:teams/yns:team', namespaces=self.apiXMLnamespaces):
            if self.teams is None:
                self.teams = YahooResourceCollection()

            self.teams.append(TeamResource(_team))

class ScoreboardResource(YahooResource):
    '''
        A scoreboard resource.

        <scoreboard>
            <week>16</week>
            <matchups count="2">
                <matchup>
                </matchup>
            </matchups>
        </scoreboard>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.week = None
        self.matchups = None

        self._parse()

    def __repr__(self):
        return "ScoreboardResource (week: {0})".format(self.week)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'week': 'yns:week'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        #extra manipulations
        for _matchup in self.rootXML.xpath('yns:matchups/yns:matchup', namespaces=self.apiXMLnamespaces):
            if self.matchups is None:
                self.matchups = YahooResourceCollection()

            self.matchups.append(MatchupResource(_matchup))

class TeamResource(YahooResource):
    '''

        <team>
            <team_key>223.l.431.t.1</team_key>
            <team_id>1</team_id>
            <name>PFW - Blunda</name>
            <url>https://football.fantasysports.yahoo.com/archive/pnfl/2009/431/1</url>
            <team_logos>
                <team_logo>
                    <size>medium</size>
                    <url>https://l.yimg.com/a/i/us/sp/fn/default/full/nfl/icon_01_48.gif</url>
                </team_logo>
            </team_logos>
            <division_id>2</division_id>
            <faab_balance>22</faab_balance>
            <managers>
                <manager>
                    <manager_id>13</manager_id>
                    <nickname>Michael Blunda</nickname>
                    <guid>XNAXQZRDZPJ3RVFMY7ZTSWEFLU</guid>
                </manager>
            </managers>
            <team_points>
                <coverage_type>week</coverage_type>
                <week>1</week>
                <total>117.88</total>
            </team_points>
            <team_projected_points>
                <coverage_type>week</coverage_type>
                <week>1</week>
                <total>107.94</total>
            </team_projected_points>
            <roster>
                ...
            </roster>
            <team_standings>
                <rank>1</rank>
                <outcome_totals>
                    <wins>9</wins>
                    <losses>4</losses>
                    <ties>0</ties>
                    <percentage>.692</percentage>
                </outcome_totals>
                <divisional_outcome_totals>
                    <wins>5</wins>
                    <losses>1</losses>
                    <ties>0</ties>
                </divisional_outcome_totals>
            </team_standings>
        </team>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.teamKey = None
        self.teamId = None
        self.name = None
        self.url = None
        self.teamLogoSize = None
        self.teamLogoUrl= None
        self.divisionId = None
        self.faabBalance = None
        self.pointsCoverage = None
        self.pointsWeek = None
        self.pointsTotal = None
        self.projectedPointsCoverage = None
        self.projectedPointsWeek = None
        self.projectedPointsTotal = None
        self.rank = None
        self.overallWin = None
        self.overallLoss = None
        self.overallTie = None
        self.overallWinPct = None
        self.divisionWin = None
        self.divisionLoss = None
        self.divisionTie = None


        self.managers = None
        self.matchups = None
        self.roster = None
        self.teamStats = None

        self._parse()

    def __repr__(self):
        return "TeamResource (id: {0})".format(self.teamId)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'teamKey': 'yns:team_key',
            'teamId': 'yns:team_id',
            'name': 'yns:name',
            'url': 'yns:url',
            'teamLogoSize': 'yns:team_logos/yns:team_logo/yns:size',
            'teamLogoUrl': 'yns:team_logos/yns:team_logo/yns:url',
            'divisionId': 'yns:division_id',
            'faabBalance': 'yns:faab_balance',
            'pointsCoverage': 'yns:team_points/yns:converage_type',
            'pointsWeek': 'yns:team_points/yns:week',
            'pointsTotal': 'yns:team_points/yns:total',
            'projectedPointsCoverage': 'yns:team_projected_points/yns:converage_type',
            'projectedPointsWeek': 'yns:team_projected_points/yns:week',
            'projectedPointsTotal': 'yns:team_projected_points/yns:total',
            'rank': 'yns:team_standings/yns:rank',
            'overallWin': 'yns:team_standings/yns:outcome_totals/yns:wins',
            'overallLoss': 'yns:team_standings/yns:outcome_totals/yns:losses',
            'overallTie': 'yns:team_standings/yns:outcome_totals/yns:ties',
            'overallWinPct': 'yns:team_standings/yns:outcome_totals/yns:percentage',
            'divisionWin': 'yns:team_standings/yns:divisional_outcome_totals/yns:wins',
            'divisionLoss': 'yns:team_standings/yns:divisional_outcome_totals/yns:losses',
            'divisionTie': 'yns:team_standings/yns:divisional_outcome_totals/yns:ties'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        #special cases
        for _manager in self.rootXML.xpath('yns:managers/yns:manager', namespaces=self.apiXMLnamespaces):
            if self.managers is None:
                self.managers = YahooResourceCollection()

            self.managers.append(ManagerResource(_manager))

        for _matchup in self.rootXML.xpath('yns:matchups/yns:matchup', namespaces=self.apiXMLnamespaces):
            if self.matchups is None:
                self.matchups = YahooResourceCollection()

            self.matchups.append(MatchupResource(_matchup))

        for _roster in self.rootXML.xpath('yns:roster', namespaces=self.apiXMLnamespaces):
            self.roster = TeamRosterResource(_roster)

        for _stat in self.rootXML.xpath('yns:team_stats', namespaces=self.apiXMLnamespaces):
            self.matchups.append(TeamStatsResource(_stat))

class TeamRosterResource(YahooResource):
    '''
        A teams roster resource.

        <roster>
            <coverage_type>date</coverage_type>
            <date>2011-07-22</date>
            <players count="22">
                <player>
                    ...
                </player>
                ...
            </players>
        </roster>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.coverageType = None
        self.date = None
        self.week = None
        self.players = None

        self._parse()

    def __repr__(self):
        return "TeamRosterResource"

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'coverageType': 'yns:coverage_type',
            'date': 'yns:date',
            'week': 'yns:week'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        #extra manipulations
        if self.date :
            self.date = dt.datetime.strptime(self.date, "%Y-%m-%d")

        for _player in self.rootXML.xpath('yns:players/yns:player', namespaces=self.apiXMLnamespaces):
            if self.players is None:
                self.players = YahooResourceCollection()

            self.players.append(PlayerResource(_player))

class MatchupResource(YahooResource):
    '''
        Data about a matchup between two teams.

        <matchup>
            <week>1</week>
            <status>postevent</status>
            <is_tied>0</is_tied>
            <winner_team_key>223.l.431.t.1</winner_team_key>
            <teams count="2">
            </teams>
        </matchup
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.week = None
        self.status = None
        self.isTied = None
        self.winnerTeamKey = None
        self.teams = None

        self._parse()

    def __repr__(self):
        return "MatchupResource (week: {0})".format(self.week)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'week': 'yns:week',
            'status': 'yns:status',
            'isTied': 'yns:is_tied',
            'winnerTeamKey': 'yns:winner_team_key'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        for _team in self.rootXML.xpath('yns:teams/yns:team', namespaces=self.apiXMLnamespaces):
            if self.teams is None:
                self.teams = YahooResourceCollection()

            self.teams.append(TeamResource(_team))

class ManagerResource(YahooResource):
    '''
        Data about a manager.

        <manager>
            <manager_id>13</manager_id>
            <nickname>Michael Blunda</nickname>
            <guid>XNAXQZRDZPJ3RVFMY7ZTSWEFLU</guid>
            <is_commissioner>1</is_commissioner>
            <is_current_login>1</is_current_login>
            <exposed_yahoo_id>1</exposed_yahoo_id>
        </manager>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.managerId = None
        self.nickname = None
        self.guid = None
        self.isCurrentLogin = None
        self.exposedYahooId = None
        self.isCommissioner = None

        self._parse()

    def __repr__(self):
        return "ManagerResource (id: {0})".format(self.managerId)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'managerId': 'yns:manager_id',
            'nickname': 'yns:nickname',
            'guid': 'yns:guid',
            'isCurrentLogin': 'yns:is_current_login',
            'exposedYahooId': 'yns:exposed_yahoo_id',
            'isCommissioner': 'yns:is_commissioner'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class GameKeyResource(YahooResource):
    '''
        Data about a game key.

        <game>
            <game_key></game_key>
            <code></code>
            <season></season>
        </game>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.gameKey = None
        self.code = None
        self.season = None

        self._parse()

    def __repr__(self):
        return "GameKeyResource (key: {0}, code: {1}, season: {2})".format(self.gameKey, self.code, self.season)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'gameKey': 'yns:game_key',
            'code': 'yns:code',
            'season': 'yns:season'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class StatCategoryResource(YahooResource):
    '''
        Data about a stat category.

        <league>
            <settings>
                <stat_categories>
                    <stats>
                        <stat>
                            <stat_id>4</stat_id>
                            <enabled>1</enabled>
                            <name>Passing Yards</name>
                            <display_name>Pass Yds</display_name>
                            <sort_order>1</sort_order>
                            <position_type>O</position_type>
                            <is_only_display_stat>1</is_only_display_stat>
                        </stat>
                    </stats>
                </stat_categories>
            </settings>
        </league>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.statId = None
        self.enabled = None
        self.name = None
        self.displayName = None
        self.sortOrder = None
        self.postitionType = None
        self.isOnlyDisplayStat = None

        self._parse()

    def __repr__(self):
        return "StatCategoryResource (statId: {0}, name: {1})".format(self.statId, self.name)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'statId': 'yns:stat_id',
            'enabled': 'yns:enabled',
            'name': 'yns:name',
            'displayName': 'yns:display_name',
            'sortOrder': 'yns:sort_order',
            'postitionType': 'yns:position_type',
            'isOnlyDisplayStat': 'yns:is_only_display_stat'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class StatModifierResource(YahooResource):
    '''
        Data about a stat modifier.

        <league>
            <settings>
                <stat_modifiers>
                    <stats>
                        <stat>
                            <stat_id>4</stat_id>
                            <value>0.04</value>
                        </stat>
                    </stats>
                </stat_modifiers>
            </settings>
        </league>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.statId = None
        self.value = None

        self._parse()

    def __repr__(self):
        return "StatModifierResource (statId: {0}, value: {1})".format(self.statId, self.value)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'statId': 'yns:stat_id',
            'value': 'yns:value'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class StatResource(YahooResource):
    '''
        Data about a stat.

        <stats>
            <stat>
                <stat_id>4</stat_id>
                <value>10</value>
            </stat>
        </stats>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.statId = None
        self.value = None

        self._parse()

    def __repr__(self):
        return "StatResource (statId: {0}, value: {1})".format(self.statId, self.value)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'statId': 'yns:stat_id',
            'value': 'yns:value'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class RosterPostionResource(YahooResource):
    '''
        Data about a roster position.

        <league>
            <settings>
                <roster_positions>
                    <roster_position>
                        <position>QB</position>
                        <count>1</count>
                    </roster_position>
                    <roster_position>
                        <position>WR</position>
                        <count>3</count>
                    </roster_position>
                    ...
                </roster_positions>
            </settings>
        </league>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.position = None
        self.count = None

        self._parse()

    def __repr__(self):
        return "RosterPostionResource (position: {0})".format(self.position)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'position': 'yns:position',
            'count': 'yns:count'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class LeagueDivisionResource(YahooResource):
    '''
        Data about a league division.

        <league>
            <settings>
                <divisions>
                    <division>
                        <division_id>1</division_id>
                        <name>Family</name>
                    </division>
                    <division>
                        <division_id>2</division_id>
                        <name>Friends</name>
                    </division>
                </divisions>
            </settings>
        </league>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.divisionId = None
        self.name = None

        self._parse()

    def __repr__(self):
        return "LeagueDivisionResource (divisionId: {0})".format(self.divisionId)

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'divisionId': 'yns:division_id',
            'name': 'yns:name'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

class LeagueSettingsResource(YahooResource):
    '''
    <league>
        <settings>
        <draft_type>live</draft_type>
        <scoring_type>head</scoring_type>
        <uses_playoff>1</uses_playoff>
        <playoff_start_week>14</playoff_start_week>
        <uses_playoff_reseeding>0</uses_playoff_reseeding>
        <uses_lock_eliminated_teams>0</uses_lock_eliminated_teams>
        <uses_faab>1</uses_faab>
        <trade_end_date>2009-11-27</trade_end_date>
        <trade_ratify_type>commish</trade_ratify_type>
        <trade_reject_time>0</trade_reject_time>
        <roster_positions>
            <roster_position>
            <position>QB</position>
            <count>1</count>
            </roster_position>
            <roster_position>
            <position>WR</position>
            <count>3</count>
            </roster_position>
            <roster_position>
            <position>RB</position>
            <count>2</count>
            </roster_position>
            <roster_position>
            <position>TE</position>
            <count>1</count>
            </roster_position>
            <roster_position>
            <position>W/R/T</position>
            <count>1</count>
            </roster_position>
            <roster_position>
            <position>K</position>
            <count>1</count>
            </roster_position>
            <roster_position>
            <position>DEF</position>
            <count>1</count>
            </roster_position>
            <roster_position>
            <position>BN</position>
            <count>4</count>
            </roster_position>
        </roster_positions>
        <stat_categories>
            <stats>
            <stat>
                <stat_id>4</stat_id>
                <enabled>1</enabled>
                <name>Passing Yards</name>
                <display_name>Pass Yds</display_name>
                <sort_order>1</sort_order>
                <position_type>O</position_type>
            </stat>
            <stat>
                <stat_id>5</stat_id>
                <enabled>1</enabled>
                <name>Passing Touchdowns</name>
                <display_name>Pass TD</display_name>
                <sort_order>1</sort_order>
                <position_type>O</position_type>
            </stat>
            ...
            </stats>
        </stat_categories>
        <stat_modifiers>
            <stats>
            <stat>
                <stat_id>4</stat_id>
                <value>0.04</value>
            </stat>
            <stat>
                <stat_id>5</stat_id>
                <value>4</value>
            </stat>
            ...
            </stats>
        </stat_modifiers>
        <divisions>
            <division>
            <division_id>1</division_id>
            <name>Family</name>
            </division>
            <division>
            <division_id>2</division_id>
            <name>Friends</name>
            </division>
        </divisions>
        </settings>
    </league>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.draftType = None
        self.scoringType = None
        self.usesPlayoff = None
        self.playoffStartWeek = None
        self.usesPlayoffReseeding = None
        self.usesLockEliminatedTeams = None
        self.usesFaab = None
        self.tradeEndDate = None
        self.tradeRatifyType = None
        self.tradeRejectTime = None
        self.rosterPositions = None
        self.statCategories = None
        self.statModifiers = None
        self.divisions = None

        self._parse()

    def __repr__(self):
        return "LeagueSettingsResource"

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'draftType': 'yns:draft_type',
            'scoringType': 'yns:scoring_type',
            'usesPlayoff': 'yns:uses_playoff',
            'playoffStartWeek': 'yns:playoff_start_week',
            'usesPlayoffReseeding': 'yns:uses_playoff_reseeding',
            'usesLockEliminatedTeams': 'yns:usesLockEliminatedTeams',
            'usesFaab': 'yns:uses_faab',
            'tradeEndDate': 'yns:trade_end_date',
            'tradeRatifyType': 'yns:trade_ratify_type',
            'tradeRejectTime': 'yns:trade_reject_time'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        # extra loading

        # roster positions present
        for _slot in self.rootXML.xpath('yns:roster_positions/yns:roster_position', namespaces=self.apiXMLnamespaces):
            if self.rosterPositions is None:
                self.rosterPositions = YahooResourceCollection()

            self.rosterPositions.append(RosterPostionResource(_slot))

        # stat category present
        for _cat in self.rootXML.xpath('yns:stat_categories/yns:stats/yns:stat', namespaces=self.apiXMLnamespaces):
            if self.statCategories is None:
                self.statCategories = YahooResourceCollection()

            self.statCategories.append(StatCategoryResource(_cat))

        # stat modifier present
        for _mod in self.rootXML.xpath('yns:stat_modifiers/yns:stats/yns:stat', namespaces=self.apiXMLnamespaces):
            if self.statModifiers is None:
                self.statModifiers = YahooResourceCollection()

            self.statModifiers.append(StatModifierResource(_mod))

        # divisions present
        for _div in self.rootXML.xpath('yns:divisions/yns:division', namespaces=self.apiXMLnamespaces):
            if self.divisions is None:
                self.divisions = YahooResourceCollection()

            self.divisions.append(LeagueDivisionResource(_div))

class TeamStatsResource(YahooResource):
    '''
    this appears to be team stats for a roto-style baseball league?

    <team>
        <team_stats>
            <coverage_type>date</coverage_type>
            <date>2011-07-06</date>
            <stats>
                <stat>
                    <stat_id>60</stat_id>
                    <value>13/31</value>
                </stat>
                <stat>
                    <stat_id>7</stat_id>
                    <value>9</value>
                </stat>
                ...
            </stats>
        </team_stats>
    </team>
    '''

    def __init__(self, apiElement):

        super().__init__(apiElement)

        self.coverageType = None
        self.week = None
        self.date = None
        self.stats = None

        self._parse()

    def __repr__(self):
        return "TeamStatsResource"

    def _parse(self):
        '''

        note: the xpath paths must specify the namespace prefix. the
        base class gives a default yns for the normal yahoo fantasy api
        default namespace.

        '''

        xPathDataMapping = {
            'coverageType': 'yns:coverage_type',
            'week': 'yns:week',
            'date': 'yns:date'
        }

        self._parse_from_xpath_mapping(self.rootXML, xPathDataMapping)

        # extra loading

        # roster positions present
        for _stat in self.rootXML.xpath('yns:stats/yns:stat', namespaces=self.apiXMLnamespaces):
            if self.stats is None:
                self.stats = YahooResourceCollection()

            self.stats.append(StatResource(_stat))

def query_yahoo_api(yahooSession, yahooEndpoint):
    '''
    query the yahoo endpoint and load results into the appropriate
    resource objects.

    returns either a YahooResource-derived object or a YahooResourceCollection
    of them.
    '''

    # input verification
    #
    if not isinstance(yahooSession, yahoo_auth.YahooOAuthSession):
        raise ValueError('yahooSession must be an instance of YahooOAuthSession.')

    if not isinstance(yahooEndpoint, yahoo_api.YahooEndpoint):
        raise ValueError('yahooEnpoint must be an instance of YahooEndpoint.')

    #
    # logger
    #
    logger = yahoo_log.set_up_logger('query_yahoo_api')

    #
    # load api response
    #
    logger.debug('Querying endpoint: {0}'.format(str(yahooEndpoint)))
    apiResponse = yahooEndpoint.get(yahooSession)
    rootXML = YahooResource.load_xml_from_api_response(apiResponse)
    namespace = {'yns':'http://fantasysports.yahooapis.com/fantasy/v2/base.rng'}

    #
    # load results based on endpoint
    #
    if isinstance(yahooEndpoint, yahoo_api.YahooEndpointGames):

        _gamesList = YahooResourceCollection()
        for _game in rootXML.xpath('/yns:fantasy_content/yns:games/yns:game', namespaces=namespace):
            _gamesList.append(GameKeyResource(_game))

        return _gamesList

    elif (isinstance(yahooEndpoint, yahoo_api.YahooEndpointTransaction) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointTransactions)):

        _transactionList = YahooResourceCollection()
        for _transaction in rootXML.xpath('//yns:transaction', namespaces=namespace):
            _transactionList.append(TransactionResource(_transaction))

        return _transactionList

    elif (isinstance(yahooEndpoint, yahoo_api.YahooEndpointLeague) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointLeagueDraft) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointPlayers) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointLeagueSettings) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointLeagueStandings) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointLeagueScoreboard) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointPlayerStats) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointTeams)):

        return LeagueResource(rootXML.xpath('/yns:fantasy_content/yns:league', namespaces=namespace)[0])

    elif (isinstance(yahooEndpoint, yahoo_api.YahooEndpointTeamMatchups) or
          isinstance(yahooEndpoint, yahoo_api.YahooEndpointTeamRoster)):

        return TeamResource(rootXML.xpath('/yns:fantasy_content/yns:team', namespaces=namespace)[0])

    else:
        raise ValueError("Unknown YahooEndpoint type.")

def main():
    '''
    entrypoint (used for basic testing)
    '''
    with open('yahoo.creds', 'r') as f:
        session = yahoo_auth.YahooOAuthSession.create_from_token(f.read())

    from yahoofantasysports import SETTINGS
    SETTINGS['logLevel'] = logging.DEBUG


    endpoint = yahoo_api.YahooEndpointPlayerStats()
    resp = query_yahoo_api(session, endpoint)
    print(resp.players[0].to_dict())
    print(len(resp.players))


    with open('yahoo.creds','w') as f:
        f.write(session.token_to_json())

if __name__ == '__main__':
    main()
