'''
Command line parsing and functions.
'''

import argparse

import yahoofantasysports.oauth

def oauth_authorize(args):
    '''
    function to run for command

        yahoofantasysports.cli oauth authorize
    '''

    session = yahoofantasysports.oauth.YahooOAuthSession(
        clientId=args.clientId,
        clientSecret=args.clientSecret
    )

    if args.outputTokenJsonString:
        print(session.token_to_json())

    if args.outputTokenFile:
        with open(args.outputTokenFile,'w') as f:
            f.write(session.token_to_json())

def build_cli_parser():
    '''
    build the argparse.ArgumentParser to use for this module.
    '''

    parser = argparse.ArgumentParser(
        prog='yahoofantasysports.cli',
        description='Convenience commands and functions for the yahoofantasysports module.'
    )

    subparsers = parser.add_subparsers()

    oauthParser = subparsers.add_parser('oauth')
    oauthSubParsers = oauthParser.add_subparsers()

    ###
    # yahoofantasysports.cli oauth authorize
    #
    ###
    oauthAuthorize = oauthSubParsers.add_parser(
        'authorize',
        description='Authorize an oauth session and return the retrieved tokens.'
    )
    oauthAuthorize.add_argument('--clientId', action='store', help='The Yahoo API client ID.')
    oauthAuthorize.add_argument('--clientSecret', action='store', help='The Yahoo API client secret.')
    oauthAuthorize.add_argument(
        '--outputTokenJsonString',
        action='store_true',
        help='Print the retrieved tokens as JSON.'
    )
    oauthAuthorize.add_argument(
        '--outputTokenFile',
        action='store',
        help='Output the retrieved tokens to a text file.'
    )
    oauthAuthorize.set_defaults(func=oauth_authorize)

    return parser

def main():
    ''' entrypoint. '''

    parser = build_cli_parser()
    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()
