'''
Endpoint objects for building requests to interact with the Yahoo Fantasy API.

YahooEndpoint has a gameKeyMap class attribute that must be updated as
new fantasy game years are created.
'''

import datetime as dt
import abc
import time
import requests

import yahoofantasysports.oauth as yahoo_auth
import yahoofantasysports.log as yahoo_log


class YahooEndpoint(abc.ABC):
    '''
    Base class for defining yahoo api endpoints. Implementation classes
    must define the abstract methods.

    An API endpoint request/response may be executed using the get() method.
    If only the Request object is needed, use build_request().

    All requests to the API require OAuth authorization.

    self.opts:  a dictionary of key,value pairs holding API query parameters.
                key = parameter name
                value = parameter value.

    '''

    # constant class attribute for mapping gameCodes and years
    # to game keys.
    # Update this as new years are available.
    #   (gameCode,year) -> gameKey
    #
    gameKeyMap = {
        ("nfl",1999): 50,
        ("nfl",2000): 53,
        ("nfl",2001): 57,
        ("nfl",2002): 49,
        ("nfl",2003): 79,
        ("nfl",2004): 101,
        ("nfl",2005): 124,
        ("nfl",2006): 153,
        ("nfl",2007): 175,
        ("nfl",2008): 199,
        ("nfl",2009): 222,
        ("nfl",2010): 242,
        ("nfl",2011): 257,
        ("nfl",2012): 273,
        ("nfl",2013): 314,
        ("nfl",2014): 331,
        ("nfl",2015): 348,
        ("nfl",2016): 359,
        ("nfl",2017): 371,
        ("nfl",2018): 380,
        ("nfl",2019): 390,
        ("nfl",2020): 399,
        ("mlb",1999): 10,
        ("mlb",2000): 11,
        ("mlb",2001): 12,
        ("mlb",2002): 39,
        ("mlb",2003): 74,
        ("mlb",2004): 98,
        ("mlb",2005): 113,
        ("mlb",2006): 147,
        ("mlb",2007): 171,
        ("mlb",2008): 195,
        ("mlb",2009): 215,
        ("mlb",2010): 238,
        ("mlb",2011): 253,
        ("mlb",2012): 268,
        ("mlb",2013): 308,
        ("mlb",2014): 328,
        ("mlb",2015): 346,
        ("mlb",2016): 357,
        ("mlb",2017): 370,
        ("mlb",2018): 378,
        ("mlb",2019): 388,
        ("mlb",2020): 398,
        ("mlb",2021): 404,
        ("nba",1999): 22,
        ("nba",2000): 26,
        ("nba",2001): 16,
        ("nba",2002): 67,
        ("nba",2003): 95,
        ("nba",2004): 112,
        ("nba",2005): 131,
        ("nba",2006): 165,
        ("nba",2007): 187,
        ("nba",2008): 211,
        ("nba",2009): 234,
        ("nba",2010): 249,
        ("nba",2011): 265,
        ("nba",2012): 304,
        ("nba",2013): 322,
        ("nba",2014): 342,
        ("nba",2015): 353,
        ("nba",2016): 364,
        ("nba",2017): 375,
        ("nba",2018): 385,
        ("nba",2019): 395,
        ("nba",2020): 402,
        ("nhl",2000): 61,
        ("nhl",2001): 15,
        ("nhl",2002): 64,
        ("nhl",2003): 94,
        ("nhl",2004): 111,
        ("nhl",2005): 130,
        ("nhl",2006): 164,
        ("nhl",2007): 186,
        ("nhl",2008): 210,
        ("nhl",2009): 233,
        ("nhl",2010): 248,
        ("nhl",2011): 263,
        ("nhl",2012): 303,
        ("nhl",2013): 321,
        ("nhl",2014): 341,
        ("nhl",2015): 352,
        ("nhl",2016): 363,
        ("nhl",2017): 376,
        ("nhl",2018): 386,
        ("nhl",2019): 396,
        ("nhl",2020): 403
    }

    def __init__(self, gameCode, responseFormat='xml'):
        '''
        gameCode        yahoo game code. must be one of [None, 'nfl', 'mlb', 'nba', 'nhl']
        responseFormat may be 'xml' or 'json'
        '''

        if gameCode not in [None, 'nfl', 'mlb', 'nba', 'nhl']:
            raise ValueError('gameCode must be one of [None, nfl, mlb, nba, nhl].')

        if responseFormat not in ['xml', 'json']:
            raise ValueError("responseFormat must be one of [xml,json]")

        self._baseURL = 'https://fantasysports.yahooapis.com/fantasy/v2'
        self._responseFormat = responseFormat
        self.gameCode = gameCode

        # opts:  a dictionary of key,value pairs holding API query parameters.
        #        key = parameter name
        #        value = parameter value.
        #
        # params:  a dictionary of key,value pairs for url parameters (after ?)
        self.opts = dict()
        self.params = dict()

        # add responseFormat to params
        self.params['format'] = self._responseFormat

        # logging
        #
        self.logger = yahoo_log.set_up_logger(type(self).__name__)

        # retry counter for rate limited request attempts
        #
        self.retryCounter = 0
        self.retryLimit = 10

    def __repr__(self):
        return "{0}".format(type(self).__name__)

    def _build_query_string(self, apiUrl):
        '''
        construct query string out of parameters and self.opts and append to apiUrl.

        The options dict self.opts is used to build options.

        API url uses semicolon delimited parameter strings, not normal URL query parameters
        that uses the ? delimiter. This method builds the compliant URL from the api URL and
        a dictionary of parameters. The format= parameter is the only one after a ?.
        '''

        if self.opts:
            if not isinstance(self.opts, dict):
                raise ValueError("opts must be a dictionary of parameter key-value pairs.")

            if len(self.opts) > 0:
                paramString = ';'.join([key + '=' + str(val) for (key,val) in self.opts.items()])
                paramString = ';' + paramString
            else:
                paramString = ''
        else:
            paramString = ''

        return '{0}{1}'.format(apiUrl, paramString)

    def _get_game_key(self, season):

        # game key maps are kept at the sports_scrapers.yahoo module level

        if (self.gameCode,season) not in self.gameKeyMap:
            raise ValueError("Unknown game key.")

        return self.gameKeyMap[(self.gameCode,season)]

    def _build_league_key(self, season, league):

        return '{0}.l.{1}'.format(
            self._get_game_key(season),
            league
        )

    def _build_team_key(self, season, league, teamId):

        return '{0}.l.{1}.t.{2}'.format(
            self._get_game_key(season),
            league,
            teamId
        )

    def _build_player_key(self, season, playerId):
        '''
        <gamekey>.p.<player id>
        '''

        return '{0}.p.{1}'.format(
            self._get_game_key(season),
            playerId
        )

    @classmethod
    def get_team_id_from_key(cls, teamKey):
        '''Isolate team id number from the yahoo team key format.'''
        if teamKey is None:
            return None
        else:
            return int(teamKey.split('.')[-1])

    def get(self, yahooSession):
        '''
        Retrieve endpoint data from Yahoo API. If a rate limiting response
        is received, will retry up to a limit using exponential delay backoff.

        yahooSession        An authorized instance of YahooOAuthSession containing API
                            access tokens and credentials.

        returns a requests.Response object with API response.
        '''

        if not isinstance(yahooSession, yahoo_auth.YahooOAuthSession):
            raise ValueError('yahooSession must be an yahoo_auth.YahooOAuthSession instance.')

        req = self.build_request()
        self.logger.debug('Requesting: {0} with header {1} params {2}'.format(
                req.url,
                req.headers,
                req.params
            )
        )

        resp = yahooSession.request(
            req.method,
            req.url,
            data=req.data,
            headers=req.headers,
            params=req.params
        )

        # handle response codes
        #
        if resp.status_code == 429 or resp.status_code == 999:
            # rate limit exceeded. pause and retry
            #

            # exponential backoff, stop after retry limit
            delay = 15 + 2.5 ** self.retryCounter

            if self.retryCounter > self.retryLimit:
                raise RuntimeError('Retry limit ({0}) reached.'.format(self.retryLimit))

            self.logger.info('Request denied due to rate limit. Retrying in {0} seconds...'.format(delay))
            time.sleep(delay)
            self.retryCounter += 1
            return self.get(yahooSession)

        if resp.status_code == 400:
            raise RuntimeError('API response code: {0}. Check request url and params.'.format(resp.status_code))

        if resp.status_code == 403:
            raise RuntimeError('API response code: {0}. Auth denied. '
                               'Check OAuth credentials are valid.'.format(resp.status_code))

        if resp.status_code != 200:
            raise RuntimeError('API response code: {0}'.format(resp.status_code))

        self.logger.debug('Response code: {0}. OK.'.format(resp.status_code))

        return resp

    def build_request(self):
        '''
        returns a requests.Request object with necessary info to
        query the API through an authenticated session with get().
        '''

        return requests.Request(
            'GET',
            url=self._build_query_string(self._build_api_url()),
            params=self.params
        )

    @abc.abstractmethod
    def _build_api_url(self):
        '''
        build the endpoint api url that the request should be sent to.
        Do not include the build query, just the api url. The opts will
        be added automatically in build_request().
        '''
        pass

class YahooEndpointGames(YahooEndpoint):
    '''
        get collection of game resources
    '''

    def __init__(self, isAvailable=None, gameTypes=[], gameCodes=[], seasons=[], gameKeys=[], responseFormat='xml'):
        '''
            isAvailable:   may be either True or False. True means that the game is in season
            gameTypes:     list of game types to retrieve. must be one of
                           ['full','pickem-team','pickem-group','pickem-team-list']
            gameCodes:     list of game codes to retrieve. must be one of ['nfl', 'mlb', 'nba', 'nhl']
            seasons:       list of integer years to retrieve.
            gameKeys:      list of game keys to retrieve. An integer number.
        '''

        super().__init__(None, responseFormat=responseFormat)

        if isAvailable is not None:
            if isAvailable not in [0, 1]:
                raise ValueError('is_available may only be 0 or 1.')

        #build filter string

        if isAvailable is not None:
            if isAvailable:
                self.opts['is_available'] = 1
            else:
                self.opts['is_available'] = 0

        if len(gameTypes) > 0:
            for _type in gameTypes:
                if _type not in ['full','pickem-team','pickem-group','pickem-team-list']:
                    raise ValueError("invalide game type given. must be one of "
                                     "'full','pickem-team','pickem-group','pickem-team-list'.")

            self.opts['game_types'] = ','.join(gameTypes)

        if len(gameCodes) > 0:
            for _type in gameCodes:
                if _type not in ['nfl', 'mlb', 'nba', 'nhl']:
                    raise ValueError("invalide game code given. must be one of 'nfl', 'mlb', 'nba', 'nhl'.")

            self.opts['game_codes'] = ','.join(gameCodes)

        if len(seasons) > 0:
            self.opts['seasons'] = ','.join([str(x) for x in seasons])

        if len(gameKeys) > 0:
            self.opts['game_keys'] = ','.join([str(x) for x in gameKeys])

    def _build_api_url(self):
        return '{0}/games'.format(self._baseURL)

class YahooEndpointTransaction(YahooEndpoint):
    '''
        retrieve data from the transaction api resource

        https://fantasysports.yahooapis.com/fantasy/v2/transaction/{trans_key}

        Completed transactions: .l.{league_id}.tr.{transaction_id}
        Example:pnfl.l.431.tr.26 or 223.l.431.tr.26

        Waiver claims: .l.{league_id}.w.c.{claim_id}
        Example:257.l.193.w.c.2_6390

        Pending trades: .l.{league_id}.pt.{pending_trade_id}
        Example:257.l.193.pt.1
    '''

    def __init__(self, league, season, transactionId, gameCode, transType='completed', responseFormat='xml'):
        '''
        league          the league ID number. Found on the league settings page.
        season          integer season year
        gameCode        yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        transType       one of ['completed','waiver','pending_trade']
        '''

        if transType not in ['completed','waiver','pending_trade']:
            raise ValueError('not a valid type')

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season
        self.transactionId = transactionId
        self.transType = transType

    def _build_api_url(self):

        transTypeStr = ''
        if self.transType == 'completed':
            transTypeStr = 'tr'

        if self.transType == 'waiver':
            transTypeStr = 'w.c'

        if self.transType == 'pending_trade':
            transTypeStr = 'pt'

        transactionKey = '{0}.{2}.{3}'.format(
            self._build_league_key(self.season, self.league),
            transTypeStr,
            self.transactionId
        )

        return '{0}/transaction/{1}'.format(self._baseURL, transactionKey)

class YahooEndpointTransactions(YahooEndpoint):
    '''
    retrieve data from the transactions collection api resource

    This is a collection of transaction resources

    https://fantasysports.yahooapis.com/fantasy/v2/league/223.l.431/transactions;transaction_keys={game_key}.l.{league_id}.{trans_key}
        see docs for transaction key formats

    transaction_keys                              transaction_keys=key1,key2,...

    filters for transactions
        type (add,drop,commish,trade)             /transactions;type=add
        types                                     /transactions;types=add,trade
        team_key                                  /transactions;team_key=257.l.193.t.1
        type (waiver,pending_trade) & team_key    /transactions;team_key=257.l.193.t.1;type=waiver
        count                                     /transactions;count=5

    '''

    def __init__(self, league, season, gameCode, transactionKeys=None, transactionTypes=None,
                       teamId=None, teamTransType=None, count=None, responseFormat='xml'):
        '''
        league                              the league ID number. Found on the league settings page.
        season                              integer season year
        gameCode                            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        transactionKeys                     list of transaction keys
        transactionTypes                    list of transaction types
        teamId                              id of the team to get transactions for (fantasy team)
        teamTransType                       must have teamId defined for this
        count                               number of transactions to return
        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

        if transactionKeys and isinstance(transactionKeys, list):
            self.opts['transaction_keys'] = ','.join(transactionKeys)

        if transactionTypes and isinstance(transactionTypes, list):
            self.opts['types'] = ','.join(transactionTypes)

        if teamId:
            self.opts['team_key'] = '{0}.l.{1}.t.{2}'.format(
                self._get_game_key(self.season),
                self.league,
                teamId
            )

        if teamId and teamTransType:
            self.opts['type'] = teamTransType

        if count:
            self.opts['count'] = count

    def _build_api_url(self):

        return '{0}/league/{1}/transactions'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointLeague(YahooEndpoint):
    '''
        get the root league resource
    '''
    def __init__(self, league, season, gameCode, responseFormat='xml'):
        '''
            league                  the league ID number. Found on the league settings page.
            season                  integer season year
            gameCode                yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        '''

        # no options for this one

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

    def _build_api_url(self):

        return '{0}/league/{1}'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointLeagueSettings(YahooEndpoint):
    '''
        get the league settings resource
    '''

    def __init__(self, league, season, gameCode, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        '''

        # no options for this one

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

    def _build_api_url(self):

        return '{0}/league/{1}/settings'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league))

class YahooEndpointLeagueStandings(YahooEndpoint):
    '''
        get the league standings resource
    '''

    def __init__(self, league, season, gameCode, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        '''

        # no options for this one

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

    def _build_api_url(self):

        return '{0}/league/{1}/standings'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointLeagueScoreboard(YahooEndpoint):
    '''
        get the league scoreboard resource
    '''

    def __init__(self, league, season, gameCode, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        '''

        # no options for this one

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

    def _build_api_url(self):

        return '{0}/league/{1}/scoreboard'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointLeagueDraft(YahooEndpoint):
    '''
        get the draft results for that league
    '''

    def __init__(self, league, season, gameCode, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        '''

        # no options for this one

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

    def _build_api_url(self):

        return '{0}/league/{1}/draftresults'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointTeam(YahooEndpoint):
    '''
        get team from a league

        team_key format = <gamekey>.l.<league id>.t.<team id>
    '''

    def __init__(self, league, season, teamId, gameCode, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            teamId              integer
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
        '''

        # no options for this one

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season
        self.teamId = teamId

    def _build_api_url(self):

        return '{0}/team/{1}'.format(
            self._baseURL,
            self._build_team_key(self.season, self.league, self.teamId)
        )

class YahooEndpointTeams(YahooEndpoint):
    '''
        get teams from a league
    '''

    def __init__(self, league, season, gameCode, teamKeys=None, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            teamKeys            list of teamKeys keys
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']

            team_key format = <gamekey>.l.<league id>.t.<team id>

            teamKeys                    team_keys=key1,key2,...
        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

        if teamKeys and isinstance(teamKeys, list):
            self.opts['team_keys'] = ','.join(teamKeys)

    def _build_api_url(self):

        return '{0}/league/{1}/teams'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointTeamMatchups(YahooEndpoint):
    '''
        get team matchups (no weeks parameter will return all)

        team_key format = <gamekey>.l.<league id>.t.<team id>
    '''

    def __init__(self, league, season, teamId, gameCode, weeks=None, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            teamId              integer
            week                list of integer weeks (h2h leagues)
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']

        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season
        self.teamId = teamId

        if weeks and isinstance(weeks, list):
            self.opts['weeks'] = ','.join(weeks)

    def _build_api_url(self):

        return '{0}/team/{1}/matchups'.format(
            self._baseURL,
            self._build_team_key(self.season, self.league, self.teamId)
        )

class YahooEndpointTeamStats(YahooEndpoint):
    '''
        get team stats

        team_key format = <gamekey>.l.<league id>.t.<team id>
    '''

    def __init__(self, league, season, teamId, gameCode, statType=None, statDate=None, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            teamId              integer
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
            statType            one of [season, date]
            statDate            date (datetime object)

        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season
        self.teamId = teamId

        if statType and statType in ['season', 'date']:
            self.opts['type'] = statType

        #in YYYY-MM-DD format
        if statDate and isinstance(statDate, dt.datetime):
            self.opts['stat_date'] = statDate.strptime("%Y-%m-%d")

    def _build_api_url(self):

        return '{0}/team/{1}/stats'.format(
            self._baseURL,
            self._build_team_key(self.season, self.league, self.teamId)
        )

class YahooEndpointTeamRoster(YahooEndpoint):
    '''
        get team roster

        team_key format = <gamekey>.l.<league id>.t.<team id>
    '''

    def __init__(self, league, season, teamId, gameCode, week=None, date=None, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            teamId              integer
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
            week                the integer week number (for weekly games)
            date                date (datetime object)

            no week or date gives the current roster

        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season
        self.teamId = teamId

        if week is not None and date is not None:
            raise ValueError("both week and date cannot be set.")

        if week:
            self.opts['week'] = week

        if date and isinstance(date, dt.datetime):
            self.opts['date'] = date.strptime("%Y-%m-%d")

    def _build_api_url(self):

        return '{0}/team/{1}/roster'.format(
            self._baseURL,
            self._build_team_key(self.season, self.league, self.teamId)
        )

class YahooEndpointPlayers(YahooEndpoint):
    '''
        get the Players Resource

        team_key format = <gamekey>.l.<league id>.t.<team id>

        https://fantasysports.yahooapis.com/fantasy/v2/league/223.l.431/players;player_keys=223.p.5479
        root - player_key (game_key.p.{id})
        /stats

        filters:
            positions (QB,RB, etc)                          /players;position={}
            status (A [all], FA [free agenst],              /players;status={}
                    W [waivers], T [taken], K[keeper])
            search (search for name)                        /players;search={}
            sort (stat_id, NAME, OR, AR, PTS)               /players;sort={}
            sortType  (season, date, week,
                       lastweek, lastmonth)                 /players;sort_type={}
            sortSeason (year)                               /players;sort_type=season;sort_season={}
            sortDate                                        /players;sort_type=date;sort_date={}
            sortWeek                                        /players;sort_type=week;sort_week={}
            start (start number)                            /players;start=25
            count (number to get)                           /players;count=5

            use start and count for paging
    '''

    def __init__(self, league, season, gameCode, playerKeys=None, positions=None,
                 status=None, search=None, sort=None, sortType=None,
                 sortSeason=None, sortDate=None, sortWeek=None, start=None,
                 count=None, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
            playerKeys          list of player keys to retrieve
            positions           list of positions to include. options are [qb, rb, wr, te, etc]
            status              player status to include. one of [A, FA, W, T, K]
            search              string player name
            sort                one of stat id number, name, OR (overall rank), AR (actual rank), pts (points)
            sortType            one of [season, date, week, lastweek, lastmonth]
            sortDate            date to show (mlb, nba nhl)
            sortWeek            week to show (nfl)
            start               integer 0 or greater, number in list to start on
            count               integer. number to retrieve (max 25, can use start to paginate)

        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

        if playerKeys and isinstance(playerKeys, list):
            self.opts['player_keys'] = ','.join(playerKeys)

        if positions and isinstance(positions, list):
            self.opts['position'] = ','.join(positions)

        if status:
            if status not in ['A', 'FA', 'W', 't']:
                raise ValueError('invalid status.')

            self.opts['status'] = status

        if search:
            self.opts['search'] = search

        if sort:
            self.opts['sort'] = sort

        if sortType:
            self.opts['sort_type'] = sortType

        if sortSeason:
            self.opts['sort_season'] = sortSeason

        if sortDate:
            self.opts['sort_date'] = sortDate

        if sortWeek:
            self.opts['sort_week'] = sortWeek

        if start:
            self.opts['start'] = start

        if count:
            self.opts['count'] = count

    def _build_api_url(self):

        return '{0}/league/{1}/players'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

class YahooEndpointPlayerStats(YahooEndpoint):
    '''
        get player stats

        will only return a max of 25
    '''

    def __init__(self, league, season, playerKeys, gameCode,
                coverageType=None, coverageWeek=None, coverageDate=None, responseFormat='xml'):
        '''
            league              the league ID number. Found on the league settings page.
            season              integer season year
            playerKeys          list of player_keys of format <gamekey>.p.<player id>
            gameCode            yahoo game code. must be one of ['nfl', 'mlb', 'nba', 'nhl']
            coverageType        one of [season, date, week]
            coverageDate        stat coverage date (mlb, nba nhl)
            coverageWeek        stat coverage week (nfl)
        '''

        super().__init__(gameCode, responseFormat=responseFormat)

        self.league = league
        self.season = season

        if playerKeys:
            if not isinstance(playerKeys, list):
                raise ValueError('playerKeys must be a list.')

            self.opts['player_keys'] = ','.join(playerKeys)

        # the options on the stats must be added after the /stats portion
        #  of the url, so a separate special case dict is needed.
        #
        self.statOpts = dict()

        if coverageType:
            if coverageType not in ['season', 'week', 'date']:
                raise ValueError('invalid coverage type.')

            self.statOpts['type'] = coverageType

        if coverageWeek:
            self.statOpts['week'] = int(coverageWeek)

        if coverageDate:
            raise ValueError('coverageDate option not implemented.')
            #self.statOpts['date'] =

    def _build_api_url(self):

        return '{0}/league/{1}/players'.format(
            self._baseURL,
            self._build_league_key(self.season, self.league)
        )

    def build_request(self):

        # override the base class build_request() since the
        # url is a bit different than usual
        #
        # options on the stats must be defined after the /stats
        #
        if len(self.statOpts) > 0:
            paramString = ';'.join([key + '=' + str(val) for (key,val) in self.statOpts.items()])
            paramString = ';' + paramString
        else:
            paramString = ''

        return requests.Request(
            'GET',
            url=self._build_query_string(self._build_api_url()) + '/stats' + paramString
        )

def main():
    '''
    entrypoint
    '''
    pass

if __name__ == '__main__':
    main()
