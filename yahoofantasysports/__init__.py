SETTINGS=dict()

# logging settings
import logging
SETTINGS['logLevel'] = logging.INFO
SETTINGS['logType'] = 'console'
