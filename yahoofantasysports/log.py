'''
logging support for the module.
'''

import logging
from yahoofantasysports import SETTINGS


def set_up_logger(loggerName):
    '''
        initialize the logger
    '''

    formatString = '[%(asctime)s][%(name)s][%(levelname)s] %(message)s'

    logger = logging.getLogger(loggerName)

    # if logger has handlers, remove them and rebuild
    if logger.hasHandlers:
        for _handler in logger.handlers:
            logger.removeHandler(_handler)

    # set up logger
    if SETTINGS['logType'] == 'console':
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(
            logging.Formatter(formatString)
        )
        logger.addHandler(consoleHandler)

    logger.setLevel(SETTINGS['logLevel'])

    return logger
