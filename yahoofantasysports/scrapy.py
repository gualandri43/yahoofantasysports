'''
This module provides base spiders and middleware to make using the
API in scrapy easier.
'''

import scrapy
from oauthlib.oauth2 import TokenExpiredError

import yahoofantasysports.oauth as yahoo_oauth

class YahooOAuthDownloaderMiddleware(object):
    '''
    Automatically add the Yahoo Oauth authorization header to all requests.
    Refresh token if reponse indicates a permission denied, and requeue request.

    Uses instance attribute spider.yahooSession to store Oauth session. It is
    built on spider_opened().

    Spider settings required:
        YAHOO_API_CLIENT_ID
        YAHOO_API_CLIENT_SECRET
        YAHOO_API_TOKEN_JSON
    '''

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=scrapy.signals.spider_opened)
        s.yahooSession = None
        return s

    def process_request(self, request, spider):
        '''
        modify request to include the OAuth authorization header


        Called for each request that goes through the downloader
        middleware.

        Must either:
        - return None: continue processing this request
        - or return a Response object
        - or return a Request object
        - or raise IgnoreRequest: process_exception() methods of
          installed downloader middleware will be called
        '''

        if spider.crawler.stats.get_value('access_denied_retries', default=0) > 5:
            raise scrapy.exceptions.CloseSpider("Access token invalid or unable to be refreshed.")

        try:
            url, headers, data = self.yahooSession._client.add_token(
                request.url,
                http_method=request.method,
                body=request.data,
                headers=request.headers
            )

        # Attempt to retrieve and save new access token if expired
        except TokenExpiredError:

            newToken = self.yahooSession.refresh_token()
            url, headers, data = self.yahooSession._client.add_token(
                request.url,
                http_method=request.method,
                body=request.data,
                headers=request.headers
            )


        return scrapy.Request(
            url=url,
            headers=headers,
            data=data,
            meta=request.meta,
            method=request.method,
            cookies=request.cookies,
            callback=request.callback,
            errback=request.errback,
            priority=request.priority
        )

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest

        #
        # catch a 401 response code (access token is incorrect)
        #     force a token refresh
        #     reschedule the request if token is refreshed
        if response.status in [401, 403]:

            newToken = self.yahooSession.refresh_token()
            spider.crawler.stats.inc_value('access_denied_retries', start=0)

            return request
        else:
            #reset counter if response is something else
            spider.crawler.stats.set_value('access_denied_retries', 0)

        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):

        # build yahoo oauth session.
        #

        tokenJson = spider.settings.get('YAHOO_API_TOKEN_JSON')
        clientId = spider.settings.get('YAHOO_API_CLIENT_ID')
        clientSecret = spider.settings.get('YAHOO_API_CLIENT_SECRET')

        if tokenJson is None or clientId is None or clientSecret is None:
            raise scrapy.exceptions.CloseSpider("YAHOO_API_* settings not found.")

        self.yahooSession = yahoo_oauth.YahooOAuthSession.create_from_token(
            tokenJson,
            clientId=clientId,
            clientSecret=clientSecret
        )
