'''
package build properties.
'''

from setuptools import setup, find_packages


setup(
    name="yahoofantasysports",
    version="0.2.3",
    author="Daniel Gualandri",
    packages=find_packages(),
    install_requires=[
        "lxml",
        "scrapy",
        "requests",
        "oauthlib",
        "requests_oauthlib"
    ],
    entry_points = {
        'console_scripts': [
            'yahoofantasysports.cli=yahoofantasysports.cli:main'
        ]
    }
)
