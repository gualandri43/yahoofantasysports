from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean ForeignKey
from sqlalchemy.orm import Session, relationship
from sqlalchemy.ext.declarative import declarative_base

BASE = declarative_base()

class DraftPicks(BASE):
    '''Class to hold draft pick data.'''

    __tablename__ = 'fantasy_draft_picks'
    __table_args__ = {'schema':'scraper_yahoo'}

    game_key = Column(Integer, primary_key=True)
    league_id = Column(Integer, primary_key=True)
    pick = Column(Integer, primary_key=True)
    player_id = Column(Integer)
    draft_round = Column(Integer)
    fantasy_team_id = Column(Integer)

class FantasyTeams(BASE):
    '''Class to hold fantasy team data.'''

    __tablename__ = 'fantasy_teams'
    __table_args__ = {'schema':'scraper_yahoo'}

    game_key = Column(Integer, primary_key=True)
    league_id = Column(Integer, primary_key=True)  
    team_id = Column(Integer, primary_key=True)

class FantasyManagers(BASE):
    '''Class to hold fantasy manager data.'''

    __tablename__ = 'fantasy_managers'
    __table_args__ = {'schema':'scraper_yahoo'}

    manager_id = Column(Integer, primary_key=True)
    name = Column(String(255))

class FantasyTeamManager(BASE):
    '''Class to map fantasy team to manager data.'''

    __tablename__ = 'fantasy_team_manager'
    __table_args__ = {'schema':'scraper_yahoo'}

    manager_id = Column(Integer, primary_key=True)
    team_id = Column(Integer, primary_key=True)
    date_started = Column(DateTime, primary_key=True)

class Players(BASE):
    '''Class to hold player data.'''

    __tablename__ = 'players'
    __table_args__ = {'schema':'scraper_yahoo'}

    player_id = Column(Integer, primary_key=True)
    first_name = Column(String(255))
    last_name = Column(String(255))
    suffix = Column(String(255))

class Teams(BASE):
    '''Class to hold team (nfl/mlb/etc) data.'''

    __tablename__ = 'teams'
    __table_args__ = {'schema':'scraper_yahoo'}

    team_id = Column(Integer, primary_key=True)
    city = Column(String(255))
    name = Column(String(255))

class PlayerTeams(BASE):
    '''Class to map players to teams data.'''

    __tablename__ = 'player_teams'
    __table_args__ = {'schema':'scraper_yahoo'}

    team_id = Column(Integer, primary_key=True)
    player_id = Column(Integer, primary_key=True)
    is_current = Column(Boolean)

