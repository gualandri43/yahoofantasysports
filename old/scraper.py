'''
command line entrypoint for yahoo API scrapers

'''


import argparse
import scrapy.crawler as scrapy_crawler

import sports_modules.sports_scraper.support.scrapy as scrapy_support
import sports_modules.sports_scraper.yahoo.yahoo_scrapy.spiders as yahoo_spiders


def _run_single_spider(spider, args):
    '''
    run a single spider
    '''

    settings = scrapy_support.load_parser_args_into_settings(args)

    process = scrapy_crawler.CrawlerProcess(settings=settings)
    process.crawl(spider)
    process.start()

def build_main_parser(parser):
    '''
    main_parser() creates the parent parser and all sub parsers

    parser is an argparse.ArgumentParser class and this routine adds all necessary
    descriptions and arguments to it.
    '''

    if not isinstance(parser, argparse.ArgumentParser):
            raise TypeError("parser should be an argparse.ArgumentParser")

    parser.description = ('Scrape the Yahoo Fantasy API for various league datasets. '
                            'Each command line arg can also be defined by an environment variable '
                            'with the same name. _FILE may also be appended to each environment variable '
                            'to specify a path to read the variable value from. This file must only have '
                            'the variable contents in it. Command line defined arguments take precedence '
                            'over environment variables.')

    subparsers = parser.add_subparsers()


    _draftResults = subparsers.add_parser('draftresults', help= 'Scrape draft results from Yahoo API.')
    yahoo_spiders.YahooDraftScraper._add_args_to_parser(_draftResults)
    _draftResults.set_defaults(
        func=lambda x: _run_single_spider(yahoo_spiders.YahooDraftScraper, x)
    )


    _transactions = subparsers.add_parser('transactions', help= 'Scrape transactions data from Yahoo API.')
    yahoo_spiders.YahooTransactionsScraper._add_args_to_parser(_transactions)
    _transactions.set_defaults(
        func=lambda x: _run_single_spider(yahoo_spiders.YahooTransactionsScraper, x)
    )


    _allPlayers = subparsers.add_parser('allplayers', help= 'Scrape all players from Yahoo API.')
    yahoo_spiders.YahooGetAllPlayersScraper._add_args_to_parser(_allPlayers)
    _allPlayers.set_defaults(
        func=lambda x: _run_single_spider(yahoo_spiders.YahooGetAllPlayersScraper, x)
    )


    _matchups = subparsers.add_parser('matchup', help= 'Scrape matchups for a given team from Yahoo API.')
    yahoo_spiders.YahooTeamMatchupsScraper._add_args_to_parser(_matchups)
    _matchups.set_defaults(
        func=lambda x: _run_single_spider(yahoo_spiders.YahooTeamMatchupsScraper, x)
    )

    _rosters = subparsers.add_parser('roster', help= 'Scrape team roster for a given team from Yahoo API.')
    yahoo_spiders.YahooTeamRoserScraper._add_args_to_parser(_rosters)
    _rosters.set_defaults(
        func=lambda x: _run_single_spider(yahoo_spiders.YahooTeamRoserScraper, x)
    )

def main():
    '''
    CLI entry point
    '''

    parser = argparse.ArgumentParser()
    build_main_parser(parser)
    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()
