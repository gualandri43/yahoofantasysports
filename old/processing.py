import json
import datetime as dt

import sports_modules.support.search as search_support

class BaseProcesser:
    '''
    common processing methods.
    '''
    def _get_first_nested_result(self, data, key):
        _resultList = list(search_support.find_nested_dict(data, key))

        if len(_resultList) <= 0:
            return None
        else:
            return _resultList[0]

    def _get_first_nested_result_value(self, data, key):
        foundDict = self._get_first_nested_result(data, key)
        if foundDict is None:
            return None
        else:
            if key in foundDict:
                return foundDict[key]
            else:
                return None

class TransactionDataPostprocesser(BaseProcesser):
    '''
    process yahoo transactions API response data into
    better formatted records.

    creates a recordset with a record for each player or pick that changed teams.
        therefore, a single yahoo transaction is made up of multiple records with the
        same transaction_id
    '''

    def _process_player(self, playerData):
        '''
        playerData     dictionary holding yahoo api info about a single player

        returns a records with info about player

        "player": [
                    [
                        {
                            "player_key": "390.p.25427"
                        },
                        {
                            "player_id": "25427"
                        },
                        {
                            "name": {
                                "full": "Dan Bailey",
                                "last": "Bailey",
                                "first": "Dan",
                                "ascii_last": "Bailey",
                                "ascii_first": "Dan"
                            }
                        },
                        {
                            "editorial_team_abbr": "Min"
                        },
                        {
                            "display_position": "K"
                        },
                        {
                            "position_type": "K"
                        }
                    ],

                ]
        '''

        _record = {}

        _record["player_key"] = self._get_first_nested_result_value(playerData, "player_key")
        _record["player_id"] = int(self._get_first_nested_result_value(playerData, "player_id"))
        _record["position"] = self._get_first_nested_result_value(playerData, "display_position")
        _record["full_name"] = self._get_first_nested_result(playerData, "name")["name"]["full"]

        return _record

    def _process_pick_trade(self, pickData):
        '''
        playerData     dictionary holding yahoo api info about a single player

        returns a records with info about player

        {
            "pick": {
                "round": "11",
                "source_team_key": "390.l.240720.t.9",
                "source_team_name": "MARCO garopPOLO",
                "original_team_key": "390.l.240720.t.9",
                "original_team_name": "MARCO garopPOLO",
                "destination_team_key": "390.l.240720.t.6",
                "destination_team_name": "Retired"
            }
        }
        '''

        _record = {}

        _record["round"] = self._get_first_nested_result_value(pickData, "round")
        _record["source_team_key"] = self._get_first_nested_result_value(pickData, "source_team_key")
        _record["source_team_name"] = self._get_first_nested_result_value(pickData, "source_team_name")
        _record["original_team_key"] = self._get_first_nested_result_value(pickData, "original_team_key")
        _record["original_team_name"] = self._get_first_nested_result_value(pickData, "original_team_name")
        _record["destination_team_key"] = self._get_first_nested_result_value(pickData, "destination_team_key")
        _record["destination_team_name"] = self._get_first_nested_result_value(pickData, "destination_team_name")

        return _record

    def _process_player_trans_data(self, playerData):
        '''
        playerData     dictionary holding yahoo api info about a single player

        returns a records with info about player transaction details

        "player": [

                    {
                        "transaction_data": [
                            {
                                "type": "add",
                                "source_type": "freeagents",
                                "destination_type": "team",
                                "destination_team_key": "390.l.240720.t.7",
                                "destination_team_name": "Los McStuds"
                            }
                        ]
                    }
                ]
        ---------------------------------------
        "source_type": "team",
        "source_team_key": "390.l.240720.t.3",
        "destination_type": "waivers",
        "source_team_name": "KandidKamara"
        '''

        _record = {}

        transDetail = self._get_first_nested_result(playerData, "transaction_data")
        _record["player_transaction_type"] = self._get_first_nested_result_value(transDetail, "type")
        _record["source_type"] = self._get_first_nested_result_value(transDetail, "source_type")
        _record["destination_type"] = self._get_first_nested_result_value(transDetail, "destination_type")
        _record["source_team_key"] = self._get_first_nested_result_value(transDetail, "source_team_key")
        _record["destination_team_key"] = self._get_first_nested_result_value(transDetail, "destination_team_key")
        _record["source_team_name"] = self._get_first_nested_result_value(transDetail, "source_team_name")
        _record["destination_team_name"] = self._get_first_nested_result_value(transDetail, "destination_team_name")

        return _record

    def _process_add_drop(self, transactionData):
        '''
        transactionData     dictionary holding yahoo api info about a single transaction

        returns a set of records for the transaction
        '''

        _metaRecord = {}

        # extract metadata
        metaData = self._get_first_nested_result(transactionData, "transaction_id")

        _metaRecord["transaction_type"] = metaData["type"]
        _metaRecord["status"] = metaData["status"]
        _metaRecord["timestamp"] = metaData["timestamp"]
        _metaRecord["timestamp_str"] = dt.datetime.fromtimestamp(int(metaData["timestamp"])).strftime("%Y-%m-%d")
        _metaRecord["transaction_id"] = metaData["transaction_id"]
        _metaRecord["transaction_key"] = metaData["transaction_key"]

        _records = []

        #player data
        for _player in search_support.find_nested_dict(transactionData, "player"):
            _playerRecord = {}
            _playerRecord.update(_metaRecord)
            _playerRecord.update(self._process_player(_player))
            _playerRecord.update(self._process_player_trans_data(_player))

            _records.append(_playerRecord)

        return _records

    def _process_trade(self, transactionData):
        '''
        transactionData     dictionary holding yahoo api info about a single transaction

        returns a set of records for the transaction
        '''

        _metaRecord = {}

        # extract metadata
        metaData = self._get_first_nested_result(transactionData, "transaction_id")

        _metaRecord["transaction_type"] = metaData["type"]
        _metaRecord["status"] = metaData["status"]
        _metaRecord["timestamp"] = metaData["timestamp"]
        _metaRecord["timestamp_str"] = dt.datetime.fromtimestamp(int(metaData["timestamp"])).strftime("%Y-%m-%d")
        _metaRecord["transaction_id"] = metaData["transaction_id"]
        _metaRecord["transaction_key"] = metaData["transaction_key"]
        _metaRecord["tradee_team_key"] = metaData["tradee_team_key"]
        _metaRecord["trader_team_key"] = metaData["trader_team_key"]
        _metaRecord["tradee_team_name"] = metaData["tradee_team_name"]
        _metaRecord["trader_team_name"] = metaData["trader_team_name"]

        _records = []

        #picks part of trade
        picksData = self._get_first_nested_result(transactionData, "picks")
        if picksData:
            for _pick in search_support.find_nested_dict(picksData, "pick"):
                _pickRecord = {}
                _pickRecord.update(_metaRecord)
                _pickRecord.update(self._process_pick_trade(_pick))

                _records.append(_pickRecord)

        #player data
        for _player in search_support.find_nested_dict(transactionData, "player"):
            _playerRecord = {}
            _playerRecord.update(_metaRecord)
            _playerRecord.update(self._process_player(_player))
            _playerRecord.update(self._process_player_trans_data(_player))

            _records.append(_playerRecord)

        return _records

    def _process_commish(self, transactionData):
        '''
        transactionData     dictionary holding yahoo api info about a single transaction

        commish transactions do not seem to have much data recorded.... this basically records metadata

        returns a set of records for the transaction
        '''

        _metaRecord = {}

        # extract metadata
        metaData = self._get_first_nested_result(transactionData, "transaction_id")

        _metaRecord["transaction_type"] = metaData["type"]
        _metaRecord["status"] = metaData["status"]
        _metaRecord["timestamp"] = metaData["timestamp"]
        _metaRecord["timestamp_str"] = dt.datetime.fromtimestamp(int(metaData["timestamp"])).strftime("%Y-%m-%d")
        _metaRecord["transaction_id"] = metaData["transaction_id"]
        _metaRecord["transaction_key"] = metaData["transaction_key"]

        _records = []
        _records.append(_metaRecord)

        return _records

    def get_records(self, apiData):
        '''
        create a set of records from API response data
        '''

        _records = []
        _transactionsData = self._get_first_nested_result_value(apiData, "transactions")

        for _transaction in search_support.find_nested_dict(_transactionsData, "transaction"):
            _transType = self._get_first_nested_result_value(_transaction, "type")

            if _transType == "add/drop" or _transType == "add" or _transType == "drop":
                _records.extend(self._process_add_drop(_transaction))

            if _transType == "trade":
                _records.extend(self._process_trade(_transaction))

            if _transType == "commish":
                _records.extend(self._process_commish(_transaction))

        return _records

class DraftDataPostprocessor:
    '''
    process yahoo draft API response data into
    better formatted records.

    creates a recordset with a record for each draft pick
    '''

    def get_records(self, apiData):

        picks = list()
        for _pick in search_support.find_nested_dict(apiData, "draft_result"):
            _pickInfo = {}
            _pickInfo["overallPick"] = int(_pick["draft_result"]["pick"])
            _pickInfo["roundPick"] = int(_pick["draft_result"]["round"])
            _pickInfo["team_key"] = _pick["draft_result"]["team_key"]
            _pickInfo["player_key"] = _pick["draft_result"]["player_key"]
            _pickInfo["player_id"] = int(_pickInfo["player_key"].split(".")[-1])
            _pickInfo["team_id"] = int(_pickInfo["team_key"].split(".")[-1])

            picks.append(_pickInfo)

        return picks

class PlayerDataPostprocessor(BaseProcesser):
    '''
    process yahoo player API response data into
    better formatted records.

    creates a recordset with a record for each player
    '''

    def _process_player_batch(self, batchData, rowPerPosition=False):
        '''
        player data is queried in chunks of 25. this method processes
        one chunk and returns a list of formatted records.
        '''

        _records = []

        for _player in search_support.find_nested_dict(batchData, "player"):
            _record = {}

            _record["player_key"] = self._get_first_nested_result_value(_player, "player_key")
            _record["player_id"] = int(self._get_first_nested_result_value(_player, "player_id"))
            _record["full_name"] = self._get_first_nested_result(_player, "name")["name"]["full"]
            _record["first_name"] = self._get_first_nested_result(_player, "name")["name"]["first"]
            _record["last_name"] = self._get_first_nested_result(_player, "name")["name"]["last"]
            _record["team_key"] = self._get_first_nested_result_value(_player, "editorial_team_key")
            _record["team"] = self._get_first_nested_result_value(_player, "editorial_team_abbr")

            #{"eligible_positions":[{"position":"1B"},{"position":"CI"},{"position":"Util"}]}
            eligiblePositions = []
            for _eligible in search_support.find_nested_dict(_player, "eligible_positions"):
                for _pos in _eligible['eligible_positions']:
                    eligiblePositions.append(_pos['position'])

            _record["eligible_positions"] = eligiblePositions

            if rowPerPosition:
                for _pos in _record["eligible_positions"]:
                    newRecord = dict(_record)
                    newRecord['position'] = _pos
                    _records.append(newRecord)
            else:
                _records.append(_record)

        return _records

    def get_records(self, apiData, rowPerPosition=False):
        '''
        rowPerPosition:     if true, a if a player has more than one
                            eligible position, that player's record is copied
                            and one record is made for each position.
        '''

        if not isinstance(apiData, list):
            raise ValueError("A list is expected.")

        players = list()
        for _chunk in apiData:
            players.extend(self._process_player_batch(_chunk, rowPerPosition=rowPerPosition))

        return players

class TeamDataPostprocessor:
    '''
    process yahoo team API response data into
    better formatted records.

    creates a recordset with a record for each team
    '''

    def get_records(self, apiData):

        teamList = list()
        for _team in search_support.find_nested_dict(apiData, "team"):
            _teamInfo = {}
            _teamInfo["team_id"] = int(list(search_support.find_nested_dict(_team, "team_id"))[0]["team_id"])
            _teamInfo["team_name"] = list(search_support.find_nested_dict(_team, "name"))[0]["name"]
            _teamInfo["manager"] = list(search_support.find_nested_dict(_team, "manager"))[0]["manager"]['nickname']

            teamList.append(_teamInfo)

        return teamList

if __name__ == "__main__":
    with open("C:/Users/guala/Desktop/players.json") as f:
        data = json.load(f)

        #processor = TransactionDataPostprocesser()
        #print(len(processor.get_records(data)))

        processor = PlayerDataPostprocessor()
        players = processor.get_records(data, rowPerPosition=True)
        print(len(players))

        #playerWithPos = []
        #for player in players:
        #    eligible = player.pop('eligible_positions')
        #    for pos in eligible:
        #        newPlayer = dict(player)
        #        newPlayer['pos'] = pos
        #        playerWithPos.append(newPlayer)

        import pandas
        df = pandas.DataFrame.from_records(players)

        print(df.head(15))

        #import pandas
        #df = pandas.DataFrame.from_records(processor.get_records(data))
        #print(df[df['transaction_id']=='447'])
