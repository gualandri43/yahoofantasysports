"""
modified from josebrunello to use requests_oauthlib and only include OAuth2
"""

import time
import json
import os

import logging
import webbrowser

import requests_oauthlib
from requests_oauthlib import OAuth2Session

import sports_modules.sports_scraper.yahoo.enums as yahoo_enums

def json_write(jsonData, filename):
    """Write json data into a file
    """
    with open(filename, 'w') as f:
        json.dump(jsonData, f, indent=4, sort_keys=True, ensure_ascii=False)
        return True
    return False

def json_read(filename):
    """Get data from json file
    """
    with open(filename) as f:
        jsonData = json.load(f)
        return jsonData

    return False

class AccessTokenError(Exception):
    '''
    custom catch-all token error exception
    '''

    def __init__(self, message='The provided access token failed to authorize access.'):
        super(AccessTokenError, self).__init__(message)

class YahooCredentials:
    '''
    manage data needed for accessing yahoo resources
    '''

    def __init__(self):
        self.consumerKey = None
        self.consumerSecret = None
        self.accessToken = None
        self.refreshToken = None
        self.guid = None
        self.tokenTime = None
        self.tokenType = None
        self.expiresIn = None

        self.log = logging.getLogger('YahooCredentials')

    def load_token_dict(self, token):
        '''
        load from an oauth token dictionary
        '''

        self.log.debug('Loading token information from token dict.')

        self.accessToken = token['access_token']
        self.refreshToken = token['refresh_token']
        self.tokenType = token['token_type']
        self.expiresIn = token['expires_in']

    def get_token_dict(self):
        '''
        create an oauth token dict
        '''

        self.log.debug('Creating token dict from members.')

        token = {}
        token['access_token'] = self.accessToken
        token['refresh_token'] = self.refreshToken
        token['token_type'] = self.tokenType
        token['expires_in'] = self.expiresIn

        return token

    def read(self, filepath):
        '''
        read credential info from file
        '''

        self.log.debug('Loading credentials from file.')

        data = json_read(filepath)

        if not data:
            return None

        self.consumerKey = data['consumer_key']
        self.consumerSecret = data['consumer_secret']
        self.accessToken = data['access_token']
        self.refreshToken = data['refresh_token']
        self.guid = data['guid']
        self.tokenTime = data['token_time']
        self.tokenType = data['token_type']
        self.expiresIn = data['expires_in']

    def write(self, filepath):
        '''
        write credential info to file
        '''

        self.log.debug('Writing credentials to file.')

        data = {}
        data['consumer_key'] = self.consumerKey
        data['consumer_secret'] = self.consumerSecret
        data['access_token'] = self.accessToken
        data['refresh_token'] = self.refreshToken
        data['guid'] = self.guid
        data['token_time'] = self.tokenTime
        data['token_type'] = self.tokenType
        data['expires_in'] = self.expiresIn

        json_write(data, filepath)

    def create_new_creds_file(self, consumerKey, consumerSecret, outPath):
        '''
        create a new creds file using a API key and secret
        '''

        self.consumerKey = consumerKey
        self.consumerSecret = consumerSecret
        self.write(outPath)

class YahooOAuth2:
    '''
    class to manage an OAuth2 connection to Yahoo
    '''

    def __init__(self, credFilepath):

        self.log = logging.getLogger('YahooOAuth2')

        self.creds = YahooCredentials()
        self.credFilepath = credFilepath

        if os.path.isfile(credFilepath):
            self.creds.read(credFilepath)
        else:
            # create an empty creds file
            self.creds.write(credFilepath)

        if self.creds.consumerKey is None or self.creds.consumerSecret is None:
            raise ValueError("The consumer key and secret must be defined in the credentials file.")

        self.oauth = OAuth2Session(self.creds.consumerKey,
                                   redirect_uri=yahoo_enums.YAHOO_API_ENDPOINTS.CALLBACK_URI.value,
                                   token=self.creds.get_token_dict())

    def refresh_access_token(self):
        """Refresh access token

            yahoo refresh endpoint docs:
                https://developer.yahoo.com/oauth2/guide/flows_authcode/#step-5-exchange-refresh-token-for-new-access-token=

            oauth refresh example with extras:
                https://requests-oauthlib.readthedocs.io/en/latest/examples/real_world_example_with_refresh.html
        """

        self.log.info('Refreshing Access Token')

        #yahoo required refresh parameters
        extra = {
            'client_id': self.creds.consumerKey,
            'client_secret': self.creds.consumerSecret,
        }

        self.creds.tokenTime = time.time()

        token = self.oauth.refresh_token(yahoo_enums.YAHOO_API_ENDPOINTS.ACCESS_TOKEN_URL.value,
                                         refresh_token=self.creds.refreshToken,
                                         **extra)

        self.creds.load_token_dict(token)
        self.creds.write(self.credFilepath)

    def authorize_access(self):
        '''
        initial access authorization

        only needed once, then the refresh token can be used from then on
        '''

        self.log.info('Running Access Authorization.')

        self.creds.tokenTime = time.time()

        authorizationUrl, state = self.oauth.authorization_url(yahoo_enums.YAHOO_API_ENDPOINTS.AUTHORIZE_TOKEN_URL.value)
        webbrowser.open(authorizationUrl)
        verifier = input("Enter verifier : ")

        self.log.info('Obtaining Access Token.')

        token = self.oauth.fetch_token(yahoo_enums.YAHOO_API_ENDPOINTS.ACCESS_TOKEN_URL.value,
                                       code=verifier,
                                       client_secret=self.creds.consumerSecret)

        self.creds.load_token_dict(token)
        self.creds.write(self.credFilepath)

    def get_data(self, url, params=None):
        '''
        retrieve yahoo data from given url
        '''

        self.log.info('Requesting data from url: {0}'.format(url))

        if params is None:
            opts = {}

        #force json format response
        opts['format'] = 'json'

        try:
            resp = self.oauth.get(url, params=opts)
            resp.raise_for_status()

        except requests_oauthlib.TokenExpiredError as expiredTokenError:
            self.log.info('Token expired. Try refreshing.')
            self.log.info(expiredTokenError.message)

            #refresh token and try again
            self.refresh_access_token()

            try:
                self.get_data(url, params=opts)
            except requests_oauthlib.TokenExpiredError as secondExpiredTokenError:
                self.log.info('Token still expired even after refreshing.')
                self.log.info(secondExpiredTokenError.message)
                raise AccessTokenError

        except requests_oauthlib.AccessDeniedError as noAccessError:
            self.log.info('Access denied.')
            self.log.info(noAccessError.message)

            raise AccessTokenError

        except requests_oauthlib.InvalidTokenError as invalidTokenError:
            self.log.info('Token is invalid.')
            self.log.info(invalidTokenError.message)

            raise AccessTokenError

        return json.loads(resp.text)

def main():
    '''
    main entrypoint
    '''

    #creds = YahooCredentials()
    #creds.consumer_key = 'dj0yJmk9a280UVNMaE5GNHFMJmQ9WVdrOWJHbERUV0ZuTnpRbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1hNQ--'
    #creds.consumer_secret = '07c58d8bb595ae61bd63c1bdf2e0259e0d018928'
    #creds.write('creds.json')

    logging.basicConfig(level=logging.INFO, format="[%(asctime)s : %(name)s : %(levelname)s] %(message)s")
    yahoo = YahooOAuth2('C:/Users/gualandrid/Documents/_LocalOnly/creds.json')
    #yahoo.authorize_access()
    #yahoo.refresh_access_token()
    #data = yahoo.get_data('https://fantasysports.yahooapis.com/fantasy/v2/league/380.l.115846/standings')
    data = yahoo.get_data('https://fantasysports.yahooapis.com/fantasy/v2/league/380.l.115846/players;status=A;start=50')
    #print(data)
    print(json.dumps(data['fantasy_content']['league'][1]['players'],indent=2))

if __name__ == "__main__":
    main()
