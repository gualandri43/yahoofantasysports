'''
enumerations for the yahoo scraper
'''

# pylint: disable= C0103        #disable invalid name check

import enum

class YAHOO_API_SPORTS(enum.Enum):
    '''
    yahoo sport type aliases
    '''

    NFL = 'nfl'
    MLB = 'mlb'
    NBA = 'nba'
    NHL = 'nhl'

class YAHOO_API_GAME_TYPES(enum.Enum):
    '''
    yahoo game type aliases
    '''

    FULL = 'full'
    PICKEM_TEAM = 'pickem-team'
    PICKEM_GROUP = 'pickem-group'
    PICKEM_TEAM_LIST = 'pickem-team-list'

class YAHOO_API_ENDPOINTS(enum.Enum):
    '''
    yahoo API endpoints
    '''

    AUTHORIZE_TOKEN_URL = "https://api.login.yahoo.com/oauth2/request_auth"
    ACCESS_TOKEN_URL = "https://api.login.yahoo.com/oauth2/get_token"
    CALLBACK_URI = 'oob'
