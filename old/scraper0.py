import argparse
import json
import pandas as pd

import sports_modules.sports_scraper.yahoo.oauth as oauth
import sports_modules.sports_scraper.yahoo.yahoo_fantasy_api as yahoo_fantasy_api
import sports_modules.sports_scraper.yahoo.enums as yahoo_enums
from sports_modules.settings import DatabaseDefinition

def player_scrape():
    yahoo = oauth.YahooOAuth2('C:/Users/gualandrid/Documents/_LocalOnly/creds.json')
    yPlayer = yahoo_fantasy_api.YahooPlayer(yahoo)
    yPlayer._add_game_key(2018, 'nfl', 380)
    players = yPlayer.get_all_players(sport='nfl', season=2018, leagueID=115846)
    #print(json.dumps(players,indent=2))
    #print(players.keys())


def all_player_scrape():

    dbDef = DatabaseDefinition('192.168.1.101', 27017)
    db = dbDef.get_mongodb_database('yahoo', username='python_rw', password='')

    yahoo = oauth.YahooOAuth2('C:/Users/guala/Desktop/creds.json')
    #yahoo.authorize_access()
    yahoo.refresh_access_token()
    yPlayer = yahoo_fantasy_api.YahooPlayer(yahoo)
    gameKey = yahoo_fantasy_api.get_game_key(yahoo, 2019, yahoo_enums.YAHOO_API_SPORTS.MLB)
    yPlayer._add_game_key(2019, 'mlb', gameKey)
    players = yPlayer.get_all_players(sport='mlb', season=2019, leagueID=107752)
    #print(json.dumps(players,indent=2))
    #print(players.keys())

    playersToInsert = []
    for key, player in players.items():
        playersToInsert.append(player)

    db['players'].insert_many(playersToInsert)


    #with open('C:/Users/guala/Desktop/yahoo_players_2019.json', 'w') as f:
    #    json.dump(players, f)

def draft_scrape():
    #2018 mlb league ID: 74143
    #2018 mlb game key: 378
    #yahoo = oauth.YahooOAuth2('C:/Users/gualandrid/Documents/_LocalOnly/creds.json')
    #yahoo.refresh_access_token()
    #yLeague = yahoo_fantasy_api.YahooLeague(yahoo)
    #gameKey = yahoo_fantasy_api.get_game_key(yahoo, 2018, yahoo_enums.YAHOO_API_SPORTS.MLB)
    #yLeague._add_game_key(2018, yahoo_enums.YAHOO_API_SPORTS.MLB, 378)
    #picks = yLeague.get_draft_results(sport=yahoo_enums.YAHOO_API_SPORTS.MLB,
    #                                  season=2018,
    #                                  leagueID=74143)


    yahoo = oauth.YahooOAuth2('C:/Users/guala/Desktop/creds.json')
    #yahoo.authorize_access()
    yahoo.refresh_access_token()
    yLeague = yahoo_fantasy_api.YahooLeague(yahoo)
    gameKey = yahoo_fantasy_api.get_game_key(yahoo, 2019, yahoo_enums.YAHOO_API_SPORTS.MLB)
    yLeague._add_game_key(2019, 'mlb', gameKey)
    picks = yLeague.get_draft_results(sport='mlb', season=2019, leagueID=107752)

    #print(picks.keys())
    #print(picks['meta'])
    #print(picks['league'])
    #print(picks['picks'])

    pd.DataFrame(picks['picks']).to_csv('C:/Users/guala/Desktop/2019draft_mlb.csv')
    #with open('C:/Users/guala/Desktop/2018draft_mlb.json', 'w') as f:
    #    json.dump(picks, f)

def authorize_new_access(consumerKey, consumerSecret, outFile):
    '''
    create new credentials file and authorize initial access. tokens are
    persisted at the given outFile path.
    '''

    creds = oauth.YahooCredentials()
    creds.create_new_creds_file(consumerKey, consumerSecret, outFile)

    yahoo = oauth.YahooOAuth2(outFile)
    yahoo.authorize_access()

def read_args():
    '''
    parse command line arguments
    '''

    parser = argparse.ArgumentParser(description='yahoo API data scraper.')

    #config
    parser.add_argument('--credentialsFile', dest='credsFile', action='store',
                        help='The path to the credentials file.')
    parser.add_argument('--consumer-key', dest='consumerKey', action='store',
                        help='the Yahoo API consumer key.')
    parser.add_argument('--consumer-key-file', dest='consumerKeyFile', action='store',
                        help='the path to a text file holding a Yahoo API consumer key.')
    parser.add_argument('--consumer-secret', dest='consumerSecret', action='store',
                        help='the Yahoo API consumer secret.')
    parser.add_argument('--consumer-secret-file', dest='consumerSecretFile', action='store',
                        help='the path to a text file holding a  Yahoo API consumer secret.')

    #options
    parser.add_argument('--sport', dest='sport', action='store', type=int,
                        help='the sport to download from.')
    parser.add_argument('--season', dest='season', action='store', type=int,
                        help='the season to download from.')
    parser.add_argument('--league-id', dest='leagueID', action='store', type=int,
                        help='the league ID to download from.')

    # actions
    parser.add_argument('--authorize-new-access', dest='authNewAccess', action='store_true',
                        help='setup initial access authorization for a key and secret. '
                             'A consumer key and secret must also be given.')
    parser.add_argument('--get-all-players', dest='getAllPlayers', action='store_true',
                        help='iteratively retrieve data on all players for a season.')

    args = parser.parse_args()
    return args


def main():
    '''
    main entrypoint
    '''
    draft_scrape()
    return
    args = read_args()

    if args.authNewAccess:
        consumerKey = args.consumerKey
        if consumerKey is None:
            print('fix')

    if args.getAllPlayers:
        all_player_scrape()


if __name__ == "__main__":
    main()
