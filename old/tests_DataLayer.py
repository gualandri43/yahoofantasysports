import unittest
import datetime as dt
import DataLayer as dl

def setUpDatabase():
    '''Set up the testing database.'''

    testdb_path = ':memory:'
    db = dl.DatabaseConnection('sqlite:///{0}'.format(testdb_path))

    season1 = dl.SportSeason(season_id=0,
                             year=2017,
                             sport='nfl',
                             reg_start_date=dt.datetime.strptime('08/01/2017','%m/%d/%Y'),
                             reg_end_date=dt.datetime.strptime('01/01/2018','%m/%d/%Y'),
                             playoff_start_date=dt.datetime.strptime('01/01/2018','%m/%d/%Y'),
                             playoff_end_date=dt.datetime.strptime('02/10/2018','%m/%d/%Y'))
    
    db.session.add(season1)
    db.session.flush()

    game_code1 = dl.YahooGameCodes(game_id=0,
                                   game_key=101,
                                   season_id=season1.season_id,
                                   name='nfl 2017',
                                   code='1010a',
                                   type='game type',
                                   url='gameurl',
                                   is_registration_over=True,
                                   is_game_over=False,
                                   is_offseason=False)

    db.session.add(game_code1)
    db.session.flush()

    league1 = dl.FantasyLeague(league_id=0,
                               season_id=season1.season_id,
                               site_id=1234,
                               yahoo_game_id=game_code1.game_id,
                               league_name='test league',
                               reg_start_date=dt.datetime.strptime('08/01/2017','%m/%d/%Y'),
                               reg_end_date=dt.datetime.strptime('12/01/2017','%m/%d/%Y'),
                               playoff_start_date=dt.datetime.strptime('12/08/2017','%m/%d/%Y'),
                               playoff_end_date=dt.datetime.strptime('12/31/2017','%m/%d/%Y'))
    
    db.session.add(league1)
    db.session.flush()

    team1 = dl.Team(team_id=0,
                    city='Groton',
                    name='teamname',
                    conference='east',
                    division='northeast',
                    sport='nfl',
                    site_id=1)
    
    db.session.add(team1)
    db.session.flush()

    conv_team1 = dl.TeamConversion(team_id=team1.team_id,
                                    extern_schema='nflschema',
                                    extern_id=123)
    
    db.session.add(conv_team1)
    db.session.flush()

    plyr1 = dl.Player(player_id=0,
                        first_name = 'dude',
                        middle_name = 'wheres',
                        last_name = 'mycar',
                        team_id = team1.team_id,
                        sport = 'nfl',
                        site_id = 234)
    
    plyr2 = dl.Player(player_id=1,
                        first_name = 'hobo',
                        middle_name = 't',
                        last_name = 'bum',
                        team_id = team1.team_id,
                        sport = 'nfl',
                        site_id = 531)
    
    db.session.add(plyr1)
    db.session.add(plyr2)
    db.session.flush()

    conv_plyr1 = dl.PlayerConversion(player_id=plyr1.player_id,
                                        extern_schema='nflschema',
                                        extern_id=223)

    conv_plyr2 = dl.PlayerConversion(player_id=plyr2.player_id,
                                        extern_schema='nflschema',
                                        extern_id=5001)

    db.session.add(conv_plyr1)
    db.session.add(conv_plyr2)
    db.session.flush()

    plyr_pos1 = dl.PlayerPosition(player_id=plyr1.player_id,
                                    pos='qb')
    
    plyr_pos2 = dl.PlayerPosition(player_id=plyr2.player_id,
                                    pos='rb')

    db.session.add(plyr_pos1)
    db.session.add(plyr_pos2)
    db.session.flush()

    manager1 = dl.Manager(manager_id=0,
                            first_name='dumb',
                            last_name='ass',
                            display_name='not so dumb',
                            site_id=5)

    manager2 = dl.Manager(manager_id=1,
                            first_name='smart',
                            last_name='ass',
                            display_name='not so smart',
                            site_id=10)
    
    db.session.add(manager1)
    db.session.add(manager2)
    db.session.flush()

    conv_manager1 = dl.ManagerConversion(manager_id=manager1.manager_id,
                                            extern_schema='nflschema',
                                            extern_id=25)

    conv_manager2 = dl.ManagerConversion(manager_id=manager2.manager_id,
                                            extern_schema='nflschema',
                                            extern_id=210)

    db.session.add(conv_manager1)
    db.session.add(conv_manager2)
    db.session.flush()

    fantasy_team1 = dl.FantasyTeam(fantasy_team_id=0,
                                    league_id=league1.league_id,
                                    site_id=2001,
                                    manager_id=manager1.manager_id,
                                    name='some witty team name')

    fantasy_team2 = dl.FantasyTeam(fantasy_team_id=1,
                                    league_id=league1.league_id,
                                    site_id=3005,
                                    manager_id=manager2.manager_id,
                                    name='some crazy team name')

    db.session.add(fantasy_team1)
    db.session.add(fantasy_team2)
    db.session.flush()

    draft1 = dl.DraftResult(pick=1,
                            manager_id=manager1.manager_id,
                            league_id=league1.league_id,
                            fantasy_team_id=fantasy_team1.fantasy_team_id,
                            player_id=plyr1.player_id)
    
    db.session.add(draft1)
    db.session.flush()

    roster1 = dl.Roster(manager_id=manager1.manager_id,
                        league_id=league1.league_id,
                        date=dt.datetime.strptime('10/10/2017','%m/%d/%Y'),
                        fantasy_team_id=fantasy_team1.fantasy_team_id,
                        player_id=plyr1.player_id,
                        roster_spot=1,
                        slot='WR1')

    db.session.add(roster1)
    db.session.flush()

    trade1 = dl.Trade(trade_id=0,
                      manager1_id=manager1.manager_id,
                      manager2_id=manager2.manager_id,
                      fantasy_team1_id=0,
                      fantasy_team2_id=1,
                      league_id=league1.league_id,
                      date=dt.datetime.strptime('10/15/2017','%m/%d/%Y'))

    db.session.add(trade1)
    db.session.flush()

    trans1 = dl.Transaction(trans_id=0,
                            manager_id=manager1.manager_id,
                            league_id=league1.league_id,
                            fantasy_team_id=fantasy_team1.fantasy_team_id,
                            player_id=plyr1.player_id,
                            draft_round=5,
                            transaction_type=2,
                            date=dt.datetime.strptime('10/20/2017','%m/%d/%Y'),
                            trade_id=trade1.trade_id,
                            site_id=8979)

    db.session.add(trans1)
    db.session.flush()

    stand1 = dl.LeagueStandings(league_id=league1.league_id,
                                manager_id=manager1.manager_id,
                                date=dt.datetime.strptime('10/01/2017','%m/%d/%Y'),
                                fantasy_team_id=fantasy_team1.fantasy_team_id,
                                rank=8,
                                points_for=156,
                                points_against=243,
                                faab=34,
                                win=3,
                                loss=5,
                                tie=1)

    db.session.add(stand1)
    db.session.flush()

    db.session.commit()

    return db

class TestTables(unittest.TestCase):
    '''Test various functionality of the table objects.'''

    def setUp(self):
        '''Test set up.'''

        self.db = setUpDatabase()
    
    def tearDown(self):
        '''Test tear down.'''
        
        self.db.meta.drop_all(self.db._engine)
        self.db.close()

    def test_Player_properties(self):
        '''Tests that the Player hybrid properties works correctly.'''

        plyr = self.db.session.query(dl.Player).get(0)
        self.assertEqual(plyr.fullname,'dude mycar')
        self.assertEqual(plyr.positions,['qb'])

    def test_Player_relationships(self):
        '''Tests that the Player relationships work correctly.'''

        plyr = self.db.session.query(dl.Player).get(0)
        
        #relationship with Team
        self.assertEqual(plyr.team.city,'Groton')

        #relationship with PlayerConversion
        self.assertEqual(plyr.id_conversions[0].extern_id,223)

        #relationship with PlayerPosition
        self.assertEqual(plyr.pos[0].pos,'qb')

    def test_Manager_properties(self):
        '''Tests that the Manager hybrid properties works correctly.'''

        man = self.db.session.query(dl.Manager).get(0)
        self.assertEqual(man.name,'not so dumb')

        man.display_name=None
        self.assertEqual(man.name,'dumb')

    def test_Manager_relationships(self):
        '''Tests that the Manager relationships work correctly.'''

        manager = self.db.session.query(dl.Manager).get(0)
        
        #relationship with ManagerConversion
        self.assertEqual(manager.id_conversions[0].extern_id,25)

        #relationship with DraftResult
        self.assertEqual(manager.draft_results[0].pick,1)

        #relationship with Roster
        self.assertEqual(manager.rosters[0].slot,'WR1')

        #relationship with Transaction
        self.assertEqual(manager.transactions[0].draft_round, 5)

        #relationship with LeagueStandings
        self.assertEqual(manager.standings[0].date, dt.datetime.strptime('10/01/2017','%m/%d/%Y'))

    def test_LeagueStandings_properties(self):
        '''Tests that the LeagueStandings hybrid properties works correctly.'''

        stand = self.db.session.query(dl.LeagueStandings).get((0,
                                                              0,
                                                              dt.datetime.strptime('10/01/2017','%m/%d/%Y')))
        self.assertEqual(stand.record,'3-5-1')
        self.assertAlmostEqual(stand.win_pct,float(1.0/3.0))

    def test_LeagueStandings_relationships(self):
        '''Tests that the LeagueStandings relationships work correctly.'''

        stand = self.db.session.query(dl.LeagueStandings).get((0,0,dt.datetime.strptime('10/01/2017','%m/%d/%Y')))
        
        #relationship with FantasyLeague
        self.assertEqual(stand.league.site_id, 1234)

        #relationship with Manager
        self.assertEqual(stand.manager.first_name, 'dumb')

        #relationship with FantasyTeam
        self.assertEqual(stand.fantasy_team.site_id, 2001)

    def test_SportSeason_properties(self):
        '''Tests that the SportSeason hybrid or overloaded properties works correctly.'''

        season = self.db.session.query(dl.SportSeason).get(0)
        self.assertEqual(season.__repr__(),'2017 nfl season')

    def test_SportSeason_relationships(self):
        '''Tests that the SportSeason relationships work correctly.'''

        season = self.db.session.query(dl.SportSeason).get(0)
        
        #relationship with FantasyLeague
        self.assertEqual(season.fantasy_leagues[0].league_name,'test league')

    def test_FantasyLeague_relationships(self):
        '''Tests that the FantasyLeague relationships work correctly.'''

        league = self.db.session.query(dl.FantasyLeague).get(0)
        
        #relationship with season
        self.assertEqual(league.season.year,2017)
        self.assertEqual(league.season.sport,'nfl')

        #relationship with yahoo_game_codes
        self.assertEqual(league.yahoo_game.code,'1010a')

        #relationship with FantasyTeam
        self.assertEqual(league.teams[0].site_id,2001)

        #relationship with DraftResult
        self.assertEqual(league.draft_results[0].pick,1)

        #relationship with Roster
        self.assertEqual(league.rosters[0].slot,'WR1')

        #relationship with Transaction
        self.assertEqual(league.transactions[0].draft_round, 5)

        #relationship with Trade
        self.assertEqual(league.trades[0].date, dt.datetime.strptime('10/15/2017','%m/%d/%Y'))

        #relationship with LeagueStandings
        self.assertEqual(league.standings[0].date, dt.datetime.strptime('10/01/2017','%m/%d/%Y'))

    def test_YahooGameCodes_relationships(self):
        '''Tests that the YahooGameCodes relationships work correctly.'''

        game_code = self.db.session.query(dl.YahooGameCodes).get(0)
        
        #relationship with FantasyLeague
        self.assertEqual(game_code.fantasy_leagues[0].site_id,1234)

        #relationship with SportSeason
        self.assertEqual(game_code.season.year,2017)

    def test_Team_relationships(self):
        '''Tests that the Team relationships work correctly.'''

        team = self.db.session.query(dl.Team).get(0)
        
        #relationship with FantasyLeague
        self.assertEqual(team.id_conversions[0].extern_id,123)

        #relationship with Player
        self.assertEqual(team.players[0].last_name,'mycar')

    def test_TeamConversion_relationships(self):
        '''Tests that the TeamConversion relationships work correctly.'''

        conv = self.db.session.query(dl.TeamConversion).get((0,'nflschema'))
        
        #relationship with Team
        self.assertEqual(conv.team.city,'Groton')

    def test_PlayerConversion_relationships(self):
        '''Tests that the PlayerConversion relationships work correctly.'''

        conv = self.db.session.query(dl.PlayerConversion).get((0,'nflschema'))
        
        #relationship with Player
        self.assertEqual(conv.player.first_name,'dude')

    def test_PlayerPosition_relationships(self):
        '''Tests that the PlayerPosision relationships work correctly.'''

        pos = self.db.session.query(dl.PlayerPosition).get((0,'qb'))
        
        #relationship with Player
        self.assertEqual(pos.player.first_name,'dude')

    def test_ManagerConversion_relationships(self):
        '''Tests that the ManagerConversion relationships work correctly.'''

        conv = self.db.session.query(dl.ManagerConversion).get((0,'nflschema'))
        
        #relationship with Manager
        self.assertEqual(conv.manager.first_name,'dumb')

    def test_FantasyTeam_relationships(self):
        '''Tests that the FantasyTeam relationships work correctly.'''

        team = self.db.session.query(dl.FantasyTeam).get(0)
        
        #relationship with FantasyLeague
        self.assertEqual(team.league.site_id, 1234)

        #relationship with Manager
        self.assertEqual(team.manager.first_name, 'dumb')

    def test_DraftResult_relationships(self):
        '''Tests that the DraftResult relationships work correctly.'''

        result = self.db.session.query(dl.DraftResult).get((1,0,0))
        
        #relationship with FantasyLeague
        self.assertEqual(result.league.site_id, 1234)

        #relationship with Manager
        self.assertEqual(result.manager.first_name, 'dumb')

        #relationship with Player
        self.assertEqual(result.player.last_name, 'mycar')

        #relationship with FantasyTeam
        self.assertEqual(result.fantasy_team.site_id, 2001)

    def test_Roster_relationships(self):
        '''Tests that the Roster relationships work correctly.'''

        roster = self.db.session.query(dl.Roster).get((0,0,dt.datetime.strptime('10/10/2017','%m/%d/%Y'),1))
        
        #relationship with FantasyLeague
        self.assertEqual(roster.league.site_id, 1234)

        #relationship with Manager
        self.assertEqual(roster.manager.first_name, 'dumb')

        #relationship with Player
        self.assertEqual(roster.player.last_name, 'mycar')

        #relationship with FantasyTeam
        self.assertEqual(roster.fantasy_team.site_id, 2001)

    def test_Transaction_relationships(self):
        '''Tests that the Transaction relationships work correctly.'''

        trans = self.db.session.query(dl.Transaction).get(0)
        
        #relationship with FantasyLeague
        self.assertEqual(trans.league.site_id, 1234)

        #relationship with Manager
        self.assertEqual(trans.manager.first_name, 'dumb')

        #relationship with Player
        self.assertEqual(trans.player.last_name, 'mycar')

        #relationship with Trade
        self.assertEqual(trans.trade.date, dt.datetime.strptime('10/15/2017','%m/%d/%Y'))

        #relationship with FantasyTeam
        self.assertEqual(trans.fantasy_team.site_id, 2001)

    def test_Trade_relationships(self):
        '''Tests that the Trade relationships work correctly.'''

        trade = self.db.session.query(dl.Trade).get(0)
        
        #relationship with FantasyLeague
        self.assertEqual(trade.league.site_id, 1234)

        #relationship with Manager
        self.assertEqual(trade.manager1.first_name, 'dumb')
        self.assertEqual(trade.manager2.first_name, 'smart')

        #relationship with FantasyTeam
        self.assertEqual(trade.fantasy_team1.site_id, 2001)
        self.assertEqual(trade.fantasy_team2.site_id, 3005)

class TestDatabaseConnection(unittest.TestCase):

    def setUp(self):
        '''Test set up.'''

        self.db = setUpDatabase()

    def tearDown(self):
        '''Test tear down.'''
        
        self.db.meta.drop_all(self.db._engine)
        self.db.close()

    def test_constructor(self):
        '''Tests that the constructor creates a session and engine.'''

        self.assertIsNotNone(self.db._engine)
        self.assertIsNotNone(self.db.session)

    def test_get_player_by_site_id(self):
        '''Tests the get_player_by_site_id method.'''

        conv = self.db.get_player_by_site_id(234)
        
        self.assertEqual(conv.player_id,0)
        self.assertEqual(conv.site_id,234)

    def test_get_manager_by_site_id(self):
        '''Tests the get_manager_by_site_id method.'''

        conv = self.db.get_manager_by_site_id(10)

        self.assertEqual(conv.manager_id,1)
        self.assertEqual(conv.site_id,10)
        self.assertEqual(conv.first_name,'smart')

    def test_get_team_by_site_id(self):
        '''Tests the get_team_by_site_id method.'''

        conv = self.db.get_team_by_site_id(1)
        
        self.assertEqual(conv.team_id,0)
        self.assertEqual(conv.site_id,1)

    def test_get_fantasy_team_by_site_id(self):
        '''Tests the get_fantasy_team_by_site_id method.'''

        conv = self.db.get_fantasy_team_by_site_id(2001)

        self.assertEqual(conv.fantasy_team_id,0)
        self.assertEqual(conv.site_id,2001)

    def test_get_fantasy_league_by_site_id(self):
        '''Tests the get_fantasy_league_by_site_id method.'''

        conv = self.db.get_fantasy_league_by_site_id(1234)

        self.assertEqual(conv.league_id,0)
        self.assertEqual(conv.site_id,1234)

    def test_get_yahoo_id_conversion_dict_player(self):
        '''Tests the get_yahoo_id_conversion_dict_player method.'''

        testDict = self.db.get_yahoo_id_conversion_dict_player()

        self.assertEqual(testDict[234],0)
        self.assertEqual(testDict[531],1)

    def test_get_yahoo_id_conversion_dict_league(self):
        '''Tests the get_yahoo_id_conversion_dict_league method.'''

        testDict = self.db.get_yahoo_id_conversion_dict_league()

        self.assertEqual(testDict[1234],0)

    def test_get_yahoo_id_conversion_dict_team(self):
        '''Tests the get_yahoo_id_conversion_dict_team method.'''

        testDict = self.db.get_yahoo_id_conversion_dict_team()

        self.assertEqual(testDict[1],0)

    def test_get_yahoo_id_conversion_dict_manager(self):
        '''Tests the get_yahoo_id_conversion_dict_manager method.'''

        testDict = self.db.get_yahoo_id_conversion_dict_manager()

        self.assertEqual(testDict[5],0)
        self.assertEqual(testDict[10],1)

    def test_get_yahoo_id_conversion_dict_fantasy_team(self):
        '''Tests the test_get_yahoo_id_conversion_dict_fantasy_team method.'''

        testDict = self.db.get_yahoo_id_conversion_dict_fantasy_team()

        self.assertEqual(testDict[2001],0)
        self.assertEqual(testDict[3005],1)

    def test_get_get_player_conversion(self):
        '''Tests the get_player_conversion method.'''

        conv = self.db.get_player_conversion(0,'nflschema')
        
        self.assertEqual(conv.extern_id,223)

    def test_add_or_update_player_conversion(self):
        '''Tests the add_or_update_player_conversion method.'''

        #conversion exists
        self.db.add_or_update_player_conversion(0,'nflschema',355)
        conv1 = self.db.session.query(dl.PlayerConversion).get((0,'nflschema'))
        self.assertEqual(conv1.extern_id,355)

        #conversion doesn't exist
        self.db.add_or_update_player_conversion(10,'new schema',755)
        conv2 = self.db.session.query(dl.PlayerConversion).get((10,'new schema'))
        self.assertEqual(conv2.extern_id,755)
        self.assertEqual(conv2.player_id,10)
        self.assertEqual(conv2.extern_schema,'new schema')

    def test_add_player(self):
        '''Tests the add_player method.'''

        #added player doesn't exist
        plyr1 = self.db.add_player(107, first_name='new', last_name='player', team_id=0, sport='nfl')
        self.assertEqual(plyr1.site_id, 107)
        self.assertEqual(plyr1.first_name, 'new')
        self.assertEqual(plyr1.last_name, 'player')
        self.assertEqual(plyr1.team.team_id, 0)
        self.assertEqual(plyr1.sport, 'nfl')

        #added player does exist, don't update
        plyr2 = self.db.add_player(234, update_if_exists=False, first_name='updated',
                                        last_name='guy', team_id=0, sport='meh')
        self.assertEqual(plyr2.site_id, 234)
        self.assertEqual(plyr2.first_name, 'dude')
        self.assertEqual(plyr2.last_name, 'mycar')
        self.assertEqual(plyr2.team.team_id, 0)
        self.assertEqual(plyr2.sport, 'nfl')

        #added player does exist, do update
        plyr3 = self.db.add_player(234, update_if_exists=True, first_name='updated',
                                        last_name='guy', team_id=0, sport='meh')
        self.assertEqual(plyr3.site_id, 234)
        self.assertEqual(plyr3.first_name, 'updated')
        self.assertEqual(plyr3.last_name, 'guy')
        self.assertEqual(plyr3.team.team_id, 0)
        self.assertEqual(plyr3.sport, 'meh')

    def test_add_manager(self):
        '''Tests the add_manager method.'''

        #added manager doesn't exist
        man1 = self.db.add_manager(201, first_name='D', last_name='G',display_name='commish')
        self.assertEqual(man1.site_id, 201)
        self.assertEqual(man1.first_name, 'D')
        self.assertEqual(man1.last_name, 'G')
        self.assertEqual(man1.display_name, 'commish')

        #added manager exists, don't update
        man2 = self.db.add_manager(5, update_if_exists=False, first_name='D', last_name='G',
                                        display_name='commish')
        self.assertEqual(man2.site_id, 5)
        self.assertEqual(man2.first_name, 'dumb')
        self.assertEqual(man2.last_name, 'ass')
        self.assertEqual(man2.display_name, 'not so dumb')

        #added manager exists, update
        man3 = self.db.add_manager(5, first_name='updated', last_name='manager', display_name='disp')
        self.assertEqual(man3.site_id, 5)
        self.assertEqual(man3.first_name, 'updated')
        self.assertEqual(man3.last_name, 'manager')
        self.assertEqual(man3.display_name, 'disp')

    def test_add_team(self):
        '''Tests the add_team method.'''

        #added team doesn't exist
        team1 = self.db.add_team(111, city='new city', name='new name', conference='new conf',
                                      division='new div', sport='new sport')
        self.assertEqual(team1.site_id, 111)
        self.assertEqual(team1.city, 'new city')
        self.assertEqual(team1.name, 'new name')
        self.assertEqual(team1.conference, 'new conf')
        self.assertEqual(team1.division, 'new div')
        self.assertEqual(team1.sport, 'new sport')

        #added team exists, don't update
        team2 = self.db.add_team(1, update_if_exists=False, city='Groton', name='teamname', 
                                    conference='east', division='northeast', sport='nfl')
        self.assertEqual(team2.site_id, 1)
        self.assertEqual(team2.city, 'Groton')
        self.assertEqual(team2.name, 'teamname')
        self.assertEqual(team2.conference, 'east')
        self.assertEqual(team2.division, 'northeast')
        self.assertEqual(team2.sport, 'nfl')

        #added team exists, update
        team3 = self.db.add_team(1, update_if_exists=True, city='update city', name='update name', 
                                    conference='update conf', division='update div', sport='update sport')
        self.assertEqual(team3.site_id, 1)
        self.assertEqual(team3.city, 'update city')
        self.assertEqual(team3.name, 'update name')
        self.assertEqual(team3.conference, 'update conf')
        self.assertEqual(team3.division, 'update div')
        self.assertEqual(team3.sport, 'update sport')

    def test_add_fantasy_team(self):
        '''Tests the add_fantasy_team method.'''

        #added team doesn't exist
        team1 = self.db.add_fantasy_team(5004, league_id=0, manager_id=0, name='new team')
        self.assertEqual(team1.site_id, 5004)
        self.assertEqual(team1.league_id, 0)
        self.assertEqual(team1.manager_id, 0)
        self.assertEqual(team1.name, 'new team')

        #added team exists, don't update
        team2 = self.db.add_fantasy_team(2001, update_if_exists=False, league_id=0,
                                               manager_id=0, name='updated team')
        self.assertEqual(team2.site_id, 2001)
        self.assertEqual(team2.league_id, 0)
        self.assertEqual(team2.manager_id, 0)
        self.assertEqual(team2.name, 'some witty team name')

        #added team exists, update
        team3 = self.db.add_fantasy_team(2001, update_if_exists=True, league_id=0,
                                               manager_id=0, name='updated team')
        self.assertEqual(team3.site_id, 2001)
        self.assertEqual(team3.league_id, 0)
        self.assertEqual(team3.manager_id, 0)
        self.assertEqual(team3.name, 'updated team')

    def test_add_fantasy_league(self):
        '''Tests the add_fantasy_league method.'''    

        #added league doesn't exist
        team1 = self.db.add_fantasy_league(53, season_id=0, yahoo_game_id=0,
                                               league_name='new league',
                                               reg_start_date=dt.datetime.strptime('10/01/2017','%m/%d/%Y'),
                                               reg_end_date=dt.datetime.strptime('10/02/2017','%m/%d/%Y'),
                                               playoff_start_date=dt.datetime.strptime('10/03/2017','%m/%d/%Y'),
                                               playoff_end_date=dt.datetime.strptime('10/04/2017','%m/%d/%Y'))
        self.assertEqual(team1.site_id, 53)
        self.assertEqual(team1.season_id, 0)
        self.assertEqual(team1.yahoo_game_id, 0)
        self.assertEqual(team1.league_name, 'new league')
        self.assertEqual(team1.reg_start_date, dt.datetime.strptime('10/01/2017','%m/%d/%Y'))
        self.assertEqual(team1.reg_end_date, dt.datetime.strptime('10/02/2017','%m/%d/%Y'))
        self.assertEqual(team1.playoff_start_date, dt.datetime.strptime('10/03/2017','%m/%d/%Y'))
        self.assertEqual(team1.playoff_end_date, dt.datetime.strptime('10/04/2017','%m/%d/%Y'))

        #added league exists, don't update
        team2 = self.db.add_fantasy_league(1234, season_id=0, yahoo_game_id=0,
                                                 update_if_exists=False, league_name='updated league',
                                                 reg_start_date=dt.datetime.strptime('12/01/2017','%m/%d/%Y'),
                                                 reg_end_date=dt.datetime.strptime('12/02/2017','%m/%d/%Y'),
                                                 playoff_start_date=dt.datetime.strptime('12/03/2017','%m/%d/%Y'),
                                                 playoff_end_date=dt.datetime.strptime('12/04/2017','%m/%d/%Y'))
        self.assertEqual(team2.site_id, 1234)
        self.assertEqual(team2.season_id, 0)
        self.assertEqual(team2.yahoo_game_id, 0)
        self.assertEqual(team2.league_name, 'test league')
        self.assertEqual(team2.reg_start_date, dt.datetime.strptime('08/01/2017','%m/%d/%Y'))
        self.assertEqual(team2.reg_end_date, dt.datetime.strptime('12/01/2017','%m/%d/%Y'))
        self.assertEqual(team2.playoff_start_date, dt.datetime.strptime('12/08/2017','%m/%d/%Y'))
        self.assertEqual(team2.playoff_end_date, dt.datetime.strptime('12/31/2017','%m/%d/%Y'))

        #added league exists, update
        team3 = self.db.add_fantasy_league(1234, season_id=0, yahoo_game_id=0,
                                                 update_if_exists=True, league_name='updated league',
                                                 reg_start_date=dt.datetime.strptime('12/01/2017','%m/%d/%Y'),
                                                 reg_end_date=dt.datetime.strptime('12/02/2017','%m/%d/%Y'),
                                                 playoff_start_date=dt.datetime.strptime('12/03/2017','%m/%d/%Y'),
                                                 playoff_end_date=dt.datetime.strptime('12/04/2017','%m/%d/%Y'))
        self.assertEqual(team3.site_id, 1234)
        self.assertEqual(team3.season_id, 0)
        self.assertEqual(team3.yahoo_game_id, 0)
        self.assertEqual(team3.league_name, 'updated league')
        self.assertEqual(team3.reg_start_date, dt.datetime.strptime('12/01/2017','%m/%d/%Y'))
        self.assertEqual(team3.reg_end_date, dt.datetime.strptime('12/02/2017','%m/%d/%Y'))
        self.assertEqual(team3.playoff_start_date, dt.datetime.strptime('12/03/2017','%m/%d/%Y'))
        self.assertEqual(team3.playoff_end_date, dt.datetime.strptime('12/04/2017','%m/%d/%Y'))


if __name__ == '__main__':
    unittest.main()