'''Data and storage layer for scraped yahoo data'''

#pylint: disable=R0903
#   not enough public methods (sqlalchemy classes don't need these)
#pylint: disable=R0913
#   too many arguments (there are a ton of optional parameters, not worried about too many)

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.orm import sessionmaker, Session, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import create_engine

BASE = declarative_base()

#### Data Model Definition ####
class SportSeason(BASE):
    '''Class to hold meta information about a fantasy/sport season.'''

    __tablename__ = "sport_season"
    season_id = Column(Integer, primary_key=True)
    year = Column(Integer)
    sport = Column(String(255))
    reg_start_date = Column(DateTime)
    reg_end_date = Column(DateTime)
    playoff_start_date = Column(DateTime)
    playoff_end_date = Column(DateTime)
    fantasy_leagues = relationship('FantasyLeague', back_populates='season')

    def __repr__(self):
        return '{0} {1} season'.format(self.year,self.sport)

class FantasyLeague(BASE):
    '''Class to hold fantasy league information. Entry for each year of the league'''

    __tablename__ = 'fantasy_league'
    league_id = Column(Integer, primary_key=True)
    season_id = Column(Integer, ForeignKey('sport_season.season_id'))
    season = relationship('SportSeason', back_populates='fantasy_leagues')
    site_id = Column(Integer)
    yahoo_game_id = Column(Integer, ForeignKey('yahoo_game_codes.game_id'))
    yahoo_game = relationship('YahooGameCodes',back_populates='fantasy_leagues')
    league_name = Column(String(255))
    reg_start_date = Column(DateTime)
    reg_end_date = Column(DateTime)
    playoff_start_date = Column(DateTime)
    playoff_end_date = Column(DateTime)
    teams = relationship('FantasyTeam', back_populates='league')
    events = relationship('LeagueEvent', back_populates='league')
    #draft_results = relationship('DraftResult', back_populates='league')
    #rosters = relationship('Roster', back_populates='league')
    #transactions = relationship('Transaction', back_populates='league')
    #trades = relationship('Trade', back_populates='league')
    #standings = relationship('LeagueStandings', back_populates='league')

class YahooGameCodes(BASE):
    '''Class to hold information about the Yahoo API game codes'''

    __tablename__ = 'yahoo_game_codes'
    game_id = Column(Integer, primary_key=True)
    fantasy_leagues = relationship('FantasyLeague', back_populates='yahoo_game')
    season_id = Column(Integer, ForeignKey('sport_season.season_id'))
    season = relationship('SportSeason')
    game_key = Column(Integer)
    name = Column(String(255))
    code = Column(String(255))
    type = Column(String(255))
    url = Column(String(255))
    is_registration_over = Column(Boolean)
    is_game_over = Column(Boolean)
    is_offseason = Column(Boolean)

class Team(BASE):
    '''Class to hold team information.'''

    __tablename__ = 'team'
    team_id = Column(Integer, primary_key=True)
    city = Column(String(255))
    name = Column(String(255))
    conference = Column(String(255))
    division = Column(String(255))
    sport = Column(String(255))
    site_id = Column(Integer)
    id_conversions = relationship('TeamConversion', back_populates='team')
    players = relationship('Player', back_populates='team')

class TeamConversion(BASE):
    '''Class to hold team ID conversion information for conversion to external schema tables.'''

    __tablename__ = 'team_conversion'
    team_id = Column(Integer, ForeignKey('team.team_id'), primary_key=True)
    team = relationship("Team", back_populates='id_conversions')
    extern_schema = Column(String(255), primary_key=True)
    extern_id = Column(Integer)

class Player(BASE):
    '''Class to hold player information.'''

    __tablename__ = 'player'
    player_id = Column(Integer, primary_key=True)
    first_name = Column(String(255))
    middle_name = Column(String(255))
    last_name = Column(String(255))
    team_id = Column(Integer, ForeignKey('team.team_id'))
    team = relationship('Team', back_populates='players')
    sport = Column(String(255))
    site_id = Column(Integer)
    id_conversions = relationship('PlayerConversion', back_populates='player')
    pos = relationship('PlayerPosition', back_populates='player')

    @hybrid_property
    def fullname(self):
        """Assembles the player's full name from their first and last names."""

        return '{0} {1}'.format(self.first_name, self.last_name)

    @hybrid_property
    def positions(self):
        '''Creates a list of all positions the player plays.'''

        return [str(x.pos) for x in self.pos]

class PlayerConversion(BASE):
    '''Class to hold player ID conversion information for conversion to external schema tables.'''

    __tablename__ = 'player_conversion'
    player_id = Column(Integer, ForeignKey('player.player_id'), primary_key=True)
    player = relationship('Player', back_populates='id_conversions')
    extern_schema = Column(String(255), primary_key=True)
    extern_id = Column(Integer)

class PlayerPosition(BASE):
    '''Class to hold player position information'''

    __tablename__ = 'player_position'
    player_id = Column(Integer, ForeignKey('player.player_id'), primary_key=True)
    player = relationship('Player', back_populates='pos')
    pos = Column(String(10), primary_key=True)

class Manager(BASE):
    '''Class to hold fantasy team manager information'''

    __tablename__ = 'manager'
    manager_id = Column(Integer, primary_key=True)
    first_name = Column(String(255))
    last_name = Column(String(255))
    display_name = Column(String(255))
    site_id = Column(Integer)
    id_conversions = relationship('ManagerConversion', back_populates='manager')
    teams = relationship('FantasyTeam', back_populates='manager')
    draft_results = relationship('DraftResult', back_populates='manager')
    rosters = relationship('Roster', back_populates='manager')
    transactions = relationship('Transaction', back_populates='manager')
    standings = relationship('LeagueStandings', back_populates='manager')

    @hybrid_property
    def name(self):
        """Display manager's name based on if a display name is defined or not."""

        if self.display_name is not None:
            return self.display_name
        else:
            return self.first_name

class ManagerConversion(BASE):
    '''Class to hold manager ID conversion information for conversion to external schema tables.'''

    __tablename__ = 'manager_conversion'
    manager_id = Column(Integer, ForeignKey('manager.manager_id'), primary_key=True)
    manager = relationship('Manager', back_populates='id_conversions')
    extern_schema = Column(String(255), primary_key=True)
    extern_id = Column(Integer)

class FantasyTeam(BASE):
    '''
    Class to hold fantasy team information. There will be more than one per season, since team
    names can change throughout.
    '''

    __tablename__ = 'fantasy_team'
    fantasy_team_id = Column(Integer, primary_key=True)
    league_id = Column(Integer, ForeignKey('fantasy_league.league_id'))
    league = relationship('FantasyLeague', back_populates='teams')
    site_id = Column(Integer)
    manager_id = Column(Integer, ForeignKey('manager.manager_id'))
    manager = relationship('Manager', back_populates='teams')
    name = Column(String(255))

class LeagueRecord(BASE):
    '''Base class for data recorded about the league.'''
    
    __tablename__ = 'league_record'
    league_record_id = Column(Integer, primary_key=True)
    manager_id = Column(Integer, ForeignKey('manager.manager_id'))
    manager = relationship('Manager', back_populates='draft_results')
    league_id = Column(Integer, ForeignKey('fantasy_league.league_id'))
    league = relationship('FantasyLeague', back_populates='events')
    fantasy_team_id = Column(Integer, ForeignKey('fantasy_team.fantasy_team_id'))
    fantasy_team = relationship('FantasyTeam')
    record_timestamp = Column(TIMESTAMP)
    discriminator = Column('type', String(50))
    
    __mapper_args__ = {'polymorphic_identity':'league_record',
                       'polymorphic_on':discriminator
                      }

class LeagueStandings(LeagueRecord):
    '''Class to hold league standings information'''

    __tablename__ = 'league_standings'
    league_record_id = Column(Integer, ForeignKey('league_record.league_record_id'))
    week = Column(Integer)
    day = Column(Integer)
    rank = Column(Integer)
    points_for = Column(Float)
    points_against = Column(Float)
    faab = Column(Float)
    win = Column(Integer)
    loss = Column(Integer)
    tie = Column(Integer)

    @hybrid_property
    def record(self):
        '''Format record display in win-lose-tie format.'''
        return '{0}-{1}-{2}'.format(self.win, self.loss, self.tie)

    @hybrid_property
    def win_pct(self):
        '''Calculate win percentage.'''
        return float(self.win) / (float(self.win) + float(self.loss) + float(self.tie))

    __mapper_args__ = {'polymorphic_identity':'league_standings'}

class LeaguePlayerEvent(LeagueRecord):
    '''Base class for recorded league events.'''

    __tablename__ = 'league_player_event'
    league_record_id = Column(Integer, ForeignKey('league_record.league_record_id'))
    player_id = Column(Integer, ForeignKey('player.player_id'))
    player = relationship('Player')
    event_timestamp = Column(TIMESTAMP)
    discriminator = Column('type', String(50))
    
    __mapper_args__ = {'polymorphic_identity':'league_player_event',
                       'polymorphic_on':discriminator
                      }

class DraftResult(LeaguePlayerEvent):
    '''Class to hold fantasy draft result information'''

    __tablename__ = 'draft_result'
    league_record_id = Column(Integer, ForeignKey('league_record.league_record_id'))
    pick = Column(Integer)

    __mapper_args__ = {'polymorphic_identity':'draft_result'}

class Roster(LeaguePlayerEvent):
    '''Class to hold information about players on a roster'''

    __tablename__ = 'roster'
    league_record_id = Column(Integer, ForeignKey('league_record.league_record_id'))
    week = Column(Integer)
    day = Column(Integer)

    #roster_spot is an incrementing integer, may increase up to the roster size limit.
    #no inherent order.
    roster_spot = Column(Integer)

    #slot is the position that the player is in (i.e. QB, WR, SP, 1B, Bench, etc)
    slot = Column(String(50))

    __mapper_args__ = {'polymorphic_identity':'roster'}

class Transaction(LeaguePlayerEvent):
    '''Class to hold information about roster transactions'''

    __tablename__ = 'transaction'
    league_record_id = Column(Integer, ForeignKey('league_record.league_record_id'))
    transaction_id = Column(Integer)
    
    

    discriminator = Column('type', String(50))
    
    __mapper_args__ = {'polymorphic_identity':'transaction',
                       'polymorphic_on':discriminator
                      }

class FreeAgentEvent(Transaction):
    '''container for add/drop transactions'''

class Trade(LeagueRecord):
    '''Class to hold information about trade metadata.
    Each trade record will reference the relevant league record ids that are part of the trade.
    '''

    __tablename__ = 'trade'
    league_record_id = Column(Integer, ForeignKey('league_record.league_record_id'), primary_key=True)
    trade_id =  Column(Integer, primary_key=True)
    transactions = relationship('FantasyLeague', back_populates='events')
    transaction_ids = []


class DraftPickGet(LeagueRecord):

class DraftPickGive(LeagueRecord):

class PlayerTradeGet(LeaguePlayerEvent):

class PlayerTradeGive(LeaguePlayerEvent):

class PlayerAdd(LeaguePlayerEvent):

class PlayerDrop(LeaguePlayerEvent):

### Database Interaction Classes ###
class DatabaseConnection(object):
    '''
    Class serves as the main interface to connect and interact with the storage database.
    '''

    def __init__(self, conString):
        '''Constructor.'''

        self._engine = create_engine(conString)
        self.meta = BASE.metadata

        self.meta.create_all(self._engine)
        self.session = Session(bind=self._engine)

    def close(self):
        '''Clean up database connection.'''

        self.session.close()

    def get_player_by_site_id(self, site_id):
        '''Retrieves player based on the yahoo site ID.

        Args:
            site_id (int): Yahoo ID number for the player.

        Returns:
            Player object if found, None otherwise.
        '''

        return self.session.query(Player).filter_by(site_id=site_id).first()

    def get_manager_by_site_id(self, site_id):
        '''Retrieves manager based on the yahoo site ID.

        Args:
            site_id (int): Yahoo ID number for the team.

        Returns:
            Manager object if found, None otherwise.
        '''

        return self.session.query(Manager).filter_by(site_id=site_id).first()

    def get_team_by_site_id(self, site_id):
        '''Retrieves team based on the yahoo site ID.

        Args:
            site_id (int): Yahoo ID number for the team.

        Returns:
            Team object if found, None otherwise.
        '''

        return self.session.query(Team).filter_by(site_id=site_id).first()

    def get_fantasy_team_by_site_id(self, site_id):
        '''Retrieves fantasy team based on the yahoo site ID.

        Args:
            site_id (int): Yahoo ID number for the team.

        Returns:
            Team object if found, None otherwise.
        '''

        return self.session.query(FantasyTeam).filter_by(site_id=site_id).first()

    def get_fantasy_league_by_site_id(self, site_id):
        '''Retrieves fantasy league based on the yahoo site ID.

        Args:
            site_id (int): Yahoo ID number for the league.

        Returns:
            Team object if found, None otherwise.
        '''

        return self.session.query(FantasyLeague).filter_by(site_id=site_id).first()

    def get_yahoo_id_conversion_dict_player(self):
        '''Builds a dictionary of ID mappings for players in database.
        
        Returns:
            Dictionary of key: yahoo site id, value: internal id.
        '''

        plyrs = self.session.query(Player).all()

        ids = {}
        for p in plyrs:
            ids[p.site_id] = p.player_id
        
        return ids

    def get_yahoo_id_conversion_dict_league(self):
        '''Builds a dictionary of ID mappings for fantasy leagues in database.
        
        Returns:
            Dictionary of key: yahoo site id, value: internal id.
        '''

        leagues = self.session.query(FantasyLeague).all()

        ids = {}
        for p in leagues:
            ids[p.site_id] = p.league_id
        
        return ids

    def get_yahoo_id_conversion_dict_team(self):
        '''Builds a dictionary of ID mappings for real world teams in database.
        
        Returns:
            Dictionary of key: yahoo site id, value: internal id.
        '''

        teams = self.session.query(Team).all()

        ids = {}
        for p in teams:
            ids[p.site_id] = p.team_id
        
        return ids

    def get_yahoo_id_conversion_dict_manager(self):
        '''Builds a dictionary of ID mappings for manager in database.
        
        Returns:
            Dictionary of key: yahoo site id, value: internal id.
        '''

        managers = self.session.query(Manager).all()

        ids = {}
        for p in managers:
            ids[p.site_id] = p.manager_id
        
        return ids

    def get_yahoo_id_conversion_dict_fantasy_team(self):
        '''Builds a dictionary of ID mappings for fantasy teams in database.
        
        Returns:
            Dictionary of key: yahoo site id, value: internal id.
        '''

        teams = self.session.query(FantasyTeam).all()

        ids = {}
        for p in teams:
            ids[p.site_id] = p.fantasy_team_id
        
        return ids

    def get_player_conversion(self, player_id, extern_schema):
        '''Retrieves the player ID conversion for the given internal player ID.

        Args:
            player_id (int): Internal ID number for the player.
            extern_schema (str): Schema to get conversion for.

        Returns:
            Conversion object for the external schema. None if no conversion was found.
        '''

        return self.session.query(PlayerConversion).get((player_id, extern_schema))

    def add_or_update_player_conversion(self, player_id, extern_schema, extern_id):
        '''
        Add a player conversion to an external database schema to the database. If an
        existing conversion is found, it is updated to the new value.
        '''

        conv = self.get_player_conversion(player_id, extern_schema)

        if conv is None:
            conv = PlayerConversion(player_id=player_id, extern_schema=extern_schema, extern_id=extern_id)
            self.session.add(conv)
        else:
            conv.extern_id = extern_id

        self.session.commit()

    def add_player(self, site_id, update_if_exists=True, first_name=None, middle_name=None, 
                         last_name=None, team_id=None, sport=None):
        '''
        Add a player to the database with the given attibutes. By default, if a player
        with the same site_id is found, the info is updated for that player.

        Args:
            site_id (int): yahoo id number
            update_if_exists (bool): flag to update record. Defaults to True.

        Returns:
            The created or modified player object.
        '''

        plyr = self.get_player_by_site_id(site_id)

        if plyr is None:
            plyr = Player(site_id=site_id, first_name=first_name, middle_name=middle_name,
                        last_name=last_name, team_id=team_id, sport=sport)
            self.session.add(plyr)
        else:
            if update_if_exists:
                plyr.site_id = site_id
                plyr.first_name = first_name
                plyr.middle_name = middle_name
                plyr.last_name = last_name
                plyr.team_d = team_id
                plyr.sport = sport

        self.session.commit()

        return plyr

    def add_manager(self, site_id, update_if_exists=True, first_name=None, last_name=None,
                          display_name=None):
        '''
        Add a fantasy team manager to the database with the given attibutes. By default, if a manager with
        the same site_id is found, the info is updated for that manager.

        Returns:
            The created or modified player object.
        '''

        mgr = self.get_manager_by_site_id(site_id)

        if mgr is None:
            mgr = Manager(site_id=site_id, first_name=first_name, last_name=last_name,
                          display_name=display_name)
            self.session.add(mgr)
        else:
            if update_if_exists:
                mgr.site_id = site_id
                mgr.first_name = first_name
                mgr.last_name = last_name
                mgr.display_name = display_name

        self.session.commit()

        return mgr

    def add_team(self, site_id, update_if_exists=True, city=None, name=None, conference=None,
                       division=None, sport=None):
        '''
        Add a team (real-world) to the database with the given attibutes. By default, if a team with the
        same site_id is found, the info is updated for that team.

        Returns:
            The modified or added team object.
        '''

        team = self.get_team_by_site_id(site_id)

        if team is None:
            team = Team(site_id=site_id, city=city, name=name, conference=conference,
                        division=division, sport=sport)
            self.session.add(team)
        else:
            if update_if_exists:
                team.site_id = site_id
                team.city = city
                team.name = name
                team.conference = conference
                team.division = division
                team.sport = sport

        self.session.commit()

        return team

    def add_fantasy_team(self, site_id, update_if_exists=True, manager_id=None,
                                        name=None, league_id=None):
        '''
        Add a fantasy team to the database with the given attibutes. By default, if one is found, the
        attributes are updated

        Returns:
            The added or modified team object.
        '''

        team = self.get_fantasy_team_by_site_id(site_id)

        if team is None:
            team = FantasyTeam(site_id=site_id,
                               league_id=league_id,
                               manager_id=manager_id,
                               name=name)

            self.session.add(team)
        
        else:
            if update_if_exists:
                team.site_id = site_id
                team.league_id = league_id
                team.manager_id = manager_id
                team.name = name
        
        self.session.commit()

        return team

    def add_fantasy_league(self, site_id, update_if_exists=True, season_id=None, yahoo_game_id=None,
                                          league_name=None, reg_start_date=None, reg_end_date=None,
                                          playoff_start_date=None, playoff_end_date=None):
        '''
        Add a fantasy league to the database with the given attibutes. By default, if a
        league with the same site_id is found, the info is updated for that league.

        Returns:
            The modified or added league object.
        '''

        league = self.get_fantasy_league_by_site_id(site_id)

        if league is None:
            league = FantasyLeague(site_id=site_id, season_id=season_id, yahoo_game_id=yahoo_game_id,
                                                    league_name=league_name, reg_start_date=reg_start_date,
                                                    reg_end_date=reg_end_date,playoff_start_date=playoff_start_date,
                                                    playoff_end_date=playoff_end_date)
            self.session.add(league)
        else:
            if update_if_exists:
                league.site_id = site_id
                league.season_id = season_id
                league.yahoo_game_id = yahoo_game_id
                league.league_name = league_name
                league.reg_start_date = reg_start_date
                league.reg_end_date = reg_end_date
                league.playoff_start_date = playoff_start_date
                league.playoff_end_date = playoff_end_date

        self.session.commit()

        return league

def main():
    '''Main script.'''
    print('import module to access classes for the MySportsFeeds.com data layer.')

if __name__ == "__main__":
    main()
