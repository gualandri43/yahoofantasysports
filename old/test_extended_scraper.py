import logging

import sports_modules.sports_scraper.support.scrapy as scrapy_support
import sports_modules.sports_scraper.yahoo.yahoo_scrapy.spiders.YahooDraftScraper as draft_spider

process = scrapy_support.ExtendedCrawlerProcess(settings={
                'OAUTH_CREDENTIALS_FILE': 'C:/Users/guala/Desktop/creds.json',
                'YAHOO_LEAGUE': 'mffl',
                'YAHOO_SEASON': 2018,
                'LOG_LEVEL': 'INFO',
                'POSTGRES_DB_HOST': '',
                'POSTGRES_DB_USER': '',
                'POSTGRES_DB_PORT': ,
                'POSTGRES_DB_PASSWORD': ''
                },
                additionalLoggerHandlers=[logging.FileHandler('C:/Users/guala/Desktop/testlog.log')]
            )


process.crawl(draft_spider.YahooDraftScraper)
process.start() # the script will block here until the crawling is finished
