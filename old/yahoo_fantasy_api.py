'''
Connect to and retrieve data from the Yahoo Fantasy API.
'''

import json
#import urllib

from sports_modules.sports_scraper.yahoo.enums import YAHOO_API_GAME_TYPES, YAHOO_API_SPORTS

class YahooApiUrlBuilder:

    def __init__(self, gameKey, leagueId, responseFormat='json'):
        '''

        '''

        self._baseURL = 'https://fantasysports.yahooapis.com/fantasy/v2'

        self._gameKey = gameKey
        self._leagueId = leagueId
        self._responseFormat = responseFormat

        self._opts = {}

    def _build_query_string(self, apiUrl):
        '''
        construct query string out of parameters and append to apiUrl
        '''

        if len(self._opts) > 0:
            paramString = ';'.join([key + '=' + val for (key,val) in self._opts.items()])
            return '{0};{1}?format={2}'.format(apiUrl, paramString, self._responseFormat)
        else:
            return '{0}?format={1}'.format(apiUrl, self._responseFormat)

    def build_transaction_url(self, transactionId, transType='completed'):
        '''
        retrieve data from the transaction api resource

        transType       one of ['completed','waiver','pending_trade']


        https://fantasysports.yahooapis.com/fantasy/v2/transaction/{trans_key}

        Completed transactions: .l.{league_id}.tr.{transaction_id}
        Example:pnfl.l.431.tr.26 or 223.l.431.tr.26

        Waiver claims: .l.{league_id}.w.c.{claim_id}
        Example:257.l.193.w.c.2_6390

        Pending trades: .l.{league_id}.pt.{pending_trade_id}
        Example:257.l.193.pt.1

        '''

        if transType not in ['completed','waiver','pending_trade']:
            raise ValueError('not a valid type')

        transTypeStr = ''
        if transType == 'completed':
            transTypeStr = 'tr'

        if transType == 'waiver':
            transTypeStr = 'w.c'

        if transType == 'pending_trade':
            transTypeStr = 'pt'

        transactionKey = '{0}.l.{1}.{2}.{3}'.format(
            self._gameKey,
            self._leagueId,
            transTypeStr,
            transactionId
        )

        transactionBaseUrl = '{0}/transaction/{1}'.format(self._baseURL, transactionKey)
        return self._build_query_string(transactionBaseUrl)

    def build_transactions_url(self, transactionKeys=None, transactionTypes=None, teamId=None, teamTransType=None, count=None):
        '''
        retrieve data from the transactions collection api resource

        transactionKeys                     list of transaction keys
        transactionTypes                    list of transaction types
        teamId                              id of the team to get transactions for (fantasy team)
        teamTransType                       must have teamId defined for this
        count                               number of transactions to return

        This is a collection of transaction resources

        https://fantasysports.yahooapis.com/fantasy/v2/league/223.l.431/transactions;transaction_keys={game_key}.l.{league_id}.{trans_key}
            see docs for transaction key formats

        transaction_keys                              transaction_keys=key1,key2,...

        filters for transactions
            type (add,drop,commish,trade)             /transactions;type=add
            types                                     /transactions;types=add,trade
            team_key                                  /transactions;team_key=257.l.193.t.1
            type (waiver,pending_trade) & team_key    /transactions;team_key=257.l.193.t.1;type=waiver
            count                                     /transactions;count=5

        '''

        if transactionKeys and isinstance(transactionKeys, list):
            self._opts['transaction_keys'] = ','.join(transactionKeys)

        if transactionKeys and isinstance(transactionKeys, list):
            self._opts['types'] = ','.join(transactionTypes)

        if teamId:
            self._opts['team_key'] = '{0}.l.{1}.t.{2}'.format(self._gameKey, self._leagueId, teamId)

        if teamId and teamTransType:
            self._opts['type'] = teamTransType

        if count:
            self._opts['count'] = count

        transactionBaseUrl = '{0}/transactions'.format(self._baseURL)
        return self._build_query_string(transactionBaseUrl)









class YahooTransaction:
    '''
    retrieve data from the transactions api resource.

    This is a collection of transaction resources

    https://fantasysports.yahooapis.com/fantasy/v2/league/223.l.431/transactions;transaction_keys={game_key}.l.{league_id}.{trans_key}
     see docs for transaction key formats

    filters for transactions
        type (add,drop,commish,trade)             /transactions;type=add
        types                                     /transactions;types=add,trade
        team_key                                  /transactions;team_key=257.l.193.t.1
        type (waiver,pending_trade) & team_key    /transactions;team_key=257.l.193.t.1;type=waiver
        count                                     /transactions;count=5
    '''

    pass




class GameKeyError(Exception):
    '''
    special exception for game keys
    '''

    def __init__(self, message='The game key was not found'):
        super(GameKeyError, self).__init__(message)

class YahooAPIBase:
    '''
    base class to hold common API functionality
    '''

    def __init__(self, yahooOauth, db=None, configFile=None):
        '''
        yahooOauth:     an object of class YahooOAuth2 for API authorization
        db:             sqalchemy database
        configFile:     path to a saved config file
        '''

        self.yahooOauth = yahooOauth
        self.configFile = configFile

        # format is key: (sport, season), value: game_key
        self.knownGameKeys = {}
        self._load_known_game_keys()

        self.knownManagers = {}
        self._load_known_managers()

        # database?
        self.db = db

        self.baseURL = 'https://fantasysports.yahooapis.com/fantasy/v2'

    def _load_known_game_keys(self):
        #either database or json file

        if self.configFile is not None:
            with open(self.configFile, 'r') as f:
                cfg = json.load(f)

            if 'knownGameKeys' in cfg:
                self.knownGameKeys = cfg['knownGameKeys']

    def _load_known_managers(self):
        #either database or json file

        if self.configFile is not None:
            with open(self.configFile, 'r') as f:
                cfg = json.load(f)

            if 'knownManagers' in cfg:
                self.knownManagers = cfg['knownManagers']

    def _write_config_file(self):

        if self.configFile is not None:

            cfg = {'knownGameKeys': self.knownGameKeys,
                   'knownManagers': self.knownManagers }

            with open(self.configFile, 'w') as f:
                json.dump(cfg, f)

    def _add_game_key(self, season, sport, gameKey):
        '''
        add a game key to the internal dict of known keys.

        season:     the year of the season
        sport:      the sport to add (YAHOO_API_SPORTS)
        gameKey:   corresponding integer game key
        '''

        if (sport, season) not in self.knownGameKeys:
            self.knownGameKeys[(sport, season)] = gameKey

    def _get_data(self, url, params=None):

        return self.yahooOauth.get_data(url, params=params)

    def _get_game_key(self, season, sport):
        '''
        seasons is the year
        sport can be any of YAHOO_API_SPORTS
        '''

        if (sport, season) in self.knownGameKeys:
            return self.knownGameKeys[(sport, season)]

        # if it gets to here, the key wasn't found.
        raise GameKeyError(message="game key not found for the given sport, season combo.")

    def _build_league_code(self, gameKey, leagueID):
        return '{0}.l.{1}'.format(gameKey, leagueID)

    def _build_team_code(self, gameKey, leagueID, teamID):
        return '{0}.l.{1}.t.{2}'.format(gameKey, leagueID, teamID)

    def _build_player_code(self, gameKey, playerID):
        return '{0}.p.{1}'.format(gameKey, playerID)

class YahooGame(YahooAPIBase):
    '''
        allows retrieval of the yahoo game resource and its sub resources.
    '''

    def __init__(self, yahooOauth, db=None, configFile=None):
        super(YahooGame, self).__init__(yahooOauth, db=db, configFile=configFile)

    def get_game(self, gameKey):
        '''
            get a single game resource

            gameKey:        game key to retrieve. An integer number, or a YAHOO_API_SPORTS
                            string that gives the latest game of that sport.
        '''

        url = '{0}/game/{1}'.format(self.baseURL, gameKey)
        return self._get_data(url)

    def get_games(self, isAvailable=None, gameTypes=[], gameCodes=[], seasons=[], gameKeys=[]):
        '''
            get collection of game resources

            isAvailable:   may be either 0 or 1. 1 means that the game is in season
            gameTypes:     list of game types to retrieve. must be one of YAHOO_API_GAME_TYPES
            gameCodes:     list of game codes to retrieve. must be one of YAHOO_API_SPORTS
            seasons:        list of integer years to retrieve.
            gameKeys:      list of game keys to retrieve. An integer number.
        '''

        if isAvailable is not None:
            if isAvailable not in [0, 1]:
                raise ValueError('is_available may only be 0 or 1.')

        #build filter string
        filterStr = ''

        if isAvailable is not None:
            filterStr += ';is_available={0}'.format(isAvailable)

        if len(gameTypes) > 0:
            filterStr += ';game_types={0}'.format(','.join([x.value for x in gameTypes]))

        if len(gameCodes) > 0:
            filterStr += ';game_codes={0}'.format(','.join([x.value for x in gameCodes]))

        if len(seasons) > 0:
            filterStr += ';seasons={0}'.format(','.join([str(x) for x in seasons]))

        if len(gameKeys) > 0:
            filterStr += ';game_keys={0}'.format(','.join([str(x) for x in gameKeys]))

        url = '{0}/games{1}'.format(self.baseURL, filterStr)
        return self._get_data(url)

# schedule? teams?
class YahooLeague(YahooAPIBase):
    '''
        allows retrieval of the yahoo league resource and its sub resources.
    '''

    def __init__(self, yahooOauth, db=None, configFile=None):
        super(YahooLeague, self).__init__(yahooOauth, db=db, configFile=None)

    def _build_base_url(self, sport=None, season=None, leagueID=None):

        if sport is None:
            raise TypeError("The sport must be provided.")

        if season is None:
            raise TypeError("The season must be provided.")

        if leagueID is None:
            raise TypeError("The leagueID must be provided.")

        leagueCode = self._build_league_code(self._get_game_key(season, sport),
                                             leagueID)

        url = '{0}/league/{1}'.format(self.baseURL, leagueCode)

        return url

    def _clean_draft_results(self, resultData):
        '''
        adjust formatting of draft result response for easier use
        '''

        draftResults = {'meta': {}, 'league': {}, 'picks': []}

        draftResults['meta']['url'] = resultData['fantasy_content']['yahoo:uri']
        draftResults['meta']['time'] = resultData['fantasy_content']['time']

        draftResults['league'] = resultData['fantasy_content']['league'][0]

        results = resultData['fantasy_content']['league'][1]['draft_results']

        if len(results) > 0:
            for idx, pick in results.items():

                if idx != 'count':
                    draftResults['picks'].append(pick['draft_result'])

        return draftResults

    def get(self, sport=None, season=None, leagueID=None):
        '''
            get the root level League Resource
        '''

        url = self._build_base_url(sport=sport, season=season, leagueID=leagueID)
        return self._get_data(url)

    def get_draft_results(self, sport=None, season=None, leagueID=None, cleanResults=True):
        '''
            get the draft results for that league

            cleanResults=True runs the response through internal formatting methods.
        '''

        url = '{0}/draftresults'.format(self._build_base_url(sport=sport,
                                                             season=season,
                                                             leagueID=leagueID))

        if cleanResults:
            return self._clean_draft_results(self._get_data(url))
        else:
            return self._get_data(url)

    def get_settings(self, sport=None, season=None, leagueID=None):
        '''
            get the settings for the league
        '''

        url = '{0}/settings'.format(self._build_base_url(sport=sport,
                                                         season=season,
                                                         leagueID=leagueID))

        return self._get_data(url)

    def get_scoreboard(self, sport=None, season=None, leagueID=None):
        '''
            get the scoreboard for that league
        '''

        url = '{0}/scoreboard'.format(self._build_base_url(sport=sport,
                                                           season=season,
                                                           leagueID=leagueID))

        return self._get_data(url)

    def get_standings(self, sport=None, season=None, leagueID=None):
        '''
            get the standings for that league
        '''

        url = '{0}/standings'.format(self._build_base_url(sport=sport,
                                                          season=season,
                                                          leagueID=leagueID))

        return self._get_data(url)

class YahooTeam(YahooAPIBase):
    '''
    retrieve data related to the team api resource
    '''

    pass
    #root - /team/team_key (game_key.l.league_key.t.1)
    #matchups - team/223.l.431.t.1/matchups;weeks=1,5
    #stats    - team/223.l.431.t.1/stats;type=season
    #         - team/253.l.102614.t.10/stats;type=date;date=2011-07-06

class YahooRoster(YahooAPIBase):
    '''
    retrieve data related to the roster api resource
    '''

    pass
    #root - /team/{team_key}/roster (game_key.l.league_key.t.1)
    #players
    #
    # can specify week or date    /team/{team_key}/roster;week={}
    #                             /team/{team_key}/roster;date=2011-05-01

class YahooPlayer(YahooAPIBase):
    '''
    retrieve data related to the player api resource
    '''

    def __init__(self, yahooOauth, db=None):
        super(YahooPlayer, self).__init__(yahooOauth, db=db)

    def _build_base_url(self, sport=None, season=None, leagueID=None):

        if sport is None:
            raise TypeError("The sport must be provided.")

        if season is None:
            raise TypeError("The season must be provided.")

        if leagueID is None:
            raise TypeError("The leagueID must be provided.")

        leagueCode = self._build_league_code(self._get_game_key(season, sport),
                                             leagueID)

        url = '{0}/league/{1}/players'.format(self.baseURL, leagueCode)

        return url

    def _clean_player_results(self, playerData):
        '''
        makes the API returned list of players into a cleaner data structure.
        as-returned is a nested mess

        playerData:     the raw response from API player resource

        returns a clean dictionary of retrieved players, key= playerID, value= data dictionary.
                None if no players are found matching the query.
        '''

        cleanPlayers = {}

        apiPlayers = playerData['fantasy_content']['league'][1]['players']

        if len(apiPlayers) == 0:
            return None

        for idx, player in apiPlayers.items():
            cleanData = {}

            if isinstance(player, dict):
                for entry in player['player'][0]:

                    if 'player_key' in entry:
                        cleanData['player_key'] = entry['player_key']

                    if 'player_id' in entry:
                        cleanData['player_id'] = int(entry['player_id'])

                    if 'name' in entry:
                        cleanData['name_full'] = entry['name']['full']
                        cleanData['name_first'] = entry['name']['first']
                        cleanData['name_last'] = entry['name']['last']
                        cleanData['name_ascii_first'] = entry['name']['ascii_first']
                        cleanData['name_ascii_last'] = entry['name']['ascii_last']

                    if 'status' in entry:
                        cleanData['status'] = entry['status']

                    if 'status_full' in entry:
                        cleanData['status_full'] = entry['status_full']

                    if 'editorial_player_key' in entry:
                        cleanData['editorial_player_key'] = entry['editorial_player_key']

                    if 'editorial_player_key' in entry:
                        cleanData['editorial_player_key'] = entry['editorial_player_key']

                    if 'editorial_team_key' in entry:
                        cleanData['editorial_team_key'] = entry['editorial_team_key']

                    if 'editorial_team_full_name' in entry:
                        cleanData['editorial_team_full_name'] = entry['editorial_team_full_name']

                    if 'editorial_team_abbr' in entry:
                        cleanData['editorial_team_abbr'] = entry['editorial_team_abbr']

                    if 'uniform_number' in entry:
                        cleanData['uniform_number'] = entry['uniform_number']

                    if 'display_position' in entry:
                        cleanData['display_position'] = entry['display_position']

                    if 'is_undroppable' in entry:
                        cleanData['is_undroppable'] = entry['is_undroppable']

                    if 'position_type' in entry:
                        cleanData['position_type'] = entry['position_type']

                    if 'eligible_positions' in entry:
                        cleanData['eligible_positions'] = []

                        for pos in entry['eligible_positions']:
                            cleanData['eligible_positions'].append(pos['position'])

                cleanPlayers[cleanData['player_id']] = cleanData

        return cleanPlayers

    def get_players(self, sport=None, season=None, leagueID=None, cleanResults=True,
                    positions=[], status=None, search=None, sort=None,
                    sortType=None, sortSeason=None, sortDate=None,
                    sortWeek=None, start=None, count=None, playerKeys=[]):
        '''
        cleanResults=True calls internal formatting methods to make results easier to manage

        get the Players Resource
        https://fantasysports.yahooapis.com/fantasy/v2/league/223.l.431/players;player_keys=223.p.5479
        root - player_key (game_key.p.{id})
        /stats

        filters:
            positions (QB,RB, etc)                          /players;position={}
            status (A [all], FA [free agenst],              /players;status={}
                    W [waivers], T [taken], K[keeper])
            search (search for name)                        /players;search={}
            sort (stat_id, NAME, OR, AR, PTS)               /players;sort={}
            sortType  (season, date, week,
                       lastweek, lastmonth)                 /players;sort_type={}
            sortSeason (year)                               /players;sort_type=season;sort_season={}
            sortDate                                        /players;sort_type=date;sort_date={}
            sortWeek                                        /players;sort_type=week;sort_week={}
            start (start number)                            /players;start=25
            count (number to get)                           /players;count=5

            use start and count for paging

            TODO: add filter validation
        '''

        filterStr = ''

        if len(positions) > 0:
            filterStr += ';position={0}'.format(','.join(positions))

        if len(playerKeys) > 0:
            filterStr += ';player_keys={0}'.format(','.join(playerKeys))

        if status is not None:
            filterStr += ';status={0}'.format(status)

        if search is not None:
            filterStr += ';search={0}'.format(search)

        if sort is not None:
            filterStr += ';sort={0}'.format(sort)

        if sortType is not None:
            filterStr += ';sort_type={0}'.format(sortType)

        if sortSeason is not None:
            filterStr += ';sort_season={0}'.format(sortSeason)

        if sortDate is not None:
            filterStr += ';sort_date={0}'.format(sortDate)

        if sortWeek is not None:
            filterStr += ';sort_week={0}'.format(sortWeek)

        if start is not None:
            filterStr += ';start={0}'.format(start)

        if count is not None:
            filterStr += ';count={0}'.format(count)


        url = '{0};{1}'.format(
                                self._build_base_url(sport=sport, season=season, leagueID=leagueID),
                                filterStr
                              )

        if cleanResults:
            return self._clean_player_results(self._get_data(url))
        else:
            return self._get_data(url)

    def get_all_players(self, sport, season, leagueID, cleanResults=True):
        '''
        get all players in league, regardless of ownership.
        '''

        players = {}
        startValue = 0

        while True:
            newPlayers = self.get_players(sport=sport, season=season,
                                          leagueID=leagueID, cleanResults=cleanResults,
                                          status='A', start=startValue, count=25)

            if newPlayers is not None:
                players.update(newPlayers)
                startValue += len(newPlayers)
            else:
                break

        return players

    def get_all_available_players(self, sport, season, leagueID, cleanResults=True):
        '''
        get all players that are unowned in league, regardless of ownership.
        '''

        players = {}
        startValue = 0

        # Free agents first
        while True:
            newPlayers = self.get_players(sport=sport, season=season, leagueID=leagueID,
                                          cleanResults=cleanResults, status='FA',
                                          start=startValue, count=25)

            if newPlayers is not None:
                players.update(newPlayers)
                startValue += len(newPlayers)
            else:
                break

        # now waivers
        startValue = 0
        while True:
            newPlayers = self.get_players(sport=sport, season=season, leagueID=leagueID,
                                          status='W', start=startValue, count=25)

            if newPlayers is not None:
                players.update(newPlayers)
                startValue += len(newPlayers)
            else:
                break

        return players



class YahooUser(YahooAPIBase):
    '''
    retrieve data from the user api resource
    '''

    pass


def get_game_key(oauth, season, sport, gameType=YAHOO_API_GAME_TYPES.FULL):
    '''
    oauth is YahooOAuth2 class for API authentication
    seasons is the year
    sport can be any of YAHOO_API_SPORTS
    '''

    yahoo = YahooGame(oauth)
    data = yahoo.get_games(seasons=[season], gameCodes=[sport], gameTypes=[gameType])

    for key, gms in data['fantasy_content']['games'].items():
        if key != 'count':
            for gm in gms['game']:
                if gm['code'] == sport.value:
                    return int(gm['game_key'])

    # if it gets to here, the key wasn't found.
    raise GameKeyError(message="game key not found for the given sport, season combo.")

def get_all_game_keys(oauth):
    '''
    oauth is YahooOAuth2 class for API authentication
    '''

    yahoo = YahooGame(oauth)
    data = yahoo.get_games(gameCodes=[YAHOO_API_SPORTS.NFL, YAHOO_API_SPORTS.MLB,
                                      YAHOO_API_SPORTS.NBA, YAHOO_API_SPORTS.NHL],
                           gameTypes=[YAHOO_API_GAME_TYPES.FULL])

    # for key, gms in data['fantasy_content']['games'].items():
    #     if key != 'count':
    #         for gm in gms['game']:
    #             if gm['code'] == sport:
    #                 return int(gm['game_key'])

    # # if it gets to here, the key wasn't found.
    # raise GameKeyError(message="game key not found for the given sport, season combo.")


#[2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018]
#MFFL league ids:[267890,214624,420672,395295,128931,61287,241165,110660,78339,357590,358169,115846]

def main():
    urlBuilder = YahooApiUrlBuilder(380,115846)
    print(urlBuilder.build_transactions_url(teamId=2))

if __name__ == '__main__':
    main()