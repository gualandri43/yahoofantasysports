from bs4 import BeautifulSoup
import pandas as pd

class DraftPick:
    def __init__(self):
        self.overallPick = None
        self.round = None
        self.roundPick = None
        self.playerID = None
        self.playerName = None
        self.playerTeam = None
        self.playerPos = None
        self.manager = None
    
    def parse_row(self, rowElem):

        self.roundPick = int(rowElem.find(class_='first').text.strip().replace('.',''))

        self.playerName = rowElem.find(class_='player').find(class_='name').text.strip()
        self.playerID = rowElem.find(class_='player').find(class_='name')['href'].split('/')[-1]

        teamPosString = rowElem.find(class_='player').find_all("span")[-1].text.strip().replace('(','').replace(')','').split('-')
        self.playerTeam = teamPosString[0].strip()
        self.playerPos = teamPosString[1].strip()

        self.manager = rowElem.find(class_='last')['title'].replace("'","")

class Draft:
    def __init__(self):
        self.numTeams = None
        self.league = None
        self.season = None
        self.picks = []

    @property
    def numRounds(self):
        return int(len(self.picks) / self.numTeams)

    def parse_draft_webpage(self, pagePath):
        
        with open(pagePath,'r') as f:
            pg = BeautifulSoup(f, "lxml")
            for tbl in pg.find_all(class_='simpletable'):

                roundNumber = int(tbl.find('thead').find('tr').find('th').text.split(' ')[-1])

                for bdy in tbl.find_all('tbody'):
                    for row in bdy.find_all('tr'):
                        dp = DraftPick()
                        dp.parse_row(row)
                        dp.round = roundNumber
                        dp.overallPick = (dp.round-1) * self.numTeams + dp.roundPick
                        
                        self.picks.append(dp)
    
    def make_dataframe(self):

        df = pd.DataFrame.from_records([x.__dict__ for x in self.picks])
        df['numteams'] = self.numTeams
        df['numRounds'] = self.numRounds
        df['season'] = self.season
        df['league'] = self.league

        return df.set_index(['league','season','overallPick'])


if __name__ == "__main__":
    # read all draft files
    years       = [2017,2016,2015,2014,2013]#,2012,2011,2010,2009,2008,2007]
    num_teams   = [  10,  10,  10,  10,  10]#,   8,   8,   8,   8,   8,   8]

    htmlDir = 'D:/Google Drive/Fantasy Sports/Fantasy Football/MFFL/mffl_draft_archive'

    allDrafts = pd.DataFrame()

    for i, val in enumerate(years):
        print(val)
        curDraft = Draft()
        curDraft.numTeams = num_teams[i]
        curDraft.season = years[i]
        curDraft.league = 'mffl'

        draftFile = "{0}/draft{1}.html".format(htmlDir, years[i])

        curDraft.parse_draft_webpage(draftFile)

        df = curDraft.make_dataframe()
        allDrafts = pd.concat([allDrafts, df])


    #print(allDrafts['playerPos'].value_counts())
    countDF = allDrafts.loc[:,['playerPos','round','playerID']].reset_index('overallPick',drop=True)
    print(countDF.groupby(['league','season','round', 'playerPos']).count())

    df2017 = countDF.loc[('mffl',2017),:].groupby(['round', 'playerPos']).count().groupby(['playerPos']).cumsum()       #running round counts
    print(df2017)

    #print(allDrafts.groupby(['season','playerPos'])['playerID'].cumcount())
    #print(allDrafts.loc['2017'].groupby(['season','round','playerPos'])['playerID'].count())

