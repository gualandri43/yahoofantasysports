import lxml.etree as etree
import io
import datetime as dt

import sports_modules.sports_scraper.support.scrapy as scrapy_support


class YahooScrapyPipeline(scrapy_support.PostgresPipeline):
    '''
    subclass scrapy_support.PostgresPipeline to save item to database table
    '''
    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the necessary spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        scrapy_support.PostgresPipeline._add_args_to_parser(parser)

    def _get_table_name(self, item=None, spider=None):
        return "api_scrape_log"

    def _get_schema_name(self, item=None, spider=None):
        #return None if no schema
        return "yahoo"

    def _setup(self, spider=None):
        '''This method runs when spider is opened, and allows
        for any customized setup. Can return None if no steps needed.
        '''
        return None

    def _cleanup(self, spider=None):
        '''This method runs when spider is closed, and allows
        for any customized clean up or teardown. Can return None if no steps needed.
        '''
        return None

class YahooAllPlayersPipeline(YahooScrapyPipeline):
    '''
    added funcitonality to YahooAllPlayersPipeline to flush players collected
    from the all players scraper.
    '''

    def close_spider(self, spider):

        item = spider._prepopulate_item()
        item['endpoint_id'] = spider.settings.get('YAHOO_ENDPOINT_ID')
        item['scrape_meta'] = spider._get_all_yahoo_settings()
        item['scrape_data'] = etree.tostring(spider._playerCollection)

        self.process_item(item, spider)

class YahooFtpPipeline(scrapy_support.FtpPipeline):
    '''
    subclass scrapy_support.FtpPipeline to save item to a file on ftp server.
    '''

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the necessary spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        scrapy_support.FtpPipeline._add_args_to_parser(parser)

    def _setup(self, spider=None):
        '''This method runs when spider is opened, and allows
        for any customized setup. Can return None if no steps needed.
        '''
        None

    def _cleanup(self, spider=None):
        '''This method runs when spider is closed, and allows
        for any customized clean up or teardown. Can return None if no steps needed.
        '''
        None

    def _get_filename(self, item=None, spider=None):
        '''
            Build or give a filename, may use info from the
            item or the spider
        '''
        pass

        metaData = []
        for k,v in item['scrape_meta'].items():
            metaData.append('{0}_{1}'.format(k, v))

        if isinstance(item['scrape_time_local'], dt.datetime):
            item['scrape_time_local'] = item['scrape_time_local'].strftime('%Y-%M-%d')

        return 'endpoint_{0}.{1}.{2}.xml'.format(
                                        item['endpoint_id'],
                                        '.'.join(metaData),
                                        item['scrape_time_local']
                                    )

    def _item_to_file(self, item=None, spider=None):
        '''
            Provide a function to convert an item to a file. Called for each
            item.

            Must return a file-like object or stream (such as io.StringIO)
        '''

        # item['scrape_data'] will have an xml string
        return io.StringIO(item['scrape_data'])

class YahooAllPlayersFtpPipeline(YahooFtpPipeline):
    '''
    added funcitonality to YahooFtpPipeline to flush out player collection
    after spider has finished (paged scraper)
    '''

    def _cleanup(self, spider=None):
        '''This method runs when spider is closed, and allows
        for any customized clean up or teardown. Can return None if no steps needed.
        '''

        # create an item out of the full XML tree created by the AllPlayers spider.

        item = spider._prepopulate_item()
        item['endpoint_id'] = spider.settings.get('YAHOO_ENDPOINT_ID')
        item['scrape_meta'] = spider._get_all_yahoo_settings()
        item['scrape_data'] = etree.tostring(spider._playerCollection)

        self.process_item(item, spider)
