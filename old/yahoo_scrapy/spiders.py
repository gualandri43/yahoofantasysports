'''
base spider for yahoo scrapers

requires settings
OAUTH_CREDENTIALS_FILE

'''

import datetime as dt
import abc
import time
import argparse
import copy
import lxml.etree as etree
import scrapy

import sports_modules.sports_scraper.support.scrapy as scrapy_support
import sports_modules.sports_scraper.support.time as time_support
import sports_modules.sports_scraper.yahoo.yahoo_scrapy.items as yahoo_scrapy_items
import sports_modules.sports_scraper.yahoo.yahoo_scrapy.pipelines as yahoo_scrapy_pipelines
import sports_modules.sports_scraper.yahoo.fantasy_api as yahoo_api

class YahooResponseMiddleware(object):
    '''
    check the response for yahoo-specific response codes.
    '''

    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=scrapy.signals.spider_opened)
        return s

    def process_request(self, request, spider):
        '''
        Called for each request that goes through the downloader
        middleware.

        Must either:
        - return None: continue processing this request
        - or return a Response object
        - or return a Request object
        - or raise IgnoreRequest: process_exception() methods of
          installed downloader middleware will be called
        '''
        return None

    def process_response(self, request, response, spider):
        '''
        Yahoo API has some specific return codes. This handles them.
        https://developer.yahoo.com/yql/guide/response.html#response-errors

        200 - OK
        400 - Bad request
        401 - Auth error
        999 - unable to process (rate-limiting)

        Called with the response returned from the downloader.

        Must either;
        - return a Response object
        - return a Request object
        - or raise IgnoreRequest
        '''

        # OK
        if response.status == 200:
            return response

        # bad request
        #   return a response with a body=None
        if response.status == 400:
            spider.logger.warning("Request {0} is not correctly formed and has been ignored.".format(request.url))
            #return response.replace(body=None)
            raise scrapy.exceptions.IgnoreRequest()

        # OAuth authorization required, or invalid auth
        #   pass through this, it will be handled by OAuthDownloaderMiddleware
        if response.status == 401:
            return response

        # rate limit, delay and reschedule request
        if response.status == 999:
            spider.crawler.stats.inc_value('yahoo_999_retries', start=0)

            if spider.crawler.stats.get_value('yahoo_999_retries') < 10:
                delayTime = spider.crawler.stats.get_value('yahoo_999_retries') * 15
                spider.logger.info('Unable to process (rate limit?). Delaying {0} seconds and rescheduling request.'.format(delayTime))
                time.sleep(delayTime)

                return request
            else:
                spider.logger.error('Unable to process (rate limit?). Retry limit reached.')
                raise scrapy.exceptions.CloseSpider("Response 999 retry limit reached.")
        else:
            #reset 999 retry count
            spider.crawler.stats.set_value('yahoo_999_retries', 0)
            return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)

COMMON_DOWNLOADER_MIDDLEWARES = {
    'sports_modules.sports_scraper.support.scrapy.OAuthDownloaderMiddleware': 200,
    'sports_modules.sports_scraper.yahoo.yahoo_scrapy.spiders.YahooResponseMiddleware': 250
}

COMMON_ITEM_PIPELINE = {
    'sports_modules.sports_scraper.yahoo.yahoo_scrapy.pipelines.YahooScrapyPipeline': 500,
    'sports_modules.sports_scraper.yahoo.yahoo_scrapy.pipelines.YahooFtpPipeline': 505
}

ALLPLAYERS_ITEM_PIPELINE = {
    'sports_modules.sports_scraper.yahoo.yahoo_scrapy.pipelines.YahooAllPlayersPipeline': 500,
    'sports_modules.sports_scraper.yahoo.yahoo_scrapy.pipelines.YahooAllPlayersFtpPipeline': 505
}

class YahooBase(scrapy_support.OAuthSpider, abc.ABC):
    '''
    base spider for yahoo scrapers common functions
    '''

    name = "yahoo_api_base"

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        scrapy_support.OAuthSpider._add_args_to_parser(parser)

    @staticmethod
    def _add_league_and_season_args_to_parser(parser):
        '''
        Add arguments for YAHOO_SEASON and YAHOO_LEAGUE

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        parser.add_argument('--YAHOO_LEAGUE', dest='YAHOO_LEAGUE', action='store', required=True,
                            help='The yahoo league to scrape.')

        parser.add_argument('--YAHOO_SEASON', dest='YAHOO_SEASON', action='store', required=True,
                            help='The season to scrape.')

    @staticmethod
    def _add_teamid_args_to_parser(parser):
        '''
        Add arguments for YAHOO_TEAM

        parser      An argparse ArgumentParser class.
        '''

        if not isinstance(parser, argparse.ArgumentParser):
            raise ValueError("parser must be an ArgumentParser.")

        parser.add_argument('--YAHOO_TEAM', dest='YAHOO_TEAM', action='store', required=True,
                            help='The id of the team to scrape.')

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        '''
        snippet taken from scrapy.Spider base class, but added
        OAuth initialization to it.
        '''
        spider = cls(*args, **kwargs)
        spider._set_crawler(crawler)

        #initialize OAuth
        spider._load_oauth()

        #check needed settings using abstract method
        spider._check_base_class_required_settings()
        spider._check_for_required_settings()

        return spider

    def _check_base_class_required_settings(self):
        '''
        run check settings for base class.
        '''
        # run base class
        super()._check_for_required_settings()

    def _check_league_and_season_settings(self):
        '''
        run check settings league_and_season_settings.
        '''

        if self.settings.get('YAHOO_LEAGUE') is None:
                raise scrapy.exceptions.CloseSpider("Required setting YAHOO_LEAGUE is not defined.")

        if self.settings.get('YAHOO_SEASON') is None:
                raise scrapy.exceptions.CloseSpider("Required setting YAHOO_SEASON is not defined.")

    @abc.abstractmethod
    def _check_for_required_settings(self):
        '''
        add any required settings checks.
        '''
        raise NotImplementedError()

    def _prepopulate_item(self):
        '''
        Creates a ApiScrapeLog() item and adds scrape time info.
        '''

        scrapeTimes = time_support.record_scrape_times()

        item = yahoo_scrapy_items.ApiScrapeLog()

        #times
        item['scrape_time_local'] = scrapeTimes['scrape_time_local']
        item['scrape_time_utc'] = scrapeTimes['scrape_time_utc']

        #endpoint ID
        item['endpoint_id'] = self.settings.get('YAHOO_ENDPOINT_ID')

        return item

    def _get_all_yahoo_settings(self):
        '''
        iterate through settings and retrieve any settings prefixed with YAHOO_
        '''

        returnDict = dict()

        for _setting, _value in self.settings.copy_to_dict().items():
            if _setting.startswith("YAHOO_"):
                returnDict[_setting] = _value

        return returnDict

    def parse(self, response):
        '''
            generic parse routine. Can be specialized if needed, but this records scrape times,
            endpoint ID, the API response XML, and any settings beginning with YAHOO_.

            item entries must match the fields for the database scrape log table
            -----
            endpoint_id         integer
            scrape_time_local   datetime
            scrape_time_utc     datetime
            scrape_meta         dict
            scrape_data         dict
        '''

        item = self._prepopulate_item()
        item['endpoint_id'] = self.settings.get('YAHOO_ENDPOINT_ID')
        item['scrape_meta'] = self._get_all_yahoo_settings()
        item['scrape_data'] = response.body.decode()

        yield item

class YahooDraftScraper(YahooBase):
    '''
    spider for yahoo fantasy API draft results. add additional functionality to YahooBase spider
    '''

    name = "yahoo_api_draftresults"

    custom_settings={
        'DOWNLOADER_MIDDLEWARES': COMMON_DOWNLOADER_MIDDLEWARES,
        'ITEM_PIPELINES': COMMON_ITEM_PIPELINE
    }

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        YahooBase._add_args_to_parser(parser)
        YahooBase._add_league_and_season_args_to_parser(parser)
        yahoo_scrapy_pipelines.YahooScrapyPipeline._add_args_to_parser(parser)

    def _check_for_required_settings(self):
        super()._check_league_and_season_settings()

    def start_requests(self):

        api = yahoo_api.YahooFantasyAPI(responseFormat='xml')

        endPt = api.endpoint_league_draft(
                        self.settings.get('YAHOO_LEAGUE'),
                        self.settings.get('YAHOO_SEASON')
                    )

        self.settings.set('YAHOO_ENDPOINT_ID', endPt.endpointId.value)

        yield scrapy.Request(
                        url=endPt.url,
                        callback=self.parse
                    )

class YahooTransactionsScraper(YahooBase):
    '''
    spider for yahoo fantasy API transactions. add additional functionality to YahooBase spider
    '''

    name = "yahoo_api_transactions"

    custom_settings={
        'DOWNLOADER_MIDDLEWARES': COMMON_DOWNLOADER_MIDDLEWARES,
        'ITEM_PIPELINES': COMMON_ITEM_PIPELINE
    }

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        YahooBase._add_args_to_parser(parser)
        YahooBase._add_league_and_season_args_to_parser(parser)
        yahoo_scrapy_pipelines.YahooScrapyPipeline._add_args_to_parser(parser)

        #
        # optional
        #
        parser.add_argument('--YAHOO_TRANSACTION_KEY_LIST', dest='YAHOO_TRANSACTION_KEY_LIST', action='store', nargs='+',
                            help='A space separated list of transaction keys to pull.')

        parser.add_argument('--YAHOO_TRANSACTION_TYPE_LIST', dest='YAHOO_TRANSACTION_TYPE_LIST', action='store', nargs='+',
                            choices=['add', 'drop', 'add/drop', 'trade', 'pending_trade', 'waiver'],
                            help='Optional. The transaction types to retrieve. Space separated list. '
                                 'Choices are [add, drop, add/drop, trade, pending_trade, waiver]')

    def _check_for_required_settings(self):
        super()._check_league_and_season_settings()

    def start_requests(self):

        api = yahoo_api.YahooFantasyAPI(responseFormat='xml')

        # parse optional settings
        keyList = self.settings.getlist('YAHOO_TRANSACTION_KEY_LIST', default=None)
        typeList = self.settings.getlist('YAHOO_TRANSACTION_TYPE_LIST', default=None)

        transactions = api.endpoint_transactions(
                                        self.settings.get('YAHOO_LEAGUE'),
                                        self.settings.get('YAHOO_SEASON'),
                                        transactionKeys=keyList,
                                        transactionTypes=typeList
                                    )

        self.settings.set('YAHOO_ENDPOINT_ID', transactions.endpointId.value)

        yield scrapy.Request(
                        url=transactions.url,
                        callback=self.parse
                    )

class YahooGetAllPlayersScraper(YahooBase):
    '''
    spider for yahoo fantasy API get all players. add additional functionality to YahooBase spider
    '''

    name = "yahoo_api_allplayers"

    # override base pipeline for this spider
    custom_settings={
        'DOWNLOADER_MIDDLEWARES': COMMON_DOWNLOADER_MIDDLEWARES,
        'ITEM_PIPELINES': ALLPLAYERS_ITEM_PIPELINE
    }

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        YahooBase._add_args_to_parser(parser)
        YahooBase._add_league_and_season_args_to_parser(parser)
        yahoo_scrapy_pipelines.YahooAllPlayersPipeline._add_args_to_parser(parser)

    def _check_for_required_settings(self):
        super()._check_league_and_season_settings()

    def start_requests(self):

        #initialize overall scrape collection
        self._playerCollection = None

        #build api request
        api = yahoo_api.YahooFantasyAPI(responseFormat='xml')

        allPlayers = api.endpoint_players(
                            self.settings.get('YAHOO_LEAGUE'),
                            self.settings.get('YAHOO_SEASON'),
                            start=0,
                            count=25
                        )

        self.settings.set('YAHOO_ENDPOINT_ID', allPlayers.endpointId.value)

        yield scrapy.Request(
                        url=allPlayers.url,
                        callback=self.parse,
                        meta= {
                            'lastStart': 0
                        }
                    )

    def parse(self, response):
        '''
            check the returned player count. if some are left, schedule another request
            for the next page of results. Once none are returned, save all as one big json list.

            item entries must match the fields for the database scrape log table
            -----
            endpoint_id         integer
            scrape_time_local   datetime
            scrape_time_utc     datetime
            scrape_meta         dict
            scrape_data         dict
        '''

        #
        #   Note that players are collected into self._playerCollection,
        #   then flushed to database in the pipeline when spider is finished.
        #

        newData = etree.fromstring(response.body)

        if self._playerCollection is None:
            #initialize player response attribute if not yet done
            #   initialize to xml tree from response
            self._playerCollection = copy.deepcopy(newData)

        #check for new player results
        players = newData.xpath('/fantasy_content/league/players/player')
        numPlayers = len(players)

        #add results to overall collection
        #   overall collection is a xml element <players> tree
        _playersElement = self._playerCollection.xpath('/fantasy_content/league/players')[0]
        for _player in players:
            _playersElement.append(_player)

        if numPlayers > 0:
            # create a new request for next page of data if players
            #   were included in the last request

            nextStart = response.request.meta['lastStart'] + 25

            api = yahoo_api.YahooFantasyAPI(responseFormat='xml')

            allPlayers = api.endpoint_players(
                                self.settings.get('YAHOO_LEAGUE'),
                                self.settings.get('YAHOO_SEASON'),
                                start=nextStart,
                                count=25
                            )

            self.settings.set('YAHOO_ENDPOINT_ID', allPlayers.endpointId.value)

            yield scrapy.Request(
                        url=allPlayers.url,
                        callback=self.parse,
                        meta= {
                            'lastStart': nextStart
                        }
                    )

class YahooTeamMatchupsScraper(YahooBase):
    '''
    spider for yahoo fantasy API team matchups. add additional functionality to YahooBase spider
    '''

    name = "yahoo_api_team_matchups"

    custom_settings={
        'DOWNLOADER_MIDDLEWARES': COMMON_DOWNLOADER_MIDDLEWARES,
        'ITEM_PIPELINES': COMMON_ITEM_PIPELINE
    }

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        YahooBase._add_args_to_parser(parser)
        YahooBase._add_league_and_season_args_to_parser(parser)
        YahooBase._add_teamid_args_to_parser(parser)
        yahoo_scrapy_pipelines.YahooScrapyPipeline._add_args_to_parser(parser)

        #
        # optional
        #
        parser.add_argument('--YAHOO_WEEKS', dest='YAHOO_WEEKS', action='store', nargs='+',
                            help='A space separated list of week numbers to pull.')


    def _check_for_required_settings(self):
        super()._check_league_and_season_settings()

        if self.settings.get('YAHOO_TEAM') is None:
                raise scrapy.exceptions.CloseSpider("Required setting YAHOO_TEAM is not defined.")

    def start_requests(self):

        api = yahoo_api.YahooFantasyAPI(responseFormat='xml')

        # parse optional settings
        weekList = self.settings.getlist('YAHOO_WEEKS', default=None)

        matchups = api.endpoint_team_matchups(
                                    self.settings.get('YAHOO_LEAGUE'),
                                    self.settings.get('YAHOO_SEASON'),
                                    self.settings.get('YAHOO_TEAM'),
                                    weeks=weekList
                                )

        self.settings.set('YAHOO_ENDPOINT_ID', matchups.endpointId.value)

        yield scrapy.Request(
                        url=matchups.url,
                        callback=self.parse
                    )

class YahooTeamRoserScraper(YahooBase):
    '''
    spider for yahoo fantasy API team roster. add additional functionality to YahooBase spider
    '''

    name = "yahoo_api_team_roster"

    custom_settings={
        'DOWNLOADER_MIDDLEWARES': COMMON_DOWNLOADER_MIDDLEWARES,
        'ITEM_PIPELINES': COMMON_ITEM_PIPELINE
    }

    @staticmethod
    def _add_args_to_parser(parser):
        '''
        Add arguments needed to define the spider settings from command line.

        parser      An argparse ArgumentParser class.
        '''

        YahooBase._add_args_to_parser(parser)
        YahooBase._add_league_and_season_args_to_parser(parser)
        YahooBase._add_teamid_args_to_parser(parser)
        yahoo_scrapy_pipelines.YahooScrapyPipeline._add_args_to_parser(parser)

        #
        # optional
        #
        parser.add_argument('--YAHOO_WEEK', dest='YAHOO_WEEK', action='store',
                            help='The integer week number.')

        parser.add_argument('--YAHOO_DATE', dest='YAHOO_DATE', action='store',
                            help='Date string in YYYY-MM-DD format.')

    def _check_for_required_settings(self):
        super()._check_league_and_season_settings()

        if self.settings.get('YAHOO_TEAM') is None:
                raise scrapy.exceptions.CloseSpider("Required setting YAHOO_TEAM is not defined.")

    def start_requests(self):

        api = yahoo_api.YahooFantasyAPI(responseFormat='xml')

        # parse optional settings
        week = self.settings.get('YAHOO_WEEK', default=None)
        queryDate = self.settings.get('YAHOO_DATE', default=None)

        if queryDate:
            # parse date string if given.
            queryDate = dt.datetime.strptime(queryDate, '%Y-%m-%d')

        rosters = api.endpoint_team_roster(
                                    self.settings.get('YAHOO_LEAGUE'),
                                    self.settings.get('YAHOO_SEASON'),
                                    self.settings.get('YAHOO_TEAM'),
                                    week=week,
                                    date=queryDate
                                )

        self.settings.set('YAHOO_ENDPOINT_ID', rosters.endpointId.value)

        yield scrapy.Request(
                        url=rosters.url,
                        callback=self.parse
                    )

