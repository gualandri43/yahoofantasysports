# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ApiScrapeLog(scrapy.Item):
    '''
    define an item to match the api_scrape_log table structure.
    '''

    scrape_time_local = scrapy.Field()  # datetime
    scrape_time_utc = scrapy.Field()    # datetime
    endpoint_id = scrapy.Field()        # endpoint id number
    scrape_meta = scrapy.Field()        # dict
    scrape_data = scrapy.Field()        # dict
