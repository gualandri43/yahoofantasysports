# Description

A python wrapper for interacting with the Yahoo fantasy sports API endpoints.

# Usage

## Yahoo Authentication

The Yahoo API uses the OAuth2 standard for authenticating API requests for private fantasy leagues.
In order to use this, create an application within the Yahoo Development Network using these
[instructions](https://developer.yahoo.com/oauth2/guide/openid_connect/getting_started.html#getting-started-setup).
The only permission required is Read access to the `Fantasy Sports` endpoint. Once the application is created,
you will be given a Client ID and a Client Secret (also may be called a Consumer Key and Consumer Secret). This
information will be used for your script to request access to the fantasy API data.

The `yahoofantasysports.oauth` module contains a class called `YahooOAuthSession` to handle authentication
with the API. The easiest method is to set the Client ID and Client Secret you generated above as the `YAHOO_API_CLIENT_ID` and `YAHOO_API_CLIENT_SECRET` environment variables on your computer. If this is undesirable, they can also be passed to `YahooOAuthSession` in the constructor.

On the first connection, you must let Yahoo know that the access is authorized. This is done using `YahooOAuthSession`. Upon creation, you will be given a special Yahoo url. Click on or paste this link
into a browser and follow the instructions to grant access. Then paste the given short code into the terminal.

```
[In] >>> session = YahooOAuthSession()
[Out] Authorizing connection with Yahoo. Visit the following url and authenticate to get verification code: <url>
[Out] Enter verifier code:
```

Once this is completed, `YahooOAuthSession` will fetch the necessary tokens. This whole process is only needed to be completed once if the tokens are saved locally. These tokens are the equivalent of a password: keep them secure. To save the token for later use, use the `YahooOauthSession.token_to_json()` method.

```
[In] >>> with open('token.json', 'w') as f:
             session.token_to_json()
```

To reuse these tokens, load them in later. `YahooOAuthSession` will refresh access tokens as needed behind the scenes.

```
[In] >>> with open('token.json', 'r') as f:
             session = YahooOAuthSession.create_from_token(f.read())
```

## Endpoints
The `yahoofantasysports.fantasy_api` module contains classes for all the available endpoints of the Yahoo Fantasy API. Each class constuctor contains any necessary options or parameters that need to be defined for the API query. The class can then be used to directly query the API to get a `requests.Response` object, or used to build `YahooResource` objects.

## Resource Classes
The `yahoofantasysports.fantasy_api_resources` module contains classes for parsing the XML data returned by the `yahoofantasysports.fantasy_api` reponses into convenient python objects. To use these, create an authenticated `YahooOAuthSession` object and one of the `YahooEndpoint` objects, and use the `yahoofantasysports.fantasy_api_resources.query_yahoo_api` universal query function. This function will parse the reponses from the endpoint as necessary and return the data as the appropriate `YahooResource` object.
